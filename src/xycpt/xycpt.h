#ifndef XYCPT_H
#define XYCPT_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>
#include <stdbool.h>

typedef enum {
  reg_lower,
  reg_middle,
  reg_upper
} reg_t;

typedef struct
{
  bool verbose, debug, discrete, unital;
  comment_opt_t comment;
  reg_t reg;
  rgb_t fg, bg, nan;
  struct
  {
    const char *path;
  } input, output;
  int gmt_version;
} xycpt_opt_t;

int xycpt(const xycpt_opt_t*);

#endif
