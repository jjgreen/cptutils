gradient-convert
----------------

The `gradient-convert` script is a wrapper, calling the other programs
of _cptutils_ in sequence to effect the desired conversion.  As such,
it need these other programs to be on the path.

The file `capabilities.yaml` is the output of the `--capbilities` option
of the script, intended for the configuration and build of the package.

The file `capabilities.dot` is the output of the `--graphviz` option
of the script, this describes the conversion graph and can be plotted
by the [GraphViz][1] package.

![Example graph](capabilities.png)

[1]: https://graphviz.org/
