#ifndef CPTCAT_H
#define CPTCAT_H

#include <cptutils/cpt-fill.h>
#include <cptutils/cpt-hinge.h>
#include <cptutils/cpt-coerce.h>
#include <cptutils/comment.h>

#include <stdbool.h>
#include <stddef.h>

typedef struct
{
  bool verbose, normalise, denormalise;
  comment_opt_t comment;
  int gmt_version;
  struct
  {
    bool present;
    cpt_hinge_type_t type;
  } hinge;
  struct
  {
    const char *path;
    cpt_coerce_model_t model;
  } output;
  struct
  {
    size_t n;
    const char **path;
  } input;
} cptcat_opt_t;

int cptcat(const cptcat_opt_t*);

#endif
