#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-input.h"
#include "helper-output.h"
#include "cptinfo.h"

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  cptinfo_opt_t opt = {
    .verbose = info->verbose_flag,
    .format = (info->csv_flag ? csv : plain)
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is cptinfo (version %s)\n", VERSION);

  int err = cptinfo(&opt);

  if (err)
    fprintf(stderr, "failed (error %i)\n", err);

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
