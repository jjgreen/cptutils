/* autogenerated: do not edit */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>

#include "helper-model.h"

int helper_model(const struct gengetopt_args_info *info, cptcont_opt_t *opt)
{
  if (info->model_given)
    {
      const char *arg = info->model_arg;

      if (strcmp(arg, "rgb") == 0)
        opt->output.model = coerce_rgb;
      else if (strcmp(arg, "hsv") == 0)
        opt->output.model = coerce_hsv;
      else
        {
          fprintf(stderr, "unknown --model '%s'\n", arg);
          return 1;
        }
    }
  else
    opt->output.model = coerce_none;

  return 0;
}
