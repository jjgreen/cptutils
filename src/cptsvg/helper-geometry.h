/* autogenerated: do not edit */

#ifndef HELPER_GEOMETRY_H
#define HELPER_GEOMETRY_H

#include "cptsvg.h"
#include "options.h"

int helper_geometry(const struct gengetopt_args_info*, cptsvg_opt_t*);

#endif
