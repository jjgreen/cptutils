#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gimpsvg.h"

#include <cptutils/ggr-read.h>
#include <cptutils/svg-write.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define ERR_SEGMENT_RGBA 1
#define ERR_NULL 2
#define ERR_INSERT 3
#define ERR_COMPLETE 4

static int gimpsvg_convert(ggr_t*, svg_t*, const gimpsvg_opt_t*);

static int gimpsvg3(ggr_t *gradient,
		    svg_t *svg,
		    const gimpsvg_opt_t *opt)
{
  int err = gimpsvg_convert(gradient, svg, opt);

  if (err != 0)
    {
      switch (err)
	{
	case ERR_SEGMENT_RGBA:
	  btrace("error getting colour from segment");
	  break;
	case ERR_NULL:
	  btrace("null structure");
	  break;
	case ERR_INSERT:
	  btrace("failed structure insert");
	  break;
        case ERR_COMPLETE:
          btrace("failed SVG completion");
          break;
	default:
	  btrace("unknown error");
	}
      return 1;
    }

  if (opt->verbose)
    printf("converted to %i stops\n", svg_num_stops(svg));

  return 0;
}

static int gimpsvg2(ggr_t *gradient, const gimpsvg_opt_t *opt)
{
  int err = 1;
  svg_t *svg;

  if ((svg = svg_new()) != NULL)
    {
      if (gimpsvg3(gradient, svg, opt) == 0)
        {
          if (comment_process(svg->comment, &(opt->comment)) == 0)
            {
              const char *path = opt->output.path;
              const svg_preview_t *preview = &(opt->preview);
              svg_version_t version = opt->svg_version;

              if (svg_write(svg, path, preview, version) == 0)
                err = 0;
              else
                btrace("failed write to %s", IFNULL(path, "<stdout>"));
            }
          else
            btrace("failed to process comments");
        }
      else
        btrace("failed convert");

      svg_destroy(svg);
    }
  else
    btrace("failed new svg");

  return err;
}

int gimpsvg(const gimpsvg_opt_t *opt)
{
  ggr_t *gradient = ggr_read(opt->input.path);

  if (gradient == NULL)
    {
      btrace("failed to load gradient from %s",
             IFNULL(opt->input.path, "<stdin>"));
      return 1;
    }

  if (! ggr_valid(gradient))
    {
      btrace("input gradient is invalid");
      return 1;
    }

  int err = gimpsvg2(gradient, opt);

  ggr_destroy(gradient);

  return err;
}

#define EPSRGB   (0.5 / 256.0)
#define EPSALPHA 1e-4

static int grad_segment_jump(ggr_segment_t *left, ggr_segment_t *right)
{
  return ( (fabs(left->r1 - right->r0) > EPSRGB) ||
	   (fabs(left->g1 - right->g0) > EPSRGB) ||
	   (fabs(left->b1 - right->b0) > EPSRGB) ||
	   (fabs(left->a1 - right->a0) > EPSALPHA) );
}

static int gimpsvg_convert(ggr_t *grad,
			   svg_t *svg,
			   const gimpsvg_opt_t *opt)
{
  if (!grad) return 1;

  strncpy((char *)svg->name, grad->name, SVG_NAME_LEN-1);

  svg_stop_t stop;
  double rgbD[3], alpha;
  ggr_segment_t *gseg;

  for (gseg = grad->segments ; gseg ; gseg = gseg->next)
    {
      /* always insert the left colour */

      if (ggr_segment_rgba(gseg->left, gseg, rgbD, &alpha) != 0)
	return ERR_SEGMENT_RGBA;

      rgbD_to_rgb(rgbD, &stop.colour);

      stop.value = 100.0 * gseg->left;
      stop.opacity = alpha;

      if (svg_append(stop,svg) != 0) return ERR_INSERT;

      /* insert interior segments */

      if ((gseg->type == GGR_LINEAR) && (gseg->color == GGR_RGB))
	{
	  if (ggr_segment_rgba(gseg->middle, gseg, rgbD, &alpha) != 0)
	    return ERR_SEGMENT_RGBA;

	  rgbD_to_rgb(rgbD, &stop.colour);

	  stop.value = 100.0 * gseg->middle;
	  stop.opacity = alpha;

	  if (svg_append(stop, svg) != 0) return ERR_INSERT;
	}
      else
	{
	  /*
	    when the segment is non-linear and/or is not RGB, we
	    divide the segment up into small subsegments and write
	    the linear approximations.
	  */

	  double width = gseg->right - gseg->left;
	  size_t m = opt->samples * width + 1;

	  for (size_t i = 1 ; i < m ; i++)
	    {
	      double z = gseg->left + i * width / m;

	      if (ggr_segment_rgba(z, gseg, rgbD, &alpha) != 0)
		return ERR_SEGMENT_RGBA;

	      rgbD_to_rgb(rgbD, &stop.colour);

	      stop.value   = 100.0 * z;
	      stop.opacity = alpha;

	      if (svg_append(stop, svg) != 0)
                return ERR_INSERT;
	    }
	}

      /*
	insert right stop if it is not the same as the
	left colour of the next segment
      */

      if ((! gseg->next) || grad_segment_jump(gseg,gseg->next))
	{
	  if (ggr_segment_rgba(gseg->right, gseg, rgbD, &alpha) != 0)
	    return ERR_SEGMENT_RGBA;

	  rgbD_to_rgb(rgbD, &stop.colour);

	  stop.value   = 100.0 * gseg->right;
	  stop.opacity = alpha;

	  if (svg_append(stop, svg) != 0)
            return ERR_INSERT;
	}
    }

  if (svg_complete(svg) != 0)
    return ERR_COMPLETE;

  return 0;
}
