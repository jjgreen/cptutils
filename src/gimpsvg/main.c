#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-geometry.h"
#include "helper-input.h"
#include "helper-output.h"
#include "helper-svg2.h"
#include "gimpsvg.h"

#include <cptutils/colour.h>
#include <cptutils/svg.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SAMPLES_MIN 5

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  gimpsvg_opt_t opt = {
    .verbose = info->verbose_flag,
    .reverse = false
  };

  if ((opt.samples = info->samples_arg) < SAMPLES_MIN)
    {
      fprintf(stderr, "at least %i samples required\n", SAMPLES_MIN);
      return EXIT_FAILURE;
    }

  if ((helper_geometry(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0) ||
      (helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_svg2(info, &opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is gimpsvg (version %s)\n", VERSION);

  btrace_enable("gimpsvg");

  int err;

  if ((err = gimpsvg(&opt)) != 0)
    helper_btrace(info);
  else if (opt.verbose)
    {
      printf("palette written to %s\n", opt.output.path);
      if (opt.preview.use)
	{
	  printf("with preview (%zu x %zu px)\n",
		 opt.preview.width,
		 opt.preview.height);
	}
    }

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
