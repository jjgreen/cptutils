#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "qgssvg.h"

#include <cptutils/qgs-list.h>
#include <cptutils/qgs-list-read.h>
#include <cptutils/svg-list.h>
#include <cptutils/svg-list-write.h>
#include <cptutils/comment-list-process.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <string.h>

static double clamp(double min, double max, double v)
{
  if (v > max)
    return max;
  if (v < min)
    return min;
  return v;
}

static double qgssvg_value(double v)
{
  return clamp(0, 100, 100 * v);
}

static double qgssvg_opacity(double v)
{
  return clamp(0, 1, v / 255);
}

static int qgssvg_one(const qgs_t *qgs, svg_t *svg)
{
  /*
    The presence and value of the "discrete" attribute gives
    the type, it would seem that QGS_TYPE_INTERPOLATED is the
    default so acts the same as QGS_TYPE_UNSET.
  */

  switch (qgs->type)
    {
    case QGS_TYPE_DISCRETE:
      btrace("QGS discrete not handled");
      return 1;
    case QGS_TYPE_UNSET:
    case QGS_TYPE_INTERPOLATED:
      break;
    default:
      btrace("QGS type %i unknown", qgs->type);
    }

  if (qgs->n == 0)
    {
      btrace("QGS has no stops");
      return 1;
    }

  for (size_t i = 0 ; i < qgs->n ; i++)
    {
      const qgs_entry_t *entry = qgs->entries + i;
      svg_stop_t stop = {
        .colour = entry->rgb,
        .value = qgssvg_value(entry->value),
        .opacity = qgssvg_opacity(entry->opacity)
      };

      if (svg_append(stop, svg) != 0)
        {
          btrace("failed to append stop %zu", i);
          return 1;
        }
    }

  strncpy((char*)(svg->name), qgs->name, SVG_NAME_LEN - 1);

  if (svg_complete(svg) != 0)
    {
      btrace("failed SVG completion");
      return 1;
    }

  if (comment_copy(qgs->comment, svg->comment) != 0)
    {
      btrace("failed to copy comment");
      return 1;
    }

  return 0;
}

static int qgssvg_iterand(qgs_t *qgs, void *arg)
{
  svg_list_t *svg_list = arg;
  svg_t *svg;
  int err = 1;

  if ((svg = svg_list_next(svg_list)) != NULL)
    {
      if (qgssvg_one(qgs, svg) == 0)
        err = 0;
      else
        {
          if (svg_list_revert(svg_list) == 0)
            err = 0;
          else
            btrace("failed svg-list revert");
        }
    }
  else
    btrace("failed svg-list next");

  return err;
}

static svg_list_t* qgssvg_convert(qgs_list_t *qgs)
{
  svg_list_t *svg;

  if ((svg = svg_list_new()) != NULL)
    {
      if (qgs_list_each(qgs, qgssvg_iterand, svg) == 0)
        return svg;

      svg_list_destroy(svg);
    }

  return NULL;
}

/*
  There's quite a bit of work needed to process comments in a
  multiple-gradient file format, since some of the actions need
  to be on the whole collection of comments (read, write), so
  we iterate over the qgs_list and extract the comments into a
  comments_list, process that collection, then write the results
  back into the qgs_list
*/

typedef struct
{
  size_t n;
  const comment_list_t *comment_list;
} set_comment_t;

static int f_set_comment(qgs_t *qgs, void *varg)
{
  set_comment_t *arg = varg;
  const comment_t *comment;
  int err = 1;

  if ((comment = comment_list_entry(arg->comment_list, arg->n)) != NULL)
    {
      arg->n++;
      comment_deinit(qgs->comment);
      if (comment_copy(comment, qgs->comment) == 0)
        err = 0;
    }

  return err;
}

static int qgs_list_set_comment(qgs_list_t *qgs_list,
                                const comment_list_t* comment_list)
{
  set_comment_t arg = { .n = 0, .comment_list = comment_list };
  return qgs_list_each(qgs_list, f_set_comment, &arg);
}

static int f_get_comment(qgs_t *qgs, void *varg)
{
  comment_list_t *comment_list = varg;
  comment_t *comment;
  int err = 1;

  if ((comment = comment_list_next(comment_list)) != NULL)
    {
      if (comment_copy(qgs->comment, comment) == 0)
        err = 0;
      else
        comment_list_revert(comment_list);
    }

  return err;
}

static comment_list_t* qgs_list_get_comment(qgs_list_t *qgs_list)
{
  comment_list_t *comment_list = comment_list_new();

  if (comment_list != NULL)
    {
      if (qgs_list_each(qgs_list, f_get_comment, comment_list) == 0)
        return comment_list;

      comment_list_destroy(comment_list);
    }

  return NULL;
}

static int qgs_list_comment(qgs_list_t *qgs_list, const qgssvg_opt_t *opt)
{
  if (opt->comment.action == action_retain)
    return 0;

  comment_list_t *comment_list;
  int err = 1;

  if ((comment_list = qgs_list_get_comment(qgs_list)) != NULL)
    {
      if (comment_list_process(comment_list, &(opt->comment)) == 0)
        {
          if (qgs_list_set_comment(qgs_list, comment_list) == 0)
            err = 0;
          else
            btrace("failed to set comment-list");
        }
      else
        btrace("failed to process comment-list");

      comment_list_destroy(comment_list);
    }
  else
    btrace("failed to create comment-list");

  return err;
}

int qgssvg(const qgssvg_opt_t *opt)
{
  qgs_list_t *qgs_list;
  int err = 1;

  if ((qgs_list = qgs_list_read(opt->input.path)) != NULL)
    {
      if (qgs_list_comment(qgs_list, opt) == 0)
        {
          svg_list_t *svg_list;

          if ((svg_list = qgssvg_convert(qgs_list)) != NULL)
            {
              size_t n = svg_list_size(svg_list);

              if (n > 0)
                {
                  const char *path = opt->output.path;
                  svg_preview_t preview = { .use = false };
                  svg_version_t version = opt->svg_version;

                  if (svg_list_write(svg_list, path, &preview, version) == 0)
                    {
                      err = 0;
                      if (opt->verbose)
                        printf("wrote %zu gradients to %s\n", n, path);
                    }
                  else
                    btrace("failed write to %s", IFNULL(path, "<stdout>"));

                  svg_list_destroy(svg_list);
                }
              else
                btrace("no gradients converted");
            }
          else
            btrace("failed convert");
        }
      else
        btrace("failed to process comments");

      qgs_list_destroy(qgs_list);
    }
  else
    btrace("failed read of %s", opt->input.path);

  return err;
}
