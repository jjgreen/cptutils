'\" t
.\"     Title: GRADIENT-CONVERT
.\"    Author: [see the "AUTHOR" section]
.\" Generator: DocBook XSL Stylesheets v1.79.2 <http://docbook.sf.net/>
.\"      Date:  7 November 2024
.\"    Manual: User commands
.\"    Source: cptutils 1.83
.\"  Language: English
.\"
.TH "GRADIENT\-CONVERT" "1" "7 November 2024" "cptutils 1\&.83" "User commands"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
gradient-convert \- convert gradients between formats
.SH "SYNOPSIS"
.HP \w'\fBgradient\-convert\fR\ 'u
\fBgradient\-convert\fR [\-b\ \fIrgb\fR] [\-B] [\-c] [\-C] [\-f\ \fIrgb\fR] [\-g\ \fIgeometry\fR] [\-G] [\-h] [\-i\ \fIformat\fR] [\-n\ \fIrgb\fR] [\-o\ \fIformat\fR] [\-p] [\-T\ \fIrgb\fR] [\-v] [\-V] [\-z] [\-4] [\-5] [\-6] \fIinfile\fR \fIoutfile\fR
.SH "DESCRIPTION"
.PP
The
\fBgradient\-convert\fR
program converts gradients to other formats\&. It is a wrapper script which make a number of calls the individual programs from the
cptutils
package so, for example, to convert a GMT colour palette (cpt) to a PNG image, the programs
\fBcptsvg\fR
and
\fBsvgpng\fR
would be called in sequence to generate the required file\&.
.PP
The formats of the mandatory
\fIinfile\fR
and
\fIoutfile\fR
arguments are determined from the file extensions, or can be specified explicitly by the
\fB\-i\fR
and
\fB\-o\fR
options\&.
.SH "OPTIONS"
.PP
In the following, all
\fIrgb\fR
specifications should be of the form
\fIred\fR/\fIgreen\fR/\fIblue\fR
where the colour components are integers in the range 0 to 255\&.
.PP
\fB\-b\fR, \fB\-\-background\fR \fIrgb\fR
.RS 4
Set the background colour of cpt output\&.
.sp
Note that this only modifies the "background" field in the output cpt file, it does not affect the transparency (see the
\fB\-\-transparency\fR
option in that regard)\&.
.RE
.PP
\fB\-\-backtrace\-file\fR \fIpath\fR
.RS 4
Specify a file to which to write a formatted backtrace\&. The file will only be created if there is a backtrace created, typically when an error occurs\&.
.RE
.PP
\fB\-\-backtrace\-format\fR \fIformat\fR
.RS 4
Specify the
\fIformat\fR
of the backtrace written to the files specified by
\fB\-\-backtrace\-file\fR, one of
\fBplain\fR,
\fBxml\fR
or
\fBjson\fR\&.
.RE
.PP
\fB\-B\fR, \fB\-\-burst\fR
.RS 4
Some of the file formats handled by
cptutils
may contain multiple gradients: the Photoshop (grd) and SVG formats for example\&. By default, the program will extract only the first gradient from such files; but when the
\fB\-\-burst\fR
option is selected then all gradients will be extracted\&. In this case the
\fIoutfile\fR
argument must be a directory (and that directory should already exist)\&.
.RE
.PP
\fB\-c\fR, \fB\-\-capabilities\fR
.RS 4
Print the capabilities of the program to
stdout\&. This data (in YAML format) may be used to autoconfigure other programs which wish to use this program\&. The format should be self\-explanatory\&.
.RE
.PP
\fB\-C\fR, \fB\-\-retain\-comments\fR
.RS 4
Use the comments in the input file as the comments for the output file\&.
.sp
This option will not error if comment retention is not possible (if one of the intermediate conversion targets does not support comments, for example)\&.
.RE
.PP
\fB\-f\fR, \fB\-\-foreground\fR \fIrgb\fR
.RS 4
Set the foreground colour of cpt output\&.
.RE
.PP
\fB\-g\fR, \fB\-\-geometry\fR \fIwidth\fRx\fIheight\fR
.RS 4
Specify the size of the PNG image or SVG preview in pixels\&.
.RE
.PP
\fB\-G\fR, \fB\-\-graphviz\fR
.RS 4
Print the conversion graph to
stdout
in the GraphViz dot format\&. See
\m[blue]\fB\fBdot\fR(1)\fR\m[]
for details on creating a plot of the graph from this output\&.
.RE
.PP
\fB\-h\fR, \fB\-\-help\fR
.RS 4
Brief help\&.
.RE
.PP
\fB\-i\fR, \fB\-\-input\-format\fR \fIformat\fR
.RS 4
Specify the format of the input file\&. Run the program with the
\fB\-\-help\fR
option for a list of supported formats\&.
.RE
.PP
\fB\-n\fR, \fB\-\-nan\fR \fIrgb\fR
.RS 4
Set the NaN (no data) colour of cpt output\&.
.RE
.PP
\fB\-o\fR, \fB\-\-output\-format\fR \fIformat\fR
.RS 4
Specify the format of the output file\&. Run the program with the
\fB\-\-help\fR
option for a list of supported formats\&.
.RE
.PP
\fB\-p\fR, \fB\-\-preview\fR
.RS 4
Include a preview in the SVG output\&. See also the
\fB\-\-geometry\fR
option\&.
.RE
.PP
\fB\-T\fR, \fB\-\-transparency\fR \fIrgb\fR
.RS 4
When converting to a format which does not support transparency, replace the transparency with the specified
\fIrgb\fR
colour\&.
.RE
.PP
\fB\-v\fR, \fB\-\-verbose\fR
.RS 4
Verbose operation\&.
.RE
.PP
\fB\-V\fR, \fB\-\-version\fR
.RS 4
Version information\&.
.RE
.PP
\fB\-z\fR, \fB\-\-zip\fR
.RS 4
For input formats which may contain multiple gradients (grd, svg) the
\fB\-\-zip\fR
extracts all of the gradients and zips them up\&. Thus it can be viewed as similar to the burst option (\fB\-\-burst\fR)\&. The zipfile to be created should be the final argument and when unzipped it will create a directory (containing the converted gradients) with the same name as the file with the
\&.zip
extension removed\&.
.RE
.PP
\fB\-4\fR, \fB\-\-gmt4\fR
.RS 4
Use GMT 4 conventions when writing the cpt output: the colour\-model code is uppercase, and the colours are separated by spaces\&.
.sp
This is incompatible with the
\fB\-5\fR
and
\fB\-6\fR
options of course\&.
.sp
At present this option is the default, but that will change at some point\&. So specify this option if your use of the output depends on the GMT 4 layout (consumed by a custom parser, for example)\&.
.RE
.PP
\fB\-5\fR, \fB\-\-gmt5\fR
.RS 4
Use GMT 5 conventions when writing the cpt output: the colour\-model code is lowercase, and the colours are separated by a solidus for RGB, CMYK, by a dash for HSV\&.
.sp
This is incompatible with the
\fB\-4\fR
and
\fB\-6\fR
options of course\&.
.RE
.PP
\fB\-6\fR, \fB\-\-gmt6\fR
.RS 4
As the
\fB\-5\fR
option, but allows the
\fBHARD_HINGE\fR
and
\fBSOFT_HINGE\fR
directives in place of the explicit
\fBHINGE =\fR
directive\&.
.sp
This is incompatible with the
\fB\-4\fR
and
\fB\-5\fR
options of course\&.
.RE
.SH "EXAMPLES"
.PP
Convert a GMT colour palette to a 200\(mu20 pixel PNG image:
.sp
.if n \{\
.RS 4
.\}
.nf
gradient\-convert \-v \-g 200x20 legend\&.cpt legend\&.png
.fi
.if n \{\
.RE
.\}
.PP
Convert all gradients in a Photoshop gradient to GMT cpt files in a zipfile:
.sp
.if n \{\
.RS 4
.\}
.nf
gradient\-convert \-v \-i grd \-o cpt \-z froob\&.grd froob\&.zip
.fi
.if n \{\
.RE
.\}
.SH "CAVEATS"
.PP
The component programs in the conversion will generally have a number of options available for modifying the conversion behaviour, but only the most\-often used of these options can be passed to component programs from
\fBgradient\-convert\fR\&.
.PP
Note also that those options which are passed to component programs are not checked for relevance\&. Converting a GMT cpt file to GIMP format while using using the irrelevant
\fB\-\-geometry\fR
option will not cause an error, it will simply be ignored\&.
.SH "AUTHOR"
.PP
J\&.J\&. Green
