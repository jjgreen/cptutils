#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptupdate.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/cpt-write.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

/*
  modify the read cpt according to the command-line options,
  so setting the hinge, (de)normalising, ...
*/

int cptupdate_opt(cpt_t *cpt, const cptupdate_opt_t *opt)
{
  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  if (opt->hinge.present)
    {
      cpt->hinge.present = true;
      cpt->hinge.type = opt->hinge.type;
    }

  if (opt->normalise)
    {
      if (cpt_normalise(cpt, true, 0) != 0)
        {
          btrace("normalising");
          return 1;
        }
    }

  if (opt->denormalise)
    {
      if (cpt_denormalise(cpt, true, 0) != 0)
        {
          btrace("denormalising");
          return 1;
        }
    }

  return 0;
}

/*
  modify the output cpt according to the write-options, i.e.,
  what we are able to write given the --gmt option.  For each
  possible cpt-type we modify it to be compatible with the
  output compabilities if possible.
*/

int cptupdate_hinge(cpt_t *cpt, const cptwrite_opt_t *opt)
{
  if (! cpt->hinge.present)
    return 0;

  switch (cpt->hinge.type)
    {
    case hinge_hard:
      if (opt->hinge.hard)
        {
          // OK
        }
      else if (opt->hinge.explicit)
        {
          cpt->hinge.type = hinge_explicit;
          cpt->hinge.value = 0;
        }
      else
        cpt->hinge.present = false;
      break;

    case hinge_soft:
      if (opt->hinge.soft)
        {
          // OK
        }
      else if (opt->hinge.explicit)
        {
          cpt->hinge.type = hinge_explicit;
          cpt->hinge.value = 0;
        }
      else
        cpt->hinge.present = false;
      break;

    case hinge_explicit:
      if (opt->hinge.explicit)
        {
          // OK
        }
      else if (opt->hinge.hard)
        {
          if (cpt->hinge.value != 0)
            {
              btrace("cannot update non-zero GMT5 hinge");
              return 1;
            }
          cpt->hinge.type = hinge_hard;
        }
      else
        cpt->hinge.present = false;
      break;
    }

  return 0;
}

int cptupdate(const cptupdate_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cptupdate_opt(cpt, opt) == 0)
        {
          if (cpt_coerce_model(cpt, opt->output.model) == 0)
            {
              if (comment_process(cpt->comment, &(opt->comment)) == 0)
                {
                  int version = opt->gmt_version;
                  cptwrite_opt_t write_opt;

                  if (cpt_write_options(version, &write_opt) == 0)
                    {
                      if (cptupdate_hinge(cpt, &write_opt) == 0)
                        {
                          if (cpt_write(cpt, &write_opt, opt->output.path) == 0)
                            err = 0;
                          else
                            btrace("error writing cpt struct");
                        }
                      else
                        btrace("converting hinge");
                    }
                  else
                    btrace("bad GMT version %i", version);
                }
              else
                btrace("failed to process comments");
            }
          else
            btrace("failed to coerce colour model in output");
        }

      cpt_destroy(cpt);
    }
  else
    btrace("load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("write cpt to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}
