#ifndef CPTUPDATE_H
#define CPTUPDATE_H

#include <cptutils/cpt-fill.h>
#include <cptutils/cpt-hinge.h>
#include <cptutils/cpt-coerce.h>
#include <cptutils/comment.h>

#include <stdbool.h>
#include <stddef.h>

typedef struct
{
  bool verbose, normalise, denormalise;
  int gmt_version;
  comment_opt_t comment;
  struct
  {
    bool present;
    cpt_hinge_type_t type;
  } hinge;
  struct
  {
    const char *path;
  } input;
  struct
  {
    cpt_coerce_model_t model;
    const char *path;
  } output;
} cptupdate_opt_t;

int cptupdate(const cptupdate_opt_t*);

#endif
