#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-gmt-ver.h"
#include "helper-input.h"
#include "helper-model.h"
#include "helper-normalise.h"
#include "helper-output.h"
#include "cptupdate.h"

#include <cptutils/btrace.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
  this helper is different in behaviour and only used here, so
  we make it static
*/

static int helper_hinge(const struct gengetopt_args_info *info,
                        cptupdate_opt_t *opt)
{
  if (info->hinge_hard_flag && info->hinge_soft_flag)
    {
      fprintf(stderr, "cannot have --hinge-hard and --hinge-soft\n");
      return 1;
    }

  if (info->hinge_hard_flag)
    {
      opt->hinge.present = true;
      opt->hinge.type = hinge_hard;
    }
  else if (info->hinge_soft_flag)
    {
      opt->hinge.present = true;
      opt->hinge.type = hinge_soft;
    }
  else
    opt->hinge.present = false;

  return 0;
}

/*
  specific behaviour for the update action, if the user has
  selected bad options, we error, else we set some options
  to be compatible with that update
*/

static int helper_update4(cptupdate_opt_t *opt)
{
  if (opt->hinge.present)
    {
      fprintf(stderr, "a GMT4 cpt cannot have a hinge");
      return 1;
    }

  if (opt->normalise)
    {
      fprintf(stderr, "a GMT4 cpt cannot be ramge-normalise\n");
      return 1;
    }

  opt->denormalise = true;

  return 0;
}

static int helper_update5(cptupdate_opt_t *opt)
{
  if (opt->hinge.present)
    {
      switch (opt->hinge.type)
        {
        case hinge_hard:
          fprintf(stderr, "a GMT5 cpt cannot have a hard hinge\n");
          return 1;
        case hinge_soft:
          fprintf(stderr, "a GMT5 cpt cannot have a soft hinge\n");
          return 1;
        case hinge_explicit:
          break;
        }
    }

  opt->gmt_version = 5;

  return 0;
}

static int helper_update6(cptupdate_opt_t *opt)
{
  if (opt->hinge.present)
    {
      switch (opt->hinge.type)
        {
        case hinge_hard:
        case hinge_soft:
          break;

        case hinge_explicit:

          /*
            not actually reachable at present since there is no way
            to set an explicit hinge from the command-line, we may
            add that later
          */

          fprintf(stderr, "a GMT5 cpt cannot have an explicit hinge\n");
          return 1;
        }
    }

  opt->gmt_version = 6;

  return 0;
}

static int helper_update(cptupdate_opt_t *opt)
{
  int err;

  switch (opt->gmt_version)
    {
    case 0:
    case 6:
      err = helper_update6(opt);
      break;
    case 5:
      err = helper_update5(opt);
      break;
    case 4:
      err = helper_update4(opt);
      break;
    default:
      err = 1;
    }

  return err;
}

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  cptupdate_opt_t opt = {
    .verbose = info->verbose_flag
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_gmt_ver(info, &opt) != 0) ||
      (helper_hinge(info, &opt) != 0) ||
      (helper_model(info, &opt) != 0) ||
      (helper_normalise(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0) ||
      (helper_update(&opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is cptupdate (version %s)\n", VERSION);

  btrace_enable("cptupdate");

  int err;

  if ((err = cptupdate(&opt)) != 0)
    helper_btrace(info);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
