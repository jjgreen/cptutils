#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-input.h"
#include "helper-output.h"
#include "cptpg.h"

#include <cptutils/btrace.h>
#include <cptutils/colour.h>
#include <cptutils/macro.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  cptpg_opt_t opt = {
    .verbose = info->verbose_flag,
    .percentage = info->percentage_flag,
    .hinge = {
      .active = info->hinge_active_flag
    }
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is cptpg (version %s)\n", VERSION);

  btrace_enable("cptpg");

  int err;

  if ((err = cptpg(&opt)) != 0)
    helper_btrace(info);
  else if (opt.verbose)
    printf("gradient written to %s\n", opt.output.path);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
