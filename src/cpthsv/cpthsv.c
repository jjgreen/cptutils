#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cpthsv.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-write.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/cpt-coerce.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


static double clamp_unit(double a)
{
  a = (a < 0 ? 0 : a);
  a = (a > 1 ? 1 : a);

  return a;
}

/*
  coerce hue1 to be in [0, 360] by adding/subtracting multiples
  of 360, and do the same to hue2 (that may mean it is outside
  [0, 360]
*/

static void hue_modulo_pair(double *p1, double *p2)
{
  double
    hue1 = *p1,
    hue2 = *p2;

  int n = floor(hue1 / 360);

  hue1 -= 360 * n;
  hue2 -= 360 * n;

  *p1 = hue1;
  *p2 = hue2;
}

static void hue_modulo(double *p)
{
  double hue = *p;

  hue -= 360 * floor(hue / 360);

  *p = hue;
}

static void double_transform(double *x, hsvtrans_t t)
{
  switch (t.op)
    {
    case times: *x *= t.z; break;
    case plus: *x += t.z; break;
    case minus: *x -= t.z; break;
    case percent: *x *= (t.z / 100); break;
    }
}

/*
  the hue transform may take the value outside [0, 360] which is
  not a valid cpt HSV, the caller is expected to repair those ...
*/

static int hsv_transform(hsv_t *hsv, hsvtrans_t t)
{
  switch (t.channel)
    {
    case hue:
      double_transform(&(hsv->hue), t);
      break;
    case saturation:
      double_transform(&(hsv->sat), t);
      hsv->sat = clamp_unit(hsv->sat);
      break;
    case value:
      double_transform(&(hsv->val), t);
      hsv->val = clamp_unit(hsv->val);
      break;
    }

  return 0;
}

static int hsv_transforms(hsv_t *hsv, const cpthsv_opt_t *opt)
{
  for (size_t i = 0 ; i < opt->n ; i++)
    if (hsv_transform(hsv, opt->tran[i]) != 0)
      {
        btrace("transform %i", i + 1);
        return 1;
      }

  return 0;
}

static void annote_split(cpt_seg_t *seg1, cpt_seg_t *seg2)
{
  switch (seg1->annote)
    {
    case annote_none:
      seg1->annote = annote_none;
      seg2->annote = annote_none;
      break;
    case annote_upper:
      seg1->annote = annote_none;
      seg2->annote = annote_upper;
      break;
    case annote_lower:
      seg1->annote = annote_lower;
      seg2->annote = annote_none;
      break;
    case annote_both:
      seg1->annote = annote_lower;
      seg2->annote = annote_upper;
      break;
    }
}

static void seg_insert(cpt_seg_t *seg1, cpt_seg_t *seg2, cpt_seg_t *seg3)
{
  seg2->right = seg3;
  seg2->left = seg1;

  if (seg1)
    seg1->right = seg2;

  if (seg3)
    seg3->left = seg2;
}

static void seg_insert_right(cpt_seg_t *seg1, cpt_seg_t *seg2)
{
  seg_insert(seg1, seg2, seg1->right);
}

static cpt_seg_t* hsv_seg_transform(cpt_seg_t *seg,
                                    model_t interp_model,
                                    const cpthsv_opt_t *opt)
{
  hsv_t
    *left_hsv = &(seg->sample.left.fill.colour.hsv),
    *right_hsv = &(seg->sample.right.fill.colour.hsv);

  if ((hsv_transforms(left_hsv, opt) != 0) ||
      (hsv_transforms(right_hsv, opt) != 0))
    return NULL;

  hue_modulo_pair(&(left_hsv->hue), &(right_hsv->hue));

  /*
    The hue of the left colour is now transformed and in [0, 360],
    the hue of the right likewise, but need not be in [0, 360],

    If we are interpolating in RGB, then we just need to modulo the
    right into [0, 360], but in HSV we need to split the segment into
    two.  To do this, we create a new segment (seg2) and make it the
    "right half" of the split, the existing segment (seg1) is adjusted
    to become the left-hand side.

    Note that we return the right-hand segment so that the caller can
    adjust its pointer to the "current" segment in the case of such a
    split.
  */

  if (interp_model == model_rgb)
    {
      hue_modulo(&right_hsv->hue);
      return seg;
    }
  else if (interp_model == model_hsv)
    {
      if (right_hsv->hue > 360)
        {
          cpt_seg_t *seg1 = seg, *seg2;

          if ((seg2 = cpt_seg_new()) == NULL)
            return NULL;

          seg_insert_right(seg1, seg2);

          hsv_t
            hsv0 = *left_hsv,
            hsv1,
            hsv2 = *right_hsv;
          double
            p = (360 - hsv0.hue) / (hsv2.hue - hsv0.hue),
            z0 = seg1->sample.left.val,
            z2 = seg1->sample.right.val,
            z1 = z0 + p * (z2 - z0);

          if (hsv_interpolate(model_hsv, p, hsv0, hsv2, &hsv1) != 0)
            {
              btrace("bad hsv_interpolate");
              return NULL;
            }

          seg2->sample.left.val = z1;
          seg2->sample.left.fill.type = cpt_fill_colour_hsv;
          seg2->sample.left.fill.colour.hsv.hue = 0;
          seg2->sample.left.fill.colour.hsv.sat = hsv1.sat;
          seg2->sample.left.fill.colour.hsv.val = hsv1.val;

          seg2->sample.right = seg1->sample.right;
          seg2->sample.right.fill.colour.hsv.hue -= 360;

          seg1->sample.right.val = z1;
          seg1->sample.right.fill.colour.hsv.hue = 360;
          seg1->sample.right.fill.colour.hsv.sat = hsv1.sat;
          seg1->sample.right.fill.colour.hsv.val = hsv1.val;

          annote_split(seg1, seg2);

          return seg2;
        }
      else if (right_hsv->hue < 0)
        {
          cpt_seg_t *seg1 = seg, *seg2;

          if ((seg2 = cpt_seg_new()) == NULL)
            return NULL;

          seg_insert_right(seg1, seg2);

          hsv_t
            hsv0 = *left_hsv,
            hsv1,
            hsv2 = *right_hsv;
          double
            p = hsv0.hue / (hsv0.hue - hsv2.hue),
            z0 = seg1->sample.left.val,
            z2 = seg1->sample.right.val,
            z1 = z0 + p * (z2 - z0);

          if (hsv_interpolate(model_hsv, p, hsv0, hsv2, &hsv1) != 0)
            {
              btrace("bad hsv_interpolate");
              return NULL;
            }

          seg2->sample.left.val = z1;
          seg2->sample.left.fill.type = cpt_fill_colour_hsv;
          seg2->sample.left.fill.colour.hsv.hue = 360;
          seg2->sample.left.fill.colour.hsv.sat = hsv1.sat;
          seg2->sample.left.fill.colour.hsv.val = hsv1.val;

          seg2->sample.right = seg1->sample.right;
          seg2->sample.right.fill.colour.hsv.hue += 360;

          seg1->sample.right.val = z1;
          seg1->sample.right.fill.colour.hsv.hue = 0;
          seg1->sample.right.fill.colour.hsv.sat = hsv1.sat;
          seg1->sample.right.fill.colour.hsv.val = hsv1.val;

          annote_split(seg1, seg2);

          return seg2;
        }
      else
        return seg;
    }
  else
    {
      btrace("unsupported interpolation model");
      return NULL;
    }
}

static int rgb_seg_hsv(cpt_seg_t *seg)
{
  cpt_fill_t
    *lfill = &(seg->sample.left.fill),
    *rfill = &(seg->sample.right.fill);

  if ((rgb_to_hsv(lfill->colour.rgb, &(lfill->colour.hsv)) != 0) ||
      (rgb_to_hsv(rfill->colour.rgb, &(rfill->colour.hsv)) != 0))
    return 1;

  lfill->type = cpt_fill_colour_hsv;
  rfill->type = cpt_fill_colour_hsv;

  return 0;
}

static int hsv_seg_rgb(cpt_seg_t *seg)
{
  cpt_fill_t
    *lfill = &(seg->sample.left.fill),
    *rfill = &(seg->sample.right.fill);

  if ((hsv_to_rgb(lfill->colour.hsv, &(lfill->colour.rgb)) != 0) ||
      (hsv_to_rgb(rfill->colour.hsv, &(rfill->colour.rgb)) != 0))
    return 1;

  lfill->type = cpt_fill_colour_rgb;
  rfill->type = cpt_fill_colour_rgb;

  return 0;
}

/*
  input is a single rgb segment, convert it to hsv and then call
  hsv_seg_transform -- that may (if the intepolation model is hsv)
  split the transformed segment in two, returning the new segment;
  in that case we must convert both of them to rgb
*/

static cpt_seg_t* rgb_seg_transform(cpt_seg_t *seg,
                                    model_t interp_model,
                                    const cpthsv_opt_t *opt)
{
  if (rgb_seg_hsv(seg) != 0)
    {
      btrace("failed segment rgb to hsv");
      return NULL;
    }

  cpt_seg_t *seg0 = hsv_seg_transform(seg, interp_model, opt);

  if (seg0 == NULL)
    {
      btrace("failed call to HSV transform");
      return NULL;
    }

  if (seg0 != seg)
    {
      if (hsv_seg_rgb(seg) != 0)
        {
          btrace("failed segment hsv to rgb");
          return NULL;
        }
    }

  if (hsv_seg_rgb(seg0) != 0)
    {
      btrace("failed segment hsv to rgb");
      return NULL;
    }

  return seg0;
}

static int cpthsv_convert(cpt_t *cpt, const cpthsv_opt_t *opt)
{
  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  if (opt->normalise)
    {
      if (cpt_normalise(cpt, opt->hinge.active, opt->hinge.value) != 0)
        {
          btrace("normalising");
          return 1;
        }
    }

  if (opt->denormalise)
    {
      if (cpt_denormalise(cpt, opt->hinge.active, opt->hinge.value) != 0)
        {
          btrace("denormalising");
          return 1;
        }
    }

  model_t interp = cpt->interpolate;
  cpt_seg_t *seg;
  int n, m;

  for (n = 0, m = 0, seg = cpt->segment ; seg ; seg = seg->right, n++)
    {
      if (seg->sample.left.fill.type != seg->sample.right.fill.type)
        {
          btrace("can't convert mixed fill types");
          return 1;
        }

      switch (seg->sample.left.fill.type)
        {
        case cpt_fill_colour_rgb:
          if ((seg = rgb_seg_transform(seg, interp, opt)) == NULL)
            {
              btrace("segment %zu", n + 1);
              return 1;
            }
          m++;
          break;

        case cpt_fill_colour_hsv:
          if ((seg = hsv_seg_transform(seg, interp, opt)) == NULL)
            {
              btrace("segment %zu", n + 1);
              return 1;
            }
          m++;
          break;

        default:
          break;
	}
    }

  if (opt->verbose)
    printf("transformed %i/%i segments\n", m, n);

  return 0;
}

int cpthsv(const cpthsv_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cpthsv_convert(cpt, opt) == 0)
        {
          if (cpt_coerce_model(cpt, opt->output.model) == 0)
            {
              if (comment_process(cpt->comment, &(opt->comment)) == 0)
                {
                  cptwrite_opt_t write_opt;

                  if (cpt_write_options(opt->gmt_version, &write_opt) == 0)
                    {
                      if (cpt_write(cpt, &write_opt, opt->output.path) == 0)
                        err = 0;
                      else
                        btrace("error writing cpt struct");
                    }
                  else
                    btrace("bad GMT version %i", opt->gmt_version);
                }
              else
                btrace("failed to process comments");
            }
          else
            btrace("failed to coerce colour model in output");
        }
      else
        btrace("convert");

      cpt_destroy(cpt);
    }
  else
    btrace("load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("write cpt to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}
