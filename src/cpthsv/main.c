#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-gmt-ver.h"
#include "helper-input.h"
#include "helper-model.h"
#include "helper-normalise.h"
#include "helper-output.h"
#include "cpthsv.h"

#include <cptutils/colour.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static int parse_transform(char *T, hsvtrans_t *t, cpthsv_opt_t *opt)
{
  char cc, co;
  double z;

  switch (sscanf(T, "%c%lf%c", &cc, &z, &co))
    {
    case 2:
      co = '%';
      /* fallthrough */
    case 3:

      t->z = z;

      switch (tolower(cc))
	{
	case 'h': t->channel = hue; break;
	case 's': t->channel = saturation; break;
	case 'v': t->channel = value; break;
	default:
	  fprintf(stderr, "bad channel %c\n", *T);
	  return 1;
	}

      switch (tolower(co))
	{
	case '%': t->op = percent; break;
	case 'x': t->op = times; break;
	case '+': t->op = plus; break;
	case '-': t->op = minus; break;
	default:
	  fprintf(stderr, "bad operation %c\n", *T);
	  return 1;
	}

      break;

    default:
      return 1;
    }

  return 0;
}

static int parse_transforms(const char *T, cpthsv_opt_t *opt)
{
  int n = 1;
  char *v;
  const char *c;

  for (c = T ; *c ; c++) n += (*c == ',');

  char *Tdup = strdup(T);
  hsvtrans_t *t = calloc(n, sizeof(hsvtrans_t));

  v = strtok(Tdup, ",");
  if (parse_transform(v, t, opt) != 0)
    goto failed;

  for (size_t i = 1 ; i < n ; i++)
    {
      v = strtok(NULL, ",");
      if (parse_transform(v, t + i, opt) != 0)
        goto failed;
    }

  free(Tdup);

  if (opt->verbose)
    printf("read %i transforms\n", n);

  opt->n = n;
  opt->tran = t;

  return 0;

 failed:

  fprintf(stderr, "failed parse of %s\n", v);

  free(Tdup);
  free(t);

  return 1;
}

static void free_transforms(cpthsv_opt_t *opt)
{
  free(opt->tran);
}

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  if (! info->transform_given)
    {
      fprintf(stderr, "--transform (-T) option required\n");
      return EXIT_FAILURE;
    }

  cpthsv_opt_t opt = {
    .verbose = info->verbose_flag,
    .hinge = {
      .active = info->hinge_active_flag,
      .value = info->hinge_arg
    }
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_gmt_ver(info, &opt) != 0) ||
      (helper_model(info, &opt) != 0) ||
      (helper_normalise(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0))
    return EXIT_FAILURE;

  if (parse_transforms(info->transform_arg, &opt) != 0)
    {
      fprintf(stderr, "failed to parse transform %s\n",
	      info->transform_arg);
      return EXIT_FAILURE;
    }

  if (opt.verbose)
    printf("This is cpthsv (version %s)\n", VERSION);

  btrace_enable("cpthsv");

  int err;

  if ((err = cpthsv(&opt)) != 0)
    helper_btrace(info);
  else if (opt.verbose)
    printf("gradient written to %s\n", opt.output.path);

  btrace_reset();
  btrace_disable();

  free_transforms(&opt);

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
