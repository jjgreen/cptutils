#ifndef CPTHSV_H
#define CPTHSV_H

#include <stdbool.h>

#include <cptutils/cpt-coerce.h>
#include <cptutils/comment.h>

typedef enum {
  hue,
  saturation,
  value
} hsvchan_t;

typedef enum {
  plus,
  minus,
  times,
  percent
} hsvop_t;

typedef struct
{
  double z;
  hsvchan_t channel;
  hsvop_t op;
} hsvtrans_t;

typedef struct
{
  int n;
  hsvtrans_t *tran;
  int gmt_version;
  comment_opt_t comment;
  bool verbose, normalise, denormalise;
  struct
  {
    bool active;
    double value;
  } hinge;
  struct
  {
    const char *path;
  } input;
  struct
  {
    cpt_coerce_model_t model;
    const char *path;
  } output;
} cpthsv_opt_t;

int cpthsv(const cpthsv_opt_t*);

#endif
