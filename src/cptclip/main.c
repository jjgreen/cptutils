#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-gmt-ver.h"
#include "helper-input.h"
#include "helper-model.h"
#include "helper-normalise.h"
#include "helper-output.h"
#include "cptclip.h"

#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  if (! info->range_given)
    {
      fprintf(stderr, "--range (-R) option required\n");
      return EXIT_FAILURE;
    }

  cptclip_opt_t opt = {
    .verbose = info->verbose_flag,
    .segments = info->segments_flag,
    .hinge = {
      .active = info->hinge_active_flag,
      .value = info->hinge_arg
    }
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_gmt_ver(info, &opt) != 0) ||
      (helper_model(info, &opt) != 0) ||
      (helper_normalise(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is cptclip (version %s)\n", VERSION);

  if (opt.segments)
    {
      if (sscanf(info->range_arg,
                 "%zu/%zu",
		 &opt.segs.min,
		 &opt.segs.max) != 2)
	{
	  fprintf(stderr, "bad range argument %s\n", info-> range_arg);
	  return EXIT_FAILURE;
	}

      if ((opt.segs.min > opt.segs.max) ||
	  ( opt.segs.min < 1 ) ||
	  ( opt.segs.max < 1 ))
	{
	  fprintf(stderr, "bad segment selection %zu - %zu\n",
		  opt.segs.min, opt.segs.max);
	  return EXIT_FAILURE;
	}

      if (opt.verbose)
	{
	  printf("extracting segments %zu to %zu\n",
		 opt.segs.min, opt.segs.max);
	}
    }
  else
    {
      if (sscanf(info->range_arg,
                 "%lf/%lf",
		 &opt.z.min,
		 &opt.z.max) != 2)
	{
	  fprintf(stderr, "bad range argument %s\n", info->range_arg);
	  return EXIT_FAILURE;
	}

      if (opt.z.min >= opt.z.max)
	{
	  fprintf(stderr, "bad range %g - %g\n",
		  opt.z.min, opt.z.max);
	  return EXIT_FAILURE;
	}

      if (opt.verbose)
	{
	  printf("extracting z-range %g to %g\n",
		 opt.z.min, opt.z.max);
	}
    }

  btrace_enable("cptclip");

  int err;

  if ((err = cptclip(&opt)) != 0)
    helper_btrace(info);
  else if (opt.verbose)
    printf("gradient written to %s\n", opt.output.path);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
