#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#include <cptutils/btrace.h>

#include "options.h"
#include "helper-btrace.h"
#include "helper-output.h"
#include "helper-comment.h"
#include "svgx.h"

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  svgx_opt_t opt = {
    .verbose = info->verbose_flag,
    .permissive = ! info->strict_flag,
    .id = NULL
  };

  if (info->all_flag + info->list_flag + info->select_given > 1)
    {
      fprintf(stderr, "only one of --all, --list and --select allowed\n");
      return EXIT_FAILURE;
    }

  if (info->all_flag)
    opt.job = job_all;
  else if (info->list_flag)
    opt.job = job_list;
  else if (info->select_given)
    {
      opt.job = job_id;
      opt.id = info->select_arg;
    }
  else
    opt.job = job_first;

  if (info->type_given)
    {
      const char *name = info->type_arg;
      struct {
        const char *name;
        int type;
      } *p, types[] = {
        {"c3g", type_css3},
        {"cpt", type_cpt},
        {"css3", type_css3},
        {"ds9", type_sao},
        {"ggr", type_ggr},
        {"gimp", type_ggr},
        {"gnuplot", type_gpt},
        {"gpt", type_gpt},
        {"inc", type_pov},
        {"jgd", type_grd3},
        {"map", type_map},
        {"pg", type_pg},
        {"pov", type_pov},
        {"png", type_png},
        {"psp", type_grd3},
        {"PspGradient", type_grd3},
        {"qgs", type_qgs},
        {"sao", type_sao},
        {"svg", type_svg},
        {"tps", type_map},
        {NULL, 0}
      };

      for (p = types ; ; p++)
	{
	  if (! p->name)
	    {
	      fprintf(stderr, "output type %s not known\n", name);

	      fprintf(stderr, "supported types:\n");
	      for (p = types ; p->name ; p++)
		fprintf(stderr, "  %s\n", p->name);
	      fprintf(stderr, "\n");

	      return EXIT_FAILURE;
	    }

	  if (strcmp(name, p->name) == 0)
	    {
	      opt.type = p->type;
	      break;
	    }
	}
    }
  else
    opt.type = type_cpt;

  if ((helper_output(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0))
    return EXIT_FAILURE;

  /*
    this does not use the input helper, that allows zero or one
    input files, but this program needs exactly one (and is the
    only one that does, so does not merit a helper, that might
    change ...).
  */

  if (info->inputs_num != 1)
    {
      fprintf(stderr, "exactly one SVG input file required\n");
      return EXIT_FAILURE;
    }

  opt.input.path = info->inputs[0];

  if (opt.type == type_cpt)
    {
      int version = 0, count = 0;

      if (info->gmt4_flag)
        {
          version = 4;
          count++;
        }

      if (info->gmt5_flag)
        {
          version = 5;
          count++;
        }

      if (info->gmt6_flag)
        {
          version = 6;
          count++;
        }

      if (count > 1)
        {
          fprintf(stderr, "only one GMT version possible\n");
          return EXIT_FAILURE;
        }

      opt.format.cpt.gmt_version = version;

      if (parse_rgb(info->background_arg, &(opt.format.cpt.bg)) != 0)
        {
          fprintf(stderr, "bad background %s\n", info->background_arg);
          return EXIT_FAILURE;
        }

      if (parse_rgb(info->foreground_arg, &(opt.format.cpt.fg)) != 0)
        {
          fprintf(stderr, "bad foreground %s\n", info->foreground_arg);
          return EXIT_FAILURE;
        }

      if (parse_rgb(info->nan_arg, &(opt.format.cpt.nan)) != 0)
        {
          fprintf(stderr, "bad nan colour %s\n", info->nan_arg);
          return EXIT_FAILURE;
        }

      opt.format.cpt.normalise = info->z_normalise_flag;
    }
  else
    {
      if (info->gmt4_flag)
        {
          fprintf(stderr, "--gmt4 is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->gmt5_flag)
        {
          fprintf(stderr, "--gmt5 is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->gmt6_flag)
        {
          fprintf(stderr, "--gmt6 is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->background_given)
        {
          fprintf(stderr, "--background is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->foreground_given)
        {
          fprintf(stderr, "--foreground is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->nan_given)
        {
          fprintf(stderr, "--nan is only for cpt output\n");
          return EXIT_FAILURE;
        }

      if (info->z_normalise_flag)
        {
          fprintf(stderr, "--z-normalise is only for cpt output\n");
          return EXIT_FAILURE;
        }
    }

  if (info->transparency_given)
    {
      switch (opt.type)
        {
        case type_cpt:
        case type_gpt:
        case type_map:
        case type_sao:
          break;
        case type_css3:
        case type_ggr:
        case type_grd3:
        case type_pg:
        case type_png:
        case type_pov:
        case type_qgs:
        case type_svg:
          fprintf(stderr, "no --transparency for output type\n");
          return EXIT_FAILURE;
        default:
          fprintf(stderr, "unhandled output type\n");
          return EXIT_FAILURE;
        }
    }

  if (parse_rgb(info->transparency_arg, &(opt.format.alpha)) != 0)
    {
      fprintf(stderr, "bad transparency colour %s\n",
              info->transparency_arg);
      return EXIT_FAILURE;
    }

  if (info->geometry_given)
    {
      switch (opt.type)
	{
	case type_png:
	case type_svg:
	  break;

	default:
	  fprintf(stderr, "no --geometry for output type\n");
	  return EXIT_FAILURE;
	}
    }

  if (info->svg2_given)
    {
      switch (opt.type)
        {
        case type_svg:
          opt.format.svg.version = svg_version_20;
          break;

        default:
	  fprintf(stderr, "no --svg2 for output type\n");
	  return EXIT_FAILURE;
        }
    }
  else
    opt.format.svg.version = svg_version_11;

  switch (opt.type)
    {
    case type_png:

      if (sscanf(info->geometry_arg, "%zux%zu",
		 &opt.format.png.width,
		 &opt.format.png.height) != 2)
	{
	  fprintf(stderr, "bad argument \"%s\" for --geometry\n",
		  info->geometry_arg);
	  return EXIT_FAILURE;
	}
      break;

    case type_svg:

      if (info->preview_flag)
	{
	  opt.format.svg.preview.use = true;
	  if (svg_preview_geometry(info->geometry_arg,
				   &(opt.format.svg.preview)) != 0)
	    {
	      fprintf(stderr, "bad argument \"%s\" for --geometry",
		      info->geometry_arg);
	      return EXIT_FAILURE;
	    }
	}
      else
	opt.format.svg.preview.use = false;

      break;

    default:
      break;
    }

  if (opt.verbose)
    printf("This is svgx (version %s)\n", VERSION);

  if (opt.verbose && (opt.job != job_list))
    {
      const char *tstr;

      switch (opt.type)
	{
	case type_cpt: tstr = "GMT colour palette table"; break;
	case type_css3: tstr = "CSS3 gradient"; break;
	case type_ggr: tstr = "GIMP gradient"; break;
	case type_gpt: tstr = "Gnuplot colour map"; break;
	case type_grd3: tstr = "PaintShop Pro (grd3)"; break;
        case type_map: tstr = "Tecplot map"; break;
        case type_pg: tstr = "PostGIS colour map"; break;
	case type_pov: tstr = "POV-Ray colour map"; break;
	case type_png: tstr = "png image"; break;
	case type_qgs: tstr = "QGIS style colour map"; break;
	case type_sao: tstr = "SAO (DS9) colour map"; break;
	case type_svg: tstr = "SVG gradient"; break;

	default:
	  fprintf(stderr, "unknown output format\n");
	  return EXIT_FAILURE;
	}

      printf("convert svg to %s\n", tstr);
      printf("%s format limits\n",
             (opt.permissive ? "ignoring" : "respecting"));

      if (opt.type == type_png)
	printf("png output size is %zu x %zu px\n",
	       opt.format.png.width,
	       opt.format.png.height);
      else if ((opt.type == type_svg) &&
               (opt.format.svg.preview.use))
	printf("svg preview size is %zu x %zu px\n",
	       opt.format.svg.preview.width,
	       opt.format.svg.preview.height);
    }

  /*
    temporary directory which will be used to unpack the comments
    input in the --all case, we do this here since it makes it a
    bit easier to remove, see below
  */

  char tmp_dir[] = "/tmp/svgx-comments-XXXXXX";

  if ((opt.job == job_all) && (opt.comment.path.input != NULL))
    {
      if (mkdtemp(tmp_dir) == NULL)
        {
          fprintf(stderr, "failed to create temporary directory\n");
          return EXIT_FAILURE;
        }

      opt.tmp.path = tmp_dir;
    }
  else
    opt.tmp.path = NULL;

  btrace_enable("svgx");

  int err;

  if ((err = svgx(&opt)) != 0)
    helper_btrace(info);

  btrace_reset();
  btrace_disable();

  /*
    best effort to remove temporary directory created above and filled
    with files by the program -- we assume that the only directories
    present are "." and "..", and that there are no refular files which
    start with a "."; if those are not satisfied then the temporary
    directory will not be removed (but there is no error)
  */

  if (opt.tmp.path != NULL)
    {
      DIR *dir = opendir(opt.tmp.path);

      if (dir != NULL)
        {
          size_t n_dir = strlen(opt.tmp.path);
          struct dirent *entry;

          while ((entry = readdir(dir)) != NULL)
            {
              const char *name = entry->d_name;

              if ((name == NULL) || (name[0] == '\0') || (name[0] == '.'))
                continue;

              size_t
                n_name = strlen(name),
                n_path = n_dir + n_name + 2;
              char path[n_path];

              if (snprintf(path, n_path, "%s/%s", opt.tmp.path, name) >= n_path)
                continue;

              unlink(path);
            }

          closedir(dir);
        }

      rmdir(opt.tmp.path);
    }

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
