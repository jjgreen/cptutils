#ifndef SVGX_H
#define SVGX_H

#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>
#include <cptutils/colour.h>
#include <cptutils/comment.h>

#include <stdbool.h>
#include <stdio.h>

typedef enum {
  type_cpt,
  type_css3,
  type_ggr,
  type_gpt,
  type_grd3,
  type_map,
  type_pg,
  type_pov,
  type_png,
  type_qgs,
  type_sao,
  type_svg
} svgx_type_t;

typedef enum {
  job_first,
  job_list,
  job_id,
  job_all
} svgx_job_t;

typedef struct
{
  svgx_type_t type;
  svgx_job_t job;
  bool verbose, permissive, debug;
  comment_opt_t comment;
  char *id;
  struct
  {
    rgb_t alpha;
    struct
    {
      rgb_t fg, bg, nan;
      int gmt_version;
      bool normalise;
    } cpt;
    struct
    {
      size_t width, height;
    } png;
    struct
    {
      svg_preview_t preview;
      svg_version_t version;
    } svg;
  } format;
  struct
  {
    const char *path;
  } input, output, tmp;
} svgx_opt_t;

int svgx(const svgx_opt_t*);

#endif
