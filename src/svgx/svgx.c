#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include <cptutils/colour.h>
#include <cptutils/svg-list-read.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>
#include <cptutils/comment-list.h>
#include <cptutils/comment-list-read.h>
#include <cptutils/comment-write.h>

#include "svgx.h"
#include "svgx-dump.h"
#include "posixify.h"

static int svgx_list(const svgx_opt_t*, svg_list_t*);
static int svgx_first(const svgx_opt_t*, svg_list_t*);
static int svgx_id(const svgx_opt_t*, svg_list_t*);
static int svgx_all(const svgx_opt_t*, svg_list_t*);

int svgx(const svgx_opt_t *opt)
{
  int err = 1;
  svg_list_t *list;

  if ((list = svg_list_read(opt->input.path)) == NULL)
    btrace("error reading svg");
  else
    {
      switch (opt->job)
        {
        case job_list:
          err = svgx_list(opt, list);
          break;

        case job_first:
          err = svgx_first(opt, list);
          break;

        case job_id:
          err = svgx_id(opt, list);
          break;

        case job_all:
          err = svgx_all(opt, list);
          break;
        }

      svg_list_destroy(list);
    }

  return err;
}

/* print the gradients in the list */

static int svg_id(svg_t *svg, void *arg)
{
  const char *fmt = arg;
  printf(fmt, svg->id);
  return 0;
}

static int svgx_list(const svgx_opt_t *opt, svg_list_t *list)
{
  int err = 0;
  size_t n = svg_list_size(list);

  if (opt->verbose)
    {
      if (n == 0)
        printf("no gradient found!\n");
      else
	{
	  printf("found %zu gradient%s\n", n, PLURAL(n));
	  err = svg_list_each(list, svg_id, "  %s\n");
	}
    }
  else
    err = svg_list_each(list, svg_id, "%s\n");

  if (err)
    btrace("error listing svg");

  return err;
}

static int svg_select_id(svg_t*, void*);
static int svg_select_first(svg_t*, void*);
static int svgx_single(const svgx_opt_t*, svg_t*);

static int svgx_first(const svgx_opt_t *opt, svg_list_t *list)
{
  svg_t *svg;

  if ((svg = svg_list_find(list, svg_select_first, NULL)) == NULL)
    {
      btrace("couldn't find first gradient!");
      return 1;
    }

  return svgx_single(opt, svg);
}

static int svgx_id(const svgx_opt_t *opt, svg_list_t *list)
{
  svg_t *svg;

  if ((svg = svg_list_find(list, svg_select_id, opt->id)) == NULL)
    {
      btrace("couldn't find gradient with id %s", opt->id);
      return 1;
    }

  return svgx_single(opt, svg);
}

/* return whether a particular type should be flattened */

static bool flatten_type(svgx_type_t type)
{
  switch (type)
    {
    case type_cpt:
    case type_map:
    case type_gpt:
    case type_sao:
      return true;
    case type_css3:
    case type_ggr:
    case type_grd3:
    case type_pg:
    case type_pov:
    case type_qgs:
    case type_png:
    case type_svg:
      return false;
    }
  assert(false);
}

/* return dump function for a particular type */

typedef int (*dump_f)(svg_t*, void*);

static dump_f dump_type(svgx_type_t type)
{
  dump_f dump;

  switch (type)
    {
    case type_cpt: dump = svgcpt_dump; break;
    case type_css3: dump = svgcss3_dump; break;
    case type_ggr: dump = svgggr_dump; break;
    case type_gpt: dump = svggpt_dump; break;
    case type_grd3: dump = svggrd3_dump; break;
    case type_map: dump = svgmap_dump; break;
    case type_pg: dump = svgpg_dump; break;
    case type_pov: dump = svgpov_dump; break;
    case type_png: dump = svgpng_dump; break;
    case type_qgs: dump = svgqgs_dump; break;
    case type_sao: dump = svgsao_dump; break;
    case type_svg: dump = svgsvg_dump; break;

    default:
      btrace("strange output format!");
      dump = NULL;
    }

  return dump;
}

/* write a single file */

static int svgx_single(const svgx_opt_t *opt, svg_t *svg)
{
  if (svg_explicit(svg) != 0)
    {
      btrace("failed adding explicit stops");
      return 1;
    }

  if (flatten_type(opt->type))
    {
      if (svg_flatten(svg, opt->format.alpha) != 0)
	{
	  btrace("failed to flatten transparency");
	  return 1;
	}
    }

  dump_f dump;

  if ((dump = dump_type(opt->type)) == NULL)
    return 1;

  if (dump(svg, (void*)opt) != 0)
    {
      btrace("failed conversion");
      return 1;
    }

  if (opt->verbose)
    printf("wrote %s to %s\n",
	   IFNULL(opt->id, "gradient"),
	   IFNULL(opt->output.path, "<stdout>"));

  return 0;
}

static int svg_select_first(svg_t *svg, void *arg)
{
  return 1;
}

static int svg_select_id(svg_t *svg, void *arg)
{
  char *id = arg;
  return (strcmp((const char*)svg->id, id) == 0 ? 1 : 0);
}

static int svg_explicit2(svg_t *svg, void *arg)
{
  return svg_explicit(svg);
}

static int svg_flatten2(svg_t *svg, void *arg)
{
  rgb_t *alpha = arg;
  return svg_flatten(svg, *alpha);
}

typedef struct {
  const comment_list_t *list;
  const char *dir, *path;
  size_t n;
} comment_split_arg_t;

static int comment_split(svg_t *svg, void *varg)
{
  comment_split_arg_t *arg = varg;
  const char suffix[] = "xco";
  char *base = svg_basename(svg);

  if (base == NULL)
    {
      btrace("failed to get file basename from SVG id");
      return 1;
    }

  int err = 1;
  size_t n_file = strlen(base) + strlen(suffix) + 2;
  char file[n_file];

  if (snprintf(file, n_file, "%s.%s", base, suffix) >= n_file)
    btrace("filename truncated! %s", file);
  else
    {
      if (posixify(file) != 0)
        btrace("failed to create POSIX filename");
      else
        {
          size_t n_path = strlen(arg->dir) + n_file + 2;
          char path[n_path];

          if (snprintf(path, n_path, "%s/%s", arg->dir, file) >= n_path)
            btrace("path truncated: %s", path);
          else
            {
              const comment_t *comment;

              if ((comment = comment_list_entry(arg->list, arg->n)) == NULL)
                btrace("failed to get comment %zu", arg->n);
              else
                {
                  if (comment_write(comment, path) != 0)
                    btrace("failed to write comments to %s", path);
                  else
                    err = 0;
                }
            }
        }
    }

  free(base);
  arg->n++;

  return err;
}

static int svgx_all(const svgx_opt_t *opt, svg_list_t *list)
{
  if (svg_list_each(list, svg_explicit2, NULL) != 0)
    {
      btrace("failed coerce explicit");
      return 1;
    }

  if (flatten_type(opt->type))
    {
      if (svg_list_each(list,
                        svg_flatten2,
                        (void*)&(opt->format.alpha)) != 0)
	{
	  btrace("failed coerce explicit");
	  return 1;
	}
    }

  if (opt->comment.path.input != NULL)
    {
      comment_list_t *comment_list;

      if ((comment_list = comment_list_read(opt->comment.path.input)) == NULL)
        {
          btrace("failed read of comment-list");
          return 1;
        }

      if (comment_list_size(comment_list) != svg_list_size(list))
        {
          btrace("number of comments (%zu) doesn't match gradients (%zu)",
                 comment_list_size(comment_list), svg_list_size(list));
          return 1;
        }

      comment_split_arg_t arg = {
        .list = comment_list,
        .n = 0,
        .dir = opt->tmp.path,
        .path = opt->comment.path.input
      };

      if (svg_list_each(list, comment_split, &arg) != 0)
        {
          btrace("failed splitting of input comments");
          return 1;
        }

      comment_list_destroy(comment_list);
    }

  int (*dump)(svg_t*, void*);

  if ((dump = dump_type(opt->type)) == NULL)
    return 1;

  if (opt->verbose)
    printf("converting all gradients:\n");

  if (svg_list_each(list, dump, (void*)opt) != 0)
    {
      btrace("failed writing all gradients");
      return 1;
    }

  return 0;
}
