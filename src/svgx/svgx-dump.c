#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "svgx-dump.h"
#include "utf8x.h"
#include "posixify.h"

#include <cptutils/btrace.h>
#include <cptutils/cpt-write.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/css3-write.h>
#include <cptutils/ggr-write.h>
#include <cptutils/gpt-write.h>
#include <cptutils/grd3-write.h>
#include <cptutils/pg-write.h>
#include <cptutils/pov-write.h>
#include <cptutils/png-write.h>
#include <cptutils/qgs-list-write.h>
#include <cptutils/tpm-write.h>
#include <cptutils/sao-write.h>
#include <cptutils/svg-list-write.h>
#include <cptutils/svg-write.h>
#include <cptutils/utf8.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

/*
  create a new opt with an autogenerated filename and call f(svg, opt),
  this is common for dump functions in the opt.job == job_all case,
  we also handle the reading/writing of comments -- these file have
  the same name (again generated, <base>.xco) but the writer does so
  to the --comments-write directory, the reader from the temporary
  directory (having earlier been written by comments_split).
*/

static int call_autonamed(svg_t *svg,
			  const svgx_opt_t *opt,
			  const char *suffix,
			  int (*f)(svg_t*, void*))
{
  char *base = svg_basename(svg);

  if (base == NULL)
    {
      btrace("failed to get file basename from SVG id");
      return 1;
    }

  /* filename of output gradient */

  size_t n_gradient_file = strlen(base) + strlen(suffix) + 2;
  char gradient_file[n_gradient_file];

  if (snprintf(gradient_file, n_gradient_file,
               "%s.%s", base, suffix) >= n_gradient_file)
    {
      btrace("filename truncated! %s", gradient_file);
      return 1;
    }

  /* filename of input/output comment file */

  size_t n_comment_file = strlen(base) + strlen("xco") + 2;
  char comment_file[n_comment_file];

  if (snprintf(comment_file, n_comment_file,
               "%s.%s", base, "xco") >= n_comment_file)
    {
      btrace("filename truncated! %s", comment_file);
      return 1;
    }

  free(base);

  /*
    the SVG basename (essentially the id with "id-" removed if present)
    may not be a legitimate filename, so we clean it up an bit
  */

  if ((posixify(gradient_file) != 0) || (posixify(comment_file) != 0))
    {
      btrace("failed to create POSIX filename");
      return 0;
    }

  /* gradient path */

  const char *gradient_dir;

  if (opt->output.path)
    gradient_dir = opt->output.path;
  else
    gradient_dir = ".";

  size_t n_gradient_path =
    strlen(gradient_dir) +
    strlen(gradient_file) + 2;
  char gradient_path[n_gradient_path];

  if (snprintf(gradient_path, n_gradient_path,
               "%s/%s", gradient_dir, gradient_file) >= n_gradient_path)
    {
      btrace("path truncated: %s", gradient_path);
      return 1;
    }

  /*
    the comment output is a bit different, it may be NULL so we
    allocate on the heap rather than the stack
  */

  char *comment_output_path = NULL;

  if (opt->comment.path.output != NULL)
    {
      size_t n_comment_path =
        strlen(opt->comment.path.output) +
        strlen(comment_file) + 2;
      char comment_path[n_comment_path];

      if (snprintf(comment_path, n_comment_path,
                   "%s/%s",
                   opt->comment.path.output,
                   comment_file) >= n_comment_path)
        {
          btrace("path truncated: %s", comment_path);
          return 1;
        }

      comment_output_path = strdup(comment_path);
    }

  /*
    the comment input is also allocated on the heap, and we have
    previously generated the per-gradient file in the temporary
    directory
  */

  char *comment_input_path = NULL;

  if (opt->tmp.path != NULL)
    {
      size_t n_comment_path =
        strlen(opt->tmp.path) +
        strlen(comment_file) + 2;
      char comment_path[n_comment_path];

      if (snprintf(comment_path, n_comment_path,
                   "%s/%s",
                   opt->tmp.path,
                   comment_file) >= n_comment_path)
        {
          btrace("path truncated: %s", comment_path);
          return 1;
        }

      comment_input_path = strdup(comment_path);
    }

  /* new options struct with per-gradient paths */

  svgx_opt_t opt2 = *opt;

  opt2.job = job_id;
  opt2.output.path = gradient_path;
  opt2.comment.path.input = comment_input_path;
  opt2.comment.path.output = comment_output_path;

  int err = f(svg, &opt2);

  free(comment_input_path);
  free(comment_output_path);

  opt2.comment.path.input = NULL;
  opt2.comment.path.output = NULL;
  opt2.output.path = NULL;

  if (opt->verbose)
    printf("  %s\n", gradient_path);

  return err;
}

static bool has_name(const svg_t *svg)
{
  return svg->name[0] != '\0';
}

/*
  in general, we limit the actions which populate the options
  struct from the command-line (so a program which reads a file
  which contains comments but writes a file which cannot will not
  set 'action_generate', but for svgx we can't do that since some
  output formats do support comments and some don't; this function
  is the same as comments_process(), but errors if the action is
  a write action
*/

static int comment_process_ro(comment_t *comment, const comment_opt_t *opt)
{
  if (opt->action != action_none)
    {
      const char *desc;

      switch (opt->action)
        {
        case action_retain:
          desc = "retain";
          break;
        case action_generate:
          desc = "generate";
          break;
        case action_read:
          desc = "read";
          break;
        default:
          desc = "unknown";
        }

      btrace("output format does not support comment %s", desc);
      return 1;
    }

  return comment_process(comment, opt);
}

/*
  here there are blocks for each svgx_dump() function,
  which converts the svg structure to type x by a call
  to the file static svgx() function.  For some x there
  are a few helper functions too.

  Each block could (perhaps should) be moved into a
  separate file.
*/

/* cpt */

static int svgcpt(const svg_t *svg, cpt_t *cpt)
{
  svg_node_t *node, *next;

  node = svg->nodes;
  next = node->r;

  while (next)
    {
      double z1, z2;

      z1 = node->stop.value;
      z2 = next->stop.value;

      if (z1 < z2)
	{
	  rgb_t c1, c2;
	  cpt_seg_t *seg;

	  c1 = node->stop.colour;
	  c2 = next->stop.colour;

	  if ((seg = cpt_seg_new()) == NULL)
	    {
	      btrace("failed to create cpt segment");
	      return 1;
	    }

	  seg->sample.left.val = z1;
	  seg->sample.right.val = z2;

	  seg->sample.left.fill.type = cpt_fill_colour_rgb;
	  seg->sample.left.fill.colour.rgb = c1;

	  seg->sample.right.fill.type = cpt_fill_colour_rgb;
	  seg->sample.right.fill.colour.rgb = c2;

	  if (cpt_append(seg, cpt) != 0)
	    {
	      btrace("failed to append segment");
	      return 1;
	    }
	}

      node = next;
      next = node->r;
    }

  if (comment_copy(svg->comment, cpt->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

static int svgcpt_write(const cpt_t *cpt,
                        const cptwrite_opt_t *write_opt,
                        const char *path)
{
  int err = cpt_write(cpt, write_opt, path);

  if (err != 0)
    btrace("failed to write to %s", path);

  return err;
}

int svgcpt_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "cpt", svgcpt_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_new()) == NULL)
    btrace("failed to create cpt structure");
  else
    {
      cpt->model = model_rgb;

      cpt->fg.type = cpt->bg.type = cpt->nan.type = cpt_fill_colour_rgb;

      cpt->bg.colour.rgb = opt->format.cpt.bg;
      cpt->fg.colour.rgb = opt->format.cpt.fg;
      cpt->nan.colour.rgb = opt->format.cpt.nan;

      cpt->name = strdup(name);

      if (svgcpt(svg, cpt) != 0)
        btrace("failed to convert %s to cpt", name);
      else
        {
          int gmt_version = opt->format.cpt.gmt_version;
          cptwrite_opt_t write_opt;

          if (cpt_write_options(gmt_version, &write_opt) != 0)
            btrace("bad GMT version %i", gmt_version);
          else
            {
              if (opt->format.cpt.normalise)
                {
                  if (cpt_normalise(cpt, false, 0) != 0)
                    btrace("normalising");
                  else
                    err = svgcpt_write(cpt, &write_opt, file);
                }
              else
                err = svgcpt_write(cpt, &write_opt, file);
            }
        }
      cpt_destroy(cpt);
    }

  return err;
}

/* ggr */

static int svgggr(const svg_t *svg, ggr_t *ggr)
{
  svg_node_t *node, *next;
  ggr_segment_t *gseg, *prev = NULL;

  ggr->name = strdup((char*)svg->name);

  node = svg->nodes;
  next = node->r;

  while (next)
    {
      double z1, z2;

      z1 = node->stop.value;
      z2 = next->stop.value;

      if (z1 < z2)
	{
	  rgb_t c1, c2;
	  double o1, o2;
	  double lcol[3], rcol[3];

	  c1 = node->stop.colour;
	  c2 = next->stop.colour;

	  o1 = node->stop.opacity;
	  o2 = next->stop.opacity;

	  if ((gseg = ggr_segment_new()) == NULL) return 1;

	  gseg->prev = prev;

	  if (prev)
	    prev->next = gseg;
	  else
	    ggr->segments = gseg;

	  gseg->left = z1 / 100.0;
	  gseg->middle = (z1 + z2) / 200.0;
	  gseg->right = z2 / 100.0;

	  lcol[0] = lcol[1] = lcol[2] = 0.0;
	  rcol[0] = rcol[1] = rcol[2] = 0.0;

	  gseg->a0 = o1;
	  gseg->a1 = o2;

	  rgb_to_rgbD(c1, lcol);
	  rgb_to_rgbD(c2, rcol);

	  gseg->r0 = lcol[0];
	  gseg->g0 = lcol[1];
	  gseg->b0 = lcol[2];

	  gseg->r1 = rcol[0];
	  gseg->g1 = rcol[1];
	  gseg->b1 = rcol[2];

	  gseg->type = GGR_LINEAR;
	  gseg->color = GGR_RGB;

	  gseg->ect_left = GGR_FIXED;
	  gseg->ect_right = GGR_FIXED;

	  prev = gseg;
	}

      node = next;
      next = node->r;
    }

  return 0;
}

int svgggr_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "ggr", svgggr_dump);

  if (comment_process_ro(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  ggr_t *ggr;
  int err = 1;

  if ((ggr = ggr_new()) == NULL)
    btrace("failed to create ggr structure");
  else
    {
      if (svgggr(svg, ggr) != 0)
        btrace("failed to convert %s to cpt", name);
      else
        {
          if (ggr_write(ggr, file) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      ggr_destroy(ggr);
    }

  return err;
}

/* tecplot map (tpm) */

static int svgmap(const svg_t *svg, tpm_t *tpm)
{
  tpm_point_t point;
  svg_node_t *node = svg->nodes;

  while (node)
    {
      point.fraction = node->stop.value / 100.0;
      point.lead = point.trail = node->stop.colour;

      if (tpm_push(tpm, &point) != 0)
        {
          btrace("failed push of point %g", point.fraction);
          return 1;
        }

      node = node->r;
    }

  if (tpm_manicure(tpm) != 0)
    {
      btrace("failed tpm manicure");
      return 1;
    }

  comment_t *comment;

  if ((comment = tpm_comment(tpm)) == NULL)
    {
      btrace("failed to get comments");
      return 1;
    }
  else if (comment_copy(svg->comment, comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

static int svgmap_valid(const svg_t *svg, const svgx_opt_t *opt)
{
  if (opt->permissive)
    return 0;

  int m;

  if ((m = svg_num_stops(svg)) > TPM_STOPS_MAX)
    {
      if (opt->verbose)
        printf("Tecplot can only manage %i stops\n", TPM_STOPS_MAX);
      btrace("too many stops (%i)", m);
      return 1;
    }

  return 0;
}

int svgmap_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "map", svgmap_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  if (svgmap_valid(svg, opt) != 0)
    {
      btrace("cannot create valid map file");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *path = opt->output.path;
  tpm_t *tpm;
  int err = 1;

  if ((tpm = tpm_new()) == NULL)
    btrace("failed to create new tpm (map)");
  else
    {
      if (svgmap(svg, tpm) != 0)
        btrace("failed to convert %s to map", name);
      else
        {
          if (tpm_write(tpm, path) != 0)
            btrace("failed to write to %s", path);
          else
            err = 0;
        }

      tpm_destroy(tpm);
    }

  return err;
}

/* pg */

static int svgpg(const svg_t *svg, pg_t *pg)
{
  svg_node_t *node = svg->nodes;
  int n = 0;

  while (node)
    {
      n++;

      int alpha = node->stop.opacity * 256;

      if (alpha > 255)
        alpha = 255;
      else if (alpha < 0)
        alpha = 0;

      if (pg_push(pg,
                  node->stop.value,
                  node->stop.colour,
                  alpha) != 0)
        {
          btrace("error adding stop %i", n + 1);
          return 1;
        }

      node = node->r;
    }

  return 0;
}

int svgpg_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "pg", svgpg_dump);

  if (comment_process_ro(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *path = opt->output.path;
  pg_t *pg;
  int err = 1;

  if ((pg = pg_new()) == NULL)
    btrace("failed to create pg structure");
  else
    {
      if (svgpg(svg, pg) != 0)
        btrace("failed to convert %s to pg", name);
      else
        {
          if (pg_write(pg, path) != 0)
            btrace("failed to write to %s", path);
          else
            err = 0;
        }

      pg_destroy(pg);
    }

  return err;
}

/* povray */

static int svgpov_valid(const svg_t *svg, int permissive, int verbose)
{
  int m = svg_num_stops(svg);

  if (m > POV_STOPS_MAX)
    {
      if (permissive)
	{
	  if (verbose)
	    printf("warning: format limit broken %i stops (max is %i)\n",
		   m, POV_STOPS_MAX);
	}
      else
	{
	  btrace("format limit: POV-ray allows no more than %i stops, "
		 "but this gradient has %i", POV_STOPS_MAX, m);

	  return 0;
	}
    }

  if (m < 2)
    {
      btrace("found %i stops, but at least 2 required", m);
      return 0;
    }

  return 1;
}

static int svgpov(const svg_t *svg, pov_t *pov)
{
  /* count & allocate */

  size_t m = svg_num_stops(svg);

  if (m < 2)
    {
      btrace("bad number of stops : %i", m);
      return 1;
    }

  if (pov_stops_alloc(pov, m) != 0)
    {
      btrace("failed alloc for %i stops", m);
      return 1;
    }

  /* convert */

  size_t n;
  svg_node_t *node;

  for (n = 0, node = svg->nodes ; node ; n++, node = node->r)
    {
      pov_stop_t stop;
      rgb_t rgb;
      double c[3], t, z;

      rgb = node->stop.colour;

      if (rgb_to_rgbD(rgb, c) != 0)
	{
	  btrace("failed conversion to rgbD");
	  return 1;
	}

      t = 1.0 - node->stop.opacity;

      if ((t < 0.0) || (t > 1.0))
	{
	  btrace("bad value for transparency : %f", t);
	  return 1;
	}

      z = node->stop.value/100.0;

      if ((z < 0.0) || (z > 1.0))
	{
	  btrace("bad z value : %f", t);
	  return 1;
	}

      stop.z = z;

      stop.rgbt[0] = c[0];
      stop.rgbt[1] = c[1];
      stop.rgbt[2] = c[2];
      stop.rgbt[3] = t;

      pov->stop[n] = stop;
    }

  if (n != m)
    {
      btrace("mismatch between stops expected (%i) and found (%i)", m, n);
      return 1;
    }

  pov->n = n;

  /* povray names need to be alphanumeric ascii */

  if (has_name(svg))
    {
      char aname[SVG_NAME_LEN];

      if (utf8_to_x("ASCII", svg->name, aname, SVG_NAME_LEN) != 0)
	{
	  btrace("failed to convert name %s to ascii", aname);
	  return 1;
	}

      size_t nmod;

      if (pov_set_name(pov, aname, &nmod) != 0)
	{
	  btrace("failed to assign povray name (%s)", aname);
	  return 1;
	}

      /* warn if name was modified */

      if (nmod > 0)
	btrace("name modified: %s to %s", svg->name, pov->name);
    }
  else
    {
      const char aname[] = "unassigned";
      size_t nmod;

      if (pov_set_name(pov, aname, &nmod) != 0)
	{
	  btrace("failed to assign povray name (%s)", aname);
	  return 1;
	}
    }

  if (comment_copy(svg->comment, pov->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

int svgpov_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "inc", svgpov_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  if (! svgpov_valid(svg, opt->permissive, opt->verbose))
    {
      btrace("cannot create valid povray file");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  pov_t *pov;
  int err = 1;

  if ((pov = pov_new()) == NULL)
    btrace("failed to create pov structure");
  else
    {
      if (svgpov(svg, pov) != 0)
        btrace("failed to convert %s to pov", name);
      else
        {
          if (pov_write(pov, file) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      pov_destroy(pov);
    }

  return err;
}

/* qgs */

static unsigned char unit_uchar(double x)
{
  return nearbyint(x * 255);
}

static void svg_stop_to_qgs_entry(svg_stop_t stop, qgs_entry_t *entry)
{
  entry->rgb = stop.colour;
  entry->opacity = unit_uchar(stop.opacity);
  entry->value = stop.value/100.0;
}

static int svgqgs(const svg_t *svg, qgs_t *qgs)
{
  size_t m = svg_num_stops(svg);

  if (m < 2)
    {
      btrace("bad number of stops : %i", m);
      return 1;
    }

  if (qgs_set_name(qgs, (const char*)svg->name) != 0)
    {
      btrace("failed to set name for qgs");
      return 1;
    }

  if (qgs_set_type(qgs, QGS_TYPE_INTERPOLATED) != 0)
    {
      btrace("failed to set type for qgs");
      return 1;
    }

  if (qgs_alloc_entries(qgs, m) != 0)
    {
      btrace("failed qgs allocate for %i stops", m);
      return 1;
    }

  svg_node_t *node;
  size_t n;

  for (n = 0, node = svg->nodes ; node ; n++, node = node->r)
    {
      qgs_entry_t entry;

      svg_stop_to_qgs_entry(node->stop, &entry);

      if (qgs_set_entry(qgs, n, &entry) != 0)
	{
	  btrace("failed to set qgs entry %zi", n);
	  return 1;
	}
    }

  if (n != m)
    {
      btrace("mismatch between stops expected (%i) and found (%i)", m, n);
      return 1;
    }

  if (comment_copy(svg->comment, qgs->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

int svgqgs_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "qgs", svgqgs_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  int err = 1;
  qgs_list_t *list;

  if ((list = qgs_list_new()) != NULL)
    {
      qgs_t *qgs;

      if ((qgs = qgs_list_next(list)) != NULL)
        {
          if (svgqgs(svg, qgs) == 0)
            {
              const char *path = opt->output.path;

              if ((err = qgs_list_write(list, path)) != 0)
                btrace("failed to write to %s", path);
            }
          else
            btrace("failed convert of svg to qgs");
        }
      else
        btrace("failed to get qgs from list");

      qgs_list_destroy(list);
    }
  else
    btrace("failed create qgs list");

  return err;
}

/* sao */

static int svgsao(const svg_t *svg, sao_t *sao)
{
  svg_node_t *node;
  size_t n = 0;

  for (node = svg->nodes ; node ; node = node->r, n++)
    {
      double rgbD[3], val = node->stop.value / 100.0;

      if (rgb_to_rgbD(node->stop.colour, rgbD) != 0)
	{
	  btrace("error converting colour of stop %i", n+1);
	  return 1;
	}

      if ((sao_red_push(sao, val, rgbD[0]) != 0) ||
          (sao_green_push(sao, val, rgbD[1]) != 0) ||
          (sao_blue_push(sao, val, rgbD[2]) != 0))
	{
	  btrace("error adding sao stop %i", n + 1);
	  return 1;
	}
    }

  comment_t *comment;

  if ((comment = sao_comment(sao)) == NULL)
    {
      btrace("failed to get SAO comments");
      return 1;
    }
  else if (comment_copy(svg->comment, comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

int svgsao_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "sao", svgsao_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  sao_t *sao;
  int err = 1;

  if ((sao = sao_new()) == NULL)
    btrace("failed to create sao structure");
  else
    {
      if (svgsao(svg, sao) != 0)
        btrace("failed to convert %s to sao", opt->id);
      else
        {
          if (sao_write(sao, file, name) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      sao_destroy(sao);
    }

  return err;
}

/* gpt */

static int svggpt(const svg_t *svg, gpt_t *gpt)
{
  /* count & allocate */

  size_t m = svg_num_stops(svg);

  if (m < 2)
    {
      btrace("bad number of stops : %i", m);
      return 1;
    }

  if (gpt_stops_alloc(gpt, m) != 0)
    {
      btrace("failed alloc for %i stops", m);
      return 1;
    }

  /* convert */

  size_t n;
  svg_node_t *node;

  for (n = 0, node = svg->nodes ; node ; n++, node = node->r)
    {
      gpt_stop_t stop;
      rgb_t rgb;
      double c[3];

      rgb = node->stop.colour;

      if (rgb_to_rgbD(rgb, c) != 0)
	{
	  btrace("failed conversion to rgbD");
	  return 1;
	}

      stop.z = node->stop.value / 100.0;

      stop.rgb[0] = c[0];
      stop.rgb[1] = c[1];
      stop.rgb[2] = c[2];

      gpt->stop[n] = stop;
    }

  if (n != m)
    {
      btrace("mismatch between stops expected (%i) and found (%i)", m, n);
      return 1;
    }

  gpt->n = n;

  if (comment_copy(svg->comment, gpt->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

int svggpt_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "gpt", svggpt_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  gpt_t *gpt;
  int err = 1;

  if ((gpt = gpt_new()) == NULL)
    btrace("failed to create gpt structure");
  else
    {
      if (svggpt(svg, gpt) != 0)
        btrace("failed to convert %s to gpt", name);
      else
        {
          if (gpt_write(gpt, file) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      gpt_destroy(gpt);
    }

  return err;
}

/* css3 */

static int svgcss3(const svg_t *svg, css3_t *css3)
{
  /* count & allocate */

  size_t m = svg_num_stops(svg);

  if (m < 2)
    {
      btrace("bad number of stops : %i", m);
      return 1;
    }

  if (css3_stops_alloc(css3, m) != 0)
    {
      btrace("failed alloc for %i stops", m);
      return 1;
    }

  /* convert */

  size_t n;
  svg_node_t *node;


  for (n = 0, node = svg->nodes ; node ; n++, node = node->r)
    {
      css3_stop_t stop;

      stop.rgb = node->stop.colour;
      stop.z = node->stop.value;
      stop.alpha = node->stop.opacity;

      css3->stop[n] = stop;
    }

  if (n != m)
    {
      btrace("mismatch between stops expected (%i) and found (%i)", m, n);
      return 1;
    }

  css3->n = n;

  if (comment_copy(svg->comment, css3->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  return 0;
}

int svgcss3_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "c3g", svgcss3_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  css3_t *css3;
  int err = 1;

  if ((css3 = css3_new()) == NULL)
    btrace("failed to create css3 structure");
  else
    {
      if (svgcss3(svg, css3) != 0)
        btrace("failed to convert %s to css3", name);
      else
        {
          if (css3_write(css3, file) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      css3_destroy(css3);
    }

  return err;
}

/* grd3 */

static double clampd(double z, double min, double max)
{
  if (z < min) z = min;
  if (z > max) z = max;

  return z;
}

static int clampi(int z, int min, int max)
{
  if (z < min) z = min;
  if (z > max) z = max;

  return z;
}

static int svggrd3(const svg_t *svg, grd3_t *grd3)
{
  size_t m = svg_num_stops(svg);

  if (m < 2)
    {
      btrace("bad number of stops : %i", m);
      return 1;
    }

  grd3_rgbseg_t *pcseg = calloc(m, sizeof(grd3_rgbseg_t));

  if (pcseg == NULL)
    btrace("failed to allocate segments");
  else
    {
      grd3_opseg_t *poseg = calloc(m, sizeof(grd3_opseg_t));

      if (poseg == NULL)
	btrace("failed to allocate segments");
      else
	{
	  size_t n;
	  svg_node_t *node;

	  for (n = 0, node = svg->nodes ; node ; n++, node = node->r)
	    {
	      rgb_t rgb;
	      double op, z;

	      rgb = node->stop.colour;
	      op = node->stop.opacity;
	      z = node->stop.value;

	      pcseg[n].z = clampd(4096 * z / 100.0, 0, 4096);
	      pcseg[n].midpoint = 50;
	      pcseg[n].r = clampi(rgb.red * 257, 0, 65535);
	      pcseg[n].g = clampi(rgb.green * 257, 0, 65535);;
	      pcseg[n].b = clampi(rgb.blue * 257, 0, 65535);;

	      poseg[n].z = clampd(4096 * z / 100.0, 0, 4096);
	      poseg[n].midpoint = 50;
	      poseg[n].opacity = clampi(op * 256, 0, 255);
	    }

	  char buffer[SVG_NAME_LEN];

	  if (utf8_to_x("LATIN1", svg->name, buffer, SVG_NAME_LEN) != 0)
	    btrace("failed to convert utf name to latin1");
	  else
	    {
	      grd3->name = (unsigned char*)strdup(buffer);

	      grd3->rgb.n = m;
	      grd3->rgb.seg = pcseg;

	      grd3->op.n = m;
	      grd3->op.seg = poseg;

	      return 0;
	    }
	}
      free(poseg);
    }
  free(pcseg);

  return 1;
}

int svggrd3_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "grd", svggrd3_dump);

  if (comment_process_ro(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  grd3_t *grd3;
  int err = 1;

  if ((grd3 = grd3_new()) == NULL)
    btrace("failed to create grd3 structure");
  else
    {
      if (svggrd3(svg, grd3) != 0)
        btrace("failed to convert %s to grd3", name);
      else
        {
          if (grd3_write(grd3, file) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      grd3_destroy(grd3);
    }

  return err;
}

/* png */

static int svgpng(const svg_t *svg, png_t *png)
{

  size_t nz = png->width;
  unsigned char *row = png->row;

  for (size_t i = 0 ; i < nz ; i++)
    {
      rgb_t rgb;
      double op, z = 100.0 * (double)i / (double)(nz - 1);

      if (svg_interpolate(svg, z, &rgb, &op) != 0)
	{
	  btrace("failed svg interpolate at %.3g", z);
	  return 1;
	}

      op *= 256;

      if (op > 255) op = 255;

      row[4 * i] = rgb.red;
      row[4 * i + 1] = rgb.green;
      row[4 * i + 2] = rgb.blue;
      row[4 * i + 3] = (unsigned char)op;
    }

  return 0;
}

int svgpng_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "png", svgpng_dump);

  if (comment_process_ro(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  const char *name = (char*)svg->name;
  const char *file = opt->output.path;
  png_t *png;
  int err = 1;

  if ((png = png_new(opt->format.png.width,
		     opt->format.png.height)) == NULL)
    btrace("failed to create png structure");
  else
    {
      if (svgpng(svg, png) != 0)
        btrace("failed to convert %s to png", name);
      else
        {
          if (png_write(png, file, name) != 0)
            btrace("failed to write to %s", file);
          else
            err = 0;
        }

      png_destroy(png);
    }

  return err;
}

int svgsvg_dump(svg_t *svg, void *arg)
{
  const svgx_opt_t *opt = arg;

  if (opt->job == job_all)
    return call_autonamed(svg, opt, "svg", svgsvg_dump);

  if (comment_process(svg->comment, &(opt->comment)) != 0)
    {
      btrace("failed to process comments");
      return 1;
    }

  return
    svg_write(svg,
              opt->output.path,
              &(opt->format.svg.preview),
              opt->format.svg.version);
}
