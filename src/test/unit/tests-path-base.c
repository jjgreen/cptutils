#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-path-base.h"

#include <cptutils/path-base.h>

#include <stdlib.h>


CU_TestInfo tests_path_base[] = {
  {"specific extension", test_path_base_specific},
  {"wildcard extension", test_path_base_wildcard},
  {"extension argument null", test_path_base_null},
  CU_TEST_INFO_NULL,
};

/* with a specific extension */

static void path_base_test(const char *path, const char *ext, const char *base)
{
  char *result = path_base(path, ext);
  CU_ASSERT_PTR_NOT_NULL_FATAL(result)
  CU_ASSERT_STRING_EQUAL(result, base);
  free(result);
}

void test_path_base_specific(void)
{
  path_base_test("base", "cpt", "base");
  path_base_test("base.cpt", "cpt", "base");
  path_base_test("base.cpt", "foo", "base.cpt");
  path_base_test("relative/directory/base.cpt", "cpt", "base");
  path_base_test("/absolute/directory/base.cpt", "cpt", "base");
}

void test_path_base_wildcard(void)
{
  path_base_test("base", "*", "base");
  path_base_test("base.cpt", "*", "base");
  path_base_test("base.foo", "*", "base");
  path_base_test("base.double.ext", "*", "base.double");
}

void test_path_base_null(void)
{
  path_base_test("base", NULL, "base");
  path_base_test("base.cpt", NULL, "base.cpt");
  path_base_test("/foo/bar/base.cpt", NULL, "base.cpt");
}
