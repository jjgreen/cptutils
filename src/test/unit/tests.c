#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-btrace.h"
#include "tests-colour.h"
#include "tests-colour-model.h"
#include "tests-comment.h"
#include "tests-comment-read.h"
#include "tests-comment-write.h"
#include "tests-comment-list.h"
#include "tests-comment-list-process.h"
#include "tests-comment-list-read.h"
#include "tests-comment-list-write.h"
#include "tests-cpt.h"
#include "tests-cpt-coerce.h"
#include "tests-cpt-fill.h"
#include "tests-cpt-normalise.h"
#include "tests-cpt-read.h"
#include "tests-cpt-write.h"
#include "tests-css3-write.h"
#include "tests-ggr.h"
#include "tests-ggr-read.h"
#include "tests-ggr-write.h"
#include "tests-gmtcol.h"
#include "tests-gpt.h"
#include "tests-gpt-write.h"
#include "tests-grd3.h"
#include "tests-grd3-read.h"
#include "tests-grd3-write.h"
#include "tests-grd5.h"
#include "tests-grd5-read.h"
#include "tests-grd5-string.h"
#include "tests-grd5-type.h"
#include "tests-grdx-svg.h"
#include "tests-hash.h"
#include "tests-path-base.h"
#include "tests-pg.h"
#include "tests-pg-write.h"
#include "tests-png.h"
#include "tests-png-write.h"
#include "tests-pov.h"
#include "tests-pov-write.h"
#include "tests-qgs.h"
#include "tests-qgs-list-read.h"
#include "tests-qgs-list-write.h"
#include "tests-qgs-list.h"
#include "tests-sao.h"
#include "tests-sao-write.h"
#include "tests-stdcol.h"
#include "tests-svg.h"
#include "tests-svg-list.h"
#include "tests-svg-list-write.h"
#include "tests-svg-list-read.h"
#include "tests-svg-preview.h"
#include "tests-svg-write.h"
#include "tests-tpm.h"
#include "tests-tpm-write.h"
#include "tests-u32map.h"
#include "tests-xml-id.h"

#include "cunit-compat.h"

#include <CUnit/CUnit.h>

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] = {
  ENTRY("btrace", tests_btrace),
  ENTRY("colour", tests_colour),
  ENTRY("colour-model", tests_colour_model),
  ENTRY("comment", tests_comment),
  ENTRY("comment-read", tests_comment_read),
  ENTRY("comment-write", tests_comment_write),
  ENTRY("comment-list", tests_comment_list),
  ENTRY("comment-list-process", tests_comment_list_process),
  ENTRY("comment-list-read", tests_comment_list_read),
  ENTRY("comment-list-write", tests_comment_list_write),
  ENTRY("cpt", tests_cpt),
  ENTRY("cpt-coerce", tests_cpt_coerce),
  ENTRY("cpt-fill", tests_cpt_fill),
  ENTRY("cpt-normalise", tests_cpt_normalise),
  ENTRY("cpt-read", tests_cpt_read),
  ENTRY("cpt-write", tests_cpt_write),
  ENTRY("css3-write", tests_css3_write),
  ENTRY("ggr", tests_ggr),
  ENTRY("ggr-read", tests_ggr_read),
  ENTRY("ggr-write", tests_ggr_write),
  ENTRY("gmtcol", tests_gmtcol),
  ENTRY("gpt", tests_gpt),
  ENTRY("gpt-write", tests_gpt_write),
  ENTRY("grd3", tests_grd3),
  ENTRY("grd3-read", tests_grd3_read),
  ENTRY("grd3-write", tests_grd3_write),
  ENTRY("grd5", tests_grd5),
  ENTRY("grd5-read", tests_grd5_read),
  ENTRY("grd5-string", tests_grd5_string),
  ENTRY("grd5-type", tests_grd5_type),
  ENTRY("grdx-svg", tests_grdx_svg),
  ENTRY("hash", tests_hash),
  ENTRY("path-base", tests_path_base),
  ENTRY("pg", tests_pg),
  ENTRY("pg-write", tests_pg_write),
  ENTRY("png", tests_png),
  ENTRY("png-write", tests_png_write),
  ENTRY("pov", tests_pov),
  ENTRY("pov-write", tests_pov_write),
  ENTRY("qgs", tests_qgs),
  ENTRY("qgs-list", tests_qgs_list),
  ENTRY("qgs-list-read", tests_qgs_list_read),
  ENTRY("qgs-list-write", tests_qgs_list_write),
  ENTRY("sao", tests_sao),
  ENTRY("sao-write", tests_sao_write),
  ENTRY("stdcol", tests_stdcol),
  ENTRY("svg", tests_svg),
  ENTRY("svg-list", tests_svg_list),
  ENTRY("svg-list-write", tests_svg_list_write),
  ENTRY("svg-list-read", tests_svg_list_read),
  ENTRY("svg-preview", tests_svg_preview),
  ENTRY("svg-write", tests_svg_write),
  ENTRY("tpm", tests_tpm),
  ENTRY("tpm-write", tests_tpm_write),
  ENTRY("u32map", tests_u32map),
  ENTRY("xml-id", tests_xml_id),
  CU_SUITE_INFO_NULL,
};

void tests_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr, "suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
