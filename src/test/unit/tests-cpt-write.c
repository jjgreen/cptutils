#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-write.h"
#include "fixture-cpt.h"

#include <cptutils/cpt-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_cpt_write[] = {
  {"fixtures", test_cpt_write_fixtures},
  {"no such directory", test_cpt_write_nosuchdir},
  {"opt, NULL", test_cpt_write_opt_null},
  {"opt, colour-join: space, RGB", test_cpt_write_opt_colour_join_space_rgb},
  {"opt, colour-join: space, HSV", test_cpt_write_opt_colour_join_space_hsv},
  {"opt, colour-join: glyph, RGB", test_cpt_write_opt_colour_join_glyph_rgb},
  {"opt, colour-join: glyph, HSV", test_cpt_write_opt_colour_join_glyph_hsv},
  {"opt, model-case: upper, RGB", test_cpt_write_opt_model_case_upper_rgb},
  {"opt, model-case: upper, HSV", test_cpt_write_opt_model_case_upper_hsv},
  {"opt, model-case: lower, RGB", test_cpt_write_opt_model_case_lower_rgb},
  {"opt, model-case: lower, HSV", test_cpt_write_opt_model_case_lower_hsv},
  {"options, GMT4", test_cpt_write_options_gmt4},
  {"options, GMT5", test_cpt_write_options_gmt5},
  {"options, GMT6", test_cpt_write_options_gmt6},
  {"options, default", test_cpt_write_options_default},
  {"options, bad version", test_cpt_write_options_bad_version},
  CU_TEST_INFO_NULL,
};

void test_cpt_write_fixtures(void)
{
  const char *files[] = {
    "blue.cpt",
    "Exxon88.cpt",
    "GMT4-gebco.cpt",
    "GMT4-haxby.cpt",
    "labels.cpt",
    "Onion_Rings.cpt",
    "pakistan.cpt",
    "RdBu_10.cpt",
    "subtle.cpt",
    "test.cpt",
    "tpsfhm.cpt"
  };
  size_t nfile = sizeof(files)/sizeof(char*);

  char
    dir[] = "tmp/test-cptwrite-fixture-XXXXXX",
    file[] = "tmp.cpt";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      cpt_t *cpt = load_cpt_fixture(files[i]);

      CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
      CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
      CU_ASSERT_EQUAL(cpt_write(cpt, NULL, path), 0);
      CU_ASSERT_EQUAL(access(path, F_OK), 0);

      unlink(path);
      cpt_destroy(cpt);
    }

  rmdir(dir);
}

void test_cpt_write_nosuchdir(void)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");

  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_NOT_EQUAL(cpt_write(cpt, NULL, "/no/such/directory"), 0);

  cpt_destroy(cpt);
}

static void path_wrap(void (*f)(const char*))
{
  char
    dir[] = "tmp/test-cptwrite-XXXXXX",
    file[] = "tmp.cpt";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  f(path);

  unlink(path);
  rmdir(dir);
}

static void opt_null(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  CU_ASSERT_EQUAL(cpt_write(cpt, NULL, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_null(void)
{
  path_wrap(opt_null);
}

static void opt_colour_join_space_rgb(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_space;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_colour_join_space_rgb(void)
{
  path_wrap(opt_colour_join_space_rgb);
}

static void opt_colour_join_space_hsv(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-cyclic.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_space;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_colour_join_space_hsv(void)
{
  path_wrap(opt_colour_join_space_hsv);
}

static void opt_colour_join_glyph_rgb(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_glyph;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_colour_join_glyph_rgb(void)
{
  path_wrap(opt_colour_join_glyph_rgb);
}

static void opt_colour_join_glyph_hsv(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-cyclic.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_glyph;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_colour_join_glyph_hsv(void)
{
  path_wrap(opt_colour_join_glyph_hsv);
}

static void opt_model_case_upper_rgb(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.model_case = cpt_model_case_upper;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_model_case_upper_rgb(void)
{
  path_wrap(opt_model_case_upper_rgb);
}

static void opt_model_case_upper_hsv(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-cyclic.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.model_case = cpt_model_case_upper;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_model_case_upper_hsv(void)
{
  path_wrap(opt_model_case_upper_hsv);
}

static void opt_model_case_lower_rgb(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_glyph;
  opt.model_case = cpt_model_case_lower;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_model_case_lower_rgb(void)
{
  path_wrap(opt_model_case_lower_rgb);
}

static void opt_model_case_lower_hsv(const char *path)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-cyclic.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  opt.colour_join = cpt_colour_join_glyph;
  opt.model_case = cpt_model_case_lower;

  CU_ASSERT_EQUAL(cpt_write(cpt, &opt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);
}

void test_cpt_write_opt_model_case_lower_hsv(void)
{
  path_wrap(opt_model_case_lower_hsv);
}

void test_cpt_write_options_gmt4(void)
{
  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(4, &opt), 0);
  CU_ASSERT_EQUAL(opt.model_case, cpt_model_case_upper);
  CU_ASSERT_EQUAL(opt.colour_join, cpt_colour_join_space);
  CU_ASSERT_EQUAL(opt.hinge.soft, false);
  CU_ASSERT_EQUAL(opt.hinge.hard, false);
  CU_ASSERT_EQUAL(opt.hinge.explicit, false);
  CU_ASSERT_EQUAL(opt.range, false);
}

void test_cpt_write_options_gmt5(void)
{
  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(5, &opt), 0);
  CU_ASSERT_EQUAL(opt.model_case, cpt_model_case_lower);
  CU_ASSERT_EQUAL(opt.colour_join, cpt_colour_join_glyph);
  CU_ASSERT_EQUAL(opt.hinge.soft, false);
  CU_ASSERT_EQUAL(opt.hinge.hard, false);
  CU_ASSERT_EQUAL(opt.hinge.explicit, true);
  CU_ASSERT_EQUAL(opt.range, true);
}

void test_cpt_write_options_gmt6(void)
{
  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(6, &opt), 0);
  CU_ASSERT_EQUAL(opt.model_case, cpt_model_case_lower);
  CU_ASSERT_EQUAL(opt.colour_join, cpt_colour_join_glyph);
  CU_ASSERT_EQUAL(opt.hinge.soft, true);
  CU_ASSERT_EQUAL(opt.hinge.hard, true);
  CU_ASSERT_EQUAL(opt.hinge.explicit, false);
  CU_ASSERT_EQUAL(opt.range, true);
}

void test_cpt_write_options_default(void)
{
  cptwrite_opt_t opt;
  CU_ASSERT_EQUAL(cpt_write_options(0, &opt), 0);
  CU_ASSERT_EQUAL(opt.model_case, cpt_model_case_upper);
  CU_ASSERT_EQUAL(opt.colour_join, cpt_colour_join_space);
  CU_ASSERT_EQUAL(opt.hinge.soft, false);
  CU_ASSERT_EQUAL(opt.hinge.hard, false);
  CU_ASSERT_EQUAL(opt.hinge.explicit, false);
  CU_ASSERT_EQUAL(opt.range, false);
}

void test_cpt_write_options_bad_version(void)
{
  cptwrite_opt_t opt;
  CU_ASSERT_NOT_EQUAL(cpt_write_options(-1, &opt), 0);
}
