#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-gpt-write.h"

#include <cptutils/gpt-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_gpt_write[] = {
  {"write", test_gpt_write_write},
  {"no such directory", test_gpt_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

static gpt_t* build_gpt(void)
{
  gpt_stop_t stops[] =
    {
     {0.0, {0.0, 0.0, 0.0}},
     {1.0, {0.0, 0.5, 1.0}}
    };
  size_t n = sizeof(stops) / sizeof(gpt_stop_t);
  gpt_t *gpt;

  if ((gpt = gpt_new()) == NULL)
    return NULL;

  if (gpt_stops_alloc(gpt, n) != 0)
    return NULL;

  for (size_t i = 0 ; i < n ; i++)
    gpt->stop[i] = stops[i];

  return gpt;
}

void test_gpt_write_write(void)
{
  gpt_t *gpt = build_gpt();
  CU_ASSERT_PTR_NOT_NULL_FATAL(gpt);

  char
    dir[] = "tmp/test-gpt-write-XXXXXX",
    file[] = "tmp.cpt";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(gpt_write(gpt, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);

  gpt_destroy(gpt);
}

void test_gpt_write_nosuchdir(void)
{
  gpt_t *gpt = build_gpt();
  CU_ASSERT_PTR_NOT_NULL_FATAL(gpt);

  const char path[] = "/no/such/directory";

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(gpt_write(gpt, path), 0);
  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);

  gpt_destroy(gpt);
}
