#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-gpt.h"

#include <cptutils/gpt.h>


CU_TestInfo tests_gpt[] = {
  {"simple", test_gpt_simple},
  CU_TEST_INFO_NULL,
};

void test_gpt_simple(void)
{
  gpt_t *gpt = gpt_new();

  CU_ASSERT_PTR_NOT_NULL(gpt);
  CU_ASSERT_EQUAL(gpt->n, 0);
  CU_ASSERT_PTR_NULL(gpt->stop);
  CU_ASSERT_EQUAL(gpt_stops_alloc(gpt, 5), 0);
  CU_ASSERT_PTR_NOT_NULL(gpt->stop);
  CU_ASSERT_EQUAL(gpt->n, 5);

  gpt_destroy(gpt);
}
