#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd3.h"

#include <cptutils/grd3.h>


CU_TestInfo tests_grd3[] = {
  {"constructor", test_grd3_new},
  CU_TEST_INFO_NULL,
};

void test_grd3_new(void)
{
  grd3_t *grd3 = grd3_new();

  CU_ASSERT_PTR_NOT_NULL(grd3);

  CU_ASSERT_PTR_NULL(grd3->name);
  CU_ASSERT_EQUAL(grd3->rgb.n, 0);
  CU_ASSERT_PTR_NULL(grd3->rgb.seg);
  CU_ASSERT_EQUAL(grd3->op.n, 0);
  CU_ASSERT_PTR_NULL(grd3->op.seg);

  grd3_destroy(grd3);
}
