#ifndef FIXTURE_GRD3_H
#define FIXTURE_GRD3_H

#include <cptutils/grd3.h>

grd3_t* load_grd3_fixture(const char*);

#endif
