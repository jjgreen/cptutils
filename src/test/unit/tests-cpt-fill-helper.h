#include <cptutils/cpt-fill.h>

cpt_fill_t build_cpt_fill_grey(int);
cpt_fill_t build_cpt_fill_rgb(int, int, int);
cpt_fill_t build_cpt_fill_hsv(double, double, double);
