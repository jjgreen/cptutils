#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-sao.h"
#include "tests-sao-helper.h"

#include <cptutils/sao.h>


CU_TestInfo tests_sao[] = {
  {"create", test_sao_create },
  {"push colours", test_sao_push },
  {"colour iterator", test_sao_each },
  CU_TEST_INFO_NULL,
};

void test_sao_create(void)
{
  sao_t *sao = sao_new();

  CU_ASSERT_PTR_NOT_NULL(sao);

  sao_destroy(sao);
}

void test_sao_push(void)
{
  sao_t *sao = sao_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(sao);

  CU_ASSERT_EQUAL(sao_red_push(sao, 0, 0.0), 0);
  CU_ASSERT_EQUAL(sao_red_push(sao, 1, 0.0), 0);
  CU_ASSERT_EQUAL(sao_green_push(sao, 0, 0.0), 0);
  CU_ASSERT_EQUAL(sao_green_push(sao, 1, 0.5), 0);
  CU_ASSERT_EQUAL(sao_blue_push(sao, 0, 0.0), 0);
  CU_ASSERT_EQUAL(sao_blue_push(sao, 1, 1.0), 0);

  sao_destroy(sao);
}

static int sum_stop_fn(double z, double red, void *v)
{
  double *A = v;
  *A += red;
  return 0;
}

void test_sao_each(void)
{
  sao_t *sao = build_sao();
  CU_ASSERT_PTR_NOT_NULL_FATAL(sao);

  double sum = 0.0;

  CU_ASSERT_EQUAL(sao_eachred(sao, sum_stop_fn, &sum), 0);
  CU_ASSERT_DOUBLE_EQUAL(sum, 1.0, 1e-10);

  sao_destroy(sao);
}
