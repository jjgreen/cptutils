#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-write.h"

#include <cptutils/comment-write.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


CU_TestInfo tests_comment_write[] = {
  {"example", test_comment_write_example},
  {"no such directory", test_comment_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

static comment_t* comment_example(void)
{
  comment_t *comment = comment_new();

  if (comment != NULL)
    {
      if ((comment_push(comment, "foo\n") == 0) &&
          (comment_push(comment, "bar") == 0) &&
          (comment_finalise(comment) == 0))
        return comment;

      comment_destroy(comment);
    }

  return NULL;
}

void test_comment_write_example(void)
{
  char path[] = "tmp/test-write-XXXXXX";
  int fd = mkstemp(path);

  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  comment_t *comment = comment_example();

  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);
  CU_ASSERT_EQUAL(comment_write(comment, path), 0);

  comment_destroy(comment);

  CU_ASSERT_EQUAL(close(fd), 0);
  CU_ASSERT_EQUAL(unlink(path), 0);
}

void test_comment_write_nosuchdir(void)
{
  char path[] = "tmp/no-such-dir/file.comments";
  comment_t *comment = comment_example();

  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);
  CU_ASSERT_NOT_EQUAL(comment_write(comment, path), 0);

  comment_destroy(comment);
}
