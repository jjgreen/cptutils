#include <CUnit/CUnit.h>

extern CU_TestInfo tests_btrace[];

void test_btrace_default(void);
void test_btrace_enable(void);
void test_btrace_disable(void);
void test_btrace_format(void);
void test_btrace_is_empty(void);
void test_btrace_count(void);
void test_btrace_add(void);
void test_btrace_print_plain(void);
void test_btrace_print_xml(void);
void test_btrace_print_json(void);
