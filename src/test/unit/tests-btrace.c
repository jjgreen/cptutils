#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-btrace.h"

#include <cptutils/btrace.h>

#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_btrace[] = {
  {"default", test_btrace_default},
  {"enable", test_btrace_enable},
  {"disable", test_btrace_disable},
  {"test empty", test_btrace_is_empty},
  {"format", test_btrace_format},
  {"count", test_btrace_count},
  {"add", test_btrace_add},
  {"print to file (plain)", test_btrace_print_plain},
  {"print to file (XML)", test_btrace_print_xml},
  {"print to file (JSON)", test_btrace_print_json},
  CU_TEST_INFO_NULL,
};

/* btrace should be disabled by default */

void test_btrace_default(void)
{
  CU_ASSERT_FALSE(btrace_is_enabled());
}

/* btrace_enable() should change the enabled state */

void test_btrace_enable(void)
{
  CU_ASSERT_FALSE(btrace_is_enabled());
  btrace_enable("foo");
  CU_ASSERT_TRUE(btrace_is_enabled());
  btrace_disable();
  CU_ASSERT_FALSE(btrace_is_enabled());
}

/* btrace_disable() should change the enabled state */

void test_btrace_disable(void)
{
  btrace_enable("foo");
  CU_ASSERT_TRUE(btrace_is_enabled());
  btrace_disable();
  CU_ASSERT_FALSE(btrace_is_enabled());
}

/* btrace_is_empty should initially be false */

void test_btrace_is_empty(void)
{
  CU_ASSERT_TRUE(btrace_is_empty());
  btrace_enable("foo");
  btrace("a message");
  CU_ASSERT_FALSE(btrace_is_empty());
  btrace_reset();
  CU_ASSERT_TRUE(btrace_is_empty());
  btrace_disable();
}

/* btrace_count() should return 0 by default */

void test_btrace_count(void)
{
  CU_ASSERT_EQUAL(btrace_count(), 0);
  btrace_enable("foo");
  btrace("a message");
  CU_ASSERT_EQUAL(btrace_count(), 1);
  btrace_reset();
  CU_ASSERT_EQUAL(btrace_count(), 0);
  btrace_disable();
}

/* get (integer) format from string */

void test_btrace_format(void)
{
  CU_ASSERT_EQUAL(btrace_format(NULL), BTRACE_NONE);
  CU_ASSERT_EQUAL(btrace_format("plain"), BTRACE_PLAIN);
  CU_ASSERT_EQUAL(btrace_format("xml"), BTRACE_XML);
#ifdef WITH_JSON
  CU_ASSERT_EQUAL(btrace_format("json"), BTRACE_JSON);
#else
  CU_ASSERT_EQUAL(btrace_format("json"), BTRACE_ERROR);
#endif
  CU_ASSERT_EQUAL(btrace_format("goats"), BTRACE_ERROR);
}

/* adding lines should increase the count */

void test_btrace_add(void)
{
  btrace_enable("foo");
  btrace("no arguments");
   btrace("%d arguments", 1);
  btrace("%d %s", 2, "arguments");
  CU_ASSERT_EQUAL(btrace_count(), 3);
  btrace_reset();
  btrace_disable();
}

/*
  print_plain should print lines to the specified FILE
  and the lines should start with the message added
*/

#define MESSAGE "a message"

/* just a stub at present */

static void test_print(int type)
{
  char path[] = "tmp/test-btrace-print-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);
  btrace_enable("foo");
  btrace(MESSAGE);
  btrace_print(path, type);
  btrace_reset();
  btrace_disable();
  unlink(path);
}

void test_btrace_print_plain(void)
{
  test_print(BTRACE_PLAIN);
}

void test_btrace_print_xml(void)
{
  test_print(BTRACE_XML);
}

void test_btrace_print_json(void)
{
#ifdef WITH_JSON
  test_print(BTRACE_JSON);
#endif
}
