#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd5-string.h"
#include "tests-grd5-helper.h"

#include <cptutils/grd5-string.h>


CU_TestInfo tests_grd5_string[] = {
  {"matcher", test_grd5_string_matches},
  CU_TEST_INFO_NULL,
};

void test_grd5_string_matches(void)
{
  grd5_string_t *str = build_grd5_string("frooble");

  CU_ASSERT_PTR_NOT_NULL_FATAL(str);
  CU_ASSERT_TRUE(grd5_string_matches(str, "frooble"));
  CU_ASSERT_FALSE(grd5_string_matches(str, "brooble"));

  grd5_string_destroy(str);
}
