#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-list-read.h"
#include "fixture.h"

#include <cptutils/svg-list-read.h>


CU_TestInfo tests_svg_list_read[] = {
  {"fixtures", test_svg_list_read_fixtures},
  {"file does not exist", test_svg_list_read_nofile},
  CU_TEST_INFO_NULL,
};

void test_svg_list_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "BrBG_10.svg",
    "eyes.svg",
    "french-flag.svg",
    "gradient-pastel-blue.svg",
    "style-space.svg",
    "lemon-lime.svg",
    "mad-ids.svg",
    "radial-eclipse.svg",
    "red-green-blue.svg",
    "subtle.svg",
    "rgb-percentage.svg"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT_FATAL(fixture(path, n, "svg", files[i]) < n);

      svg_list_t *svglist = svg_list_read(path);

      CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);
      CU_ASSERT_NOT_EQUAL(svg_list_size(svglist), 0);
      svg_list_destroy(svglist);
    }
}

void test_svg_list_read_nofile(void)
{
  svg_list_t *svglist = svg_list_read("tmp/no-such-file");
  CU_ASSERT_PTR_NULL(svglist);
}
