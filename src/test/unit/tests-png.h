#include <CUnit/CUnit.h>

extern CU_TestInfo tests_png[];

void test_png_new(void);
void test_png_new_zerowidth(void);
void test_png_new_zeroheight(void);
void test_png_destroy_null(void);
