#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd3-write.h"
#include "fixture-grd3.h"

#include <cptutils/grd3-write.h>

#include <stdio.h>
#include <unistd.h>


CU_TestInfo tests_grd3_write[] = {
  {"fixtures", test_grd3_write_fixtures},
  {"no such directory", test_grd3_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

void test_grd3_write_fixtures(void)
{
  const char *files[] =
    {
     "accented.jgd",
     "test.1.jgd",
     "test.2.jgd",
     "test.3.jgd",
     "test.4.jgd"
    };
  size_t nfile = sizeof(files) / sizeof(char*);

  char
    dir[] = "tmp/test-grd3-write-XXXXXX",
    file[] = "tmp.cpt";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      grd3_t *grd3 = load_grd3_fixture(files[i]);
      CU_ASSERT_PTR_NOT_NULL(grd3);

      CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
      CU_ASSERT_EQUAL(grd3_write(grd3, path), 0);
      CU_ASSERT_EQUAL(access(path, F_OK), 0);

      unlink(path);
      grd3_destroy(grd3);
    }

  rmdir(dir);
}

void test_grd3_write_nosuchdir(void)
{
  grd3_t *grd3 = load_grd3_fixture("accented.jgd");

  CU_ASSERT_PTR_NOT_NULL_FATAL(grd3);
  CU_ASSERT_NOT_EQUAL(grd3_write(grd3, "/no/such/directory"), 0);

  grd3_destroy(grd3);
}
