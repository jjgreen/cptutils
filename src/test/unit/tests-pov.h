#include <CUnit/CUnit.h>

extern CU_TestInfo tests_pov[];

void test_pov_create(void);
void test_pov_set_name_simple(void);
void test_pov_set_name_complex(void);
void test_pov_alloc_stops(void);
