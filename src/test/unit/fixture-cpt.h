#ifndef FIXTURE_CPT_H
#define FIXTURE_CPT_H

#include <cptutils/cpt.h>

cpt_t* load_cpt_fixture(const char*);

#endif
