#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture.h"

#include <stdio.h>

#define FIXTURE_BASE "../fixtures"


int fixture(char *buff, size_t n, const char *type, const char *file)
{
  return snprintf(buff, n, "%s/%s/%s", FIXTURE_BASE, type, file);
}
