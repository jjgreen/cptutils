#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-gmtcol.h"

#include <cptutils/gmtcol.h>


CU_TestInfo tests_gmtcol[] = {
  {"lookup from name", test_gmtcol_lookup},
  {"name is not in list", test_gmtcol_badname},
  CU_TEST_INFO_NULL,
};

#define COLOUR "lightslategray"

void test_gmtcol_lookup(void)
{
  const struct gmtcol_t *col = gmtcol(COLOUR);

  CU_ASSERT_PTR_NOT_NULL(col);

  CU_ASSERT_STRING_EQUAL(col->name, COLOUR);
  CU_ASSERT_EQUAL(col->r, 119);
  CU_ASSERT_EQUAL(col->g, 136);
  CU_ASSERT_EQUAL(col->b, 153);
}

void test_gmtcol_badname(void)
{
  CU_ASSERT_PTR_NULL(gmtcol("nosuchcolour"));
}
