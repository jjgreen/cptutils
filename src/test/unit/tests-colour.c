#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-colour.h"
#include "tests-colour-helper.h"

#include <cptutils/colour.h>


#define OCTEPS (1.0 / 256.0)

CU_TestInfo tests_colour[] = {
  {"hsvD to rgbD", test_colour_hsvD_to_rgbD},
  {"rgbD to hsvD", test_colour_rgbD_to_hsvD},
  {"rgb to rgbD", test_colour_rgb_to_rgbD},
  {"rgbD to rgb", test_colour_rgbD_to_rgb},
  {"rgb to hsv", test_colour_hsv_to_rgb},
  {"grey to rgbD", test_colour_grey_to_rgbD},
  {"parse RGB string", test_colour_parse_rgb},
  {"rgb interpolate rgb", test_colour_rgb_interpolate_rgb},
  {"rgb interpolate hsv", test_colour_rgb_interpolate_hsv},
  {"hsv interpolate rgb", test_colour_hsv_interpolate_rgb},
  {"hsv interpolate hsv", test_colour_hsv_interpolate_hsv},
  CU_TEST_INFO_NULL,
};

void test_colour_hsv_to_rgb(void)
{
  struct
  {
    hsv_t hsv;
    rgb_t rgb;
  } data[9] = {
    {{  0, 0.0, 0.0}, {  0,  0,    0}},
    {{  0, 0.0, 1.0}, {255, 255, 255}},
    {{  0, 1.0, 1.0}, {255,   0,   0}},
    {{120, 1.0, 1.0}, {  0, 255,   0}},
    {{240, 1.0, 1.0}, {  0,   0, 255}},
    {{ 60, 1.0, 1.0}, {255, 255,   0}},
    {{180, 1.0, 1.0}, {  0, 255, 255}},
    {{300, 1.0, 1.0}, {255,   0, 255}},
    {{  0, 0.0, 0.5}, {128, 128, 128}},
  };

  for (size_t i = 0 ; i < 9 ; i++)
    {
      rgb_t rgb_found;

      CU_ASSERT_EQUAL(hsv_to_rgb(data[i].hsv, &rgb_found), 0);
      CU_ASSERT_TRUE(rgb_equal(data[i].rgb, rgb_found));
    }
}

void test_colour_hsvD_to_rgbD(void)
{
  double eps = 1e-10, rgb[3];

  {
    double
      hsv[3] = {0.0, 0.5, 1.0},
      rgb_expected[3] = {1.0, 0.5, 0.5};

    CU_ASSERT_EQUAL(hsvD_to_rgbD(hsv, rgb), 0);
    CU_ASSERT_TRUE(triple_equal(rgb, rgb_expected, eps));
  }

  {
    double hsv[3] = {0.0, 0.0, 1.0}, rgb_expected[3] = {1.0, 1.0, 1.0};

    CU_ASSERT_EQUAL(hsvD_to_rgbD(hsv, rgb), 0);
    CU_ASSERT_TRUE(triple_equal(rgb, rgb_expected, eps));
  }
}

void test_colour_rgbD_to_hsvD(void)
{
  double eps = 1e-10, hsv[3];

  {
    double rgb[3] = {1.0, 0.5, 0.5}, hsv_expected[3] = {0.0, 0.5, 1.0};

    CU_ASSERT_EQUAL(rgbD_to_hsvD(rgb, hsv), 0);
    CU_ASSERT_TRUE(triple_equal(hsv, hsv_expected, eps));
  }

  {
    double rgb[3] = {1.0, 1.0, 1.0}, hsv_expected[3] = {0.0, 0.0, 1.0};

    CU_ASSERT_EQUAL(rgbD_to_hsvD(rgb, hsv), 0);
    CU_ASSERT_TRUE(triple_equal(hsv, hsv_expected, eps));
  }
}

void test_colour_rgbD_to_rgb(void)
{
  double rgbD[3] = {0, 0.5, 1};
  rgb_t rgb, rgb_expected = {0, 128, 255};

  CU_ASSERT_EQUAL(rgbD_to_rgb(rgbD, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb_expected));
}

void test_colour_rgb_to_rgbD(void)
{
  rgb_t rgb = {0, 128, 255};
  double eps = OCTEPS, rgbD[3],
    rgbD_expected[3] = {0, 0.5, 1};

  CU_ASSERT_EQUAL(rgb_to_rgbD(rgb, rgbD), 0);
  CU_ASSERT_TRUE(triple_equal(rgbD, rgbD_expected, eps));
}

void test_colour_grey_to_rgbD(void)
{
  int grey = 128;
  double eps = OCTEPS, rgbD[3],
    rgbD_expected[3] = {0.5, 0.5, 0.5};

  CU_ASSERT_EQUAL(grey_to_rgbD(grey, rgbD), 0);
  CU_ASSERT_TRUE(triple_equal(rgbD, rgbD_expected, eps));
}

void test_colour_parse_rgb(void)
{
  rgb_t rgb, rgb_expected = {10, 50, 200};

  CU_ASSERT_EQUAL(parse_rgb("10/50/200", &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb_expected));

  CU_ASSERT_NOT_EQUAL(parse_rgb("10/50", &rgb), 0);
}

void test_colour_rgb_interpolate_rgb(void)
{
  rgb_t rgb,
    rgb1 = build_rgb(0, 1, 2),
    rgb2 = build_rgb(2, 2, 2),
    rgb3 = build_rgb(4, 3, 2);

  CU_ASSERT_EQUAL(rgb_interpolate(model_rgb, 0.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb1));
  CU_ASSERT_EQUAL(rgb_interpolate(model_rgb, 0.5, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb2));
  CU_ASSERT_EQUAL(rgb_interpolate(model_rgb, 1.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb3));
}

void test_colour_rgb_interpolate_hsv(void)
{
  rgb_t rgb,
    rgb1 = build_rgb(255, 0, 0),
    rgb2 = build_rgb(255, 255, 0),
    rgb3 = build_rgb(0, 255, 0);

  CU_ASSERT_EQUAL(rgb_interpolate(model_hsv, 0.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb1));
  CU_ASSERT_EQUAL(rgb_interpolate(model_hsv, 0.5, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb2));
  CU_ASSERT_EQUAL(rgb_interpolate(model_hsv, 1.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb3));
}

void test_colour_hsv_interpolate_rgb(void)
{
  double eps = OCTEPS;
  hsv_t hsv,
    hsv1 = build_hsv(0, 0.5, 1),
    hsv2 = build_hsv(0, 0.666666, 0.75),
    hsv3 = build_hsv(0, 1, 0.5);

  CU_ASSERT_EQUAL(hsv_interpolate(model_rgb, 0.0, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv1, eps));
  CU_ASSERT_EQUAL(hsv_interpolate(model_rgb, 0.5, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv2, eps));
  CU_ASSERT_EQUAL(hsv_interpolate(model_rgb, 1.0, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv3, eps));
}

void test_colour_hsv_interpolate_hsv(void)
{
  double eps = OCTEPS;
  hsv_t hsv,
    hsv1 = build_hsv(0, 0.5,  1),
    hsv2 = build_hsv(45, 0.75, 0.75),
    hsv3 = build_hsv(90, 1, 0.5);

  CU_ASSERT_EQUAL(hsv_interpolate(model_hsv, 0.0, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv1, eps));
  CU_ASSERT_EQUAL(hsv_interpolate(model_hsv, 0.5, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv2, eps));
  CU_ASSERT_EQUAL(hsv_interpolate(model_hsv, 1.0, hsv1, hsv3, &hsv), 0);
  CU_ASSERT_TRUE(hsv_equal(hsv, hsv3, eps));
}
