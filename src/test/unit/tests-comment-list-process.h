#include <CUnit/CUnit.h>

extern CU_TestInfo tests_comment_list_process[];

void test_comment_list_process_write(void);
void test_comment_list_process_read(void);
void test_comment_list_process_generate(void);
void test_comment_list_process_none(void);
