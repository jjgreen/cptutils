#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-normalise.h"
#include "fixture-cpt.h"

#include <cptutils/cpt-normalise.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_cpt_normalise[] = {
  {"normalise, GMT v4", test_cpt_normalise_gmt4},
  {"normalise, GMT v5, unhinged", test_cpt_normalise_gmt5_unhinged},
  {"normalise, GMT v5, hard-hinged", test_cpt_normalise_gmt5_hard_hinged},
  {"normalise, GMT v5, soft-hinged", test_cpt_normalise_gmt5_soft_hinged},
  {"denormalise, GMT v4", test_cpt_denormalise_gmt4},
  {"denormalise, GMT v5, unhinged", test_cpt_denormalise_gmt5_unhinged},
  {"denormalise, GMT v5, hard-hinged", test_cpt_denormalise_gmt5_hard_hinged},
  {"denormalise, GMT v5, soft-hinged", test_cpt_denormalise_gmt5_soft_hinged},
  CU_TEST_INFO_NULL,
};

void test_cpt_normalise_gmt4(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -7000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  CU_ASSERT_EQUAL(cpt_normalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -7000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  cpt_destroy(cpt);
}

void test_cpt_normalise_gmt5_unhinged(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-bathy.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -8000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  CU_ASSERT_EQUAL(cpt_normalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -8000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  cpt_destroy(cpt);
}

void test_cpt_normalise_gmt5_hard_hinged(void)
{
  cpt_t *cpt = load_cpt_fixture("hinge-hard-denorm.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  CU_ASSERT_EQUAL(cpt_normalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  cpt_destroy(cpt);
}

void test_cpt_normalise_gmt5_soft_hinged(void)
{
  cpt_t *cpt = load_cpt_fixture("hinge-soft-denorm.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  CU_ASSERT_EQUAL(cpt_normalise(cpt, true, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  cpt_destroy(cpt);
}

void test_cpt_denormalise_gmt4(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -7000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  CU_ASSERT_EQUAL(cpt_denormalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -7000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  cpt_destroy(cpt);
}

void test_cpt_denormalise_gmt5_unhinged(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-bathy.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -8000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  CU_ASSERT_EQUAL(cpt_denormalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -8000, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 0, eps);

  cpt_destroy(cpt);
}

void test_cpt_denormalise_gmt5_hard_hinged(void)
{
  cpt_t *cpt = load_cpt_fixture("hinge-hard-norm.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  CU_ASSERT_EQUAL(cpt_denormalise(cpt, false, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  cpt_destroy(cpt);
}

void test_cpt_denormalise_gmt5_soft_hinged(void)
{
  cpt_t *cpt = load_cpt_fixture("hinge-soft-norm.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);

  double eps = 1e-16, range[2];

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_explicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 1, eps);
  CU_ASSERT_EQUAL(cpt_range_explicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  CU_ASSERT_EQUAL(cpt_denormalise(cpt, true, 0), 0);

  CU_ASSERT_EQUAL(cpt_range_type(cpt), range_implicit);
  CU_ASSERT_EQUAL(cpt_range_implicit(cpt, range), 0);
  CU_ASSERT_DOUBLE_EQUAL(range[0], -100, eps);
  CU_ASSERT_DOUBLE_EQUAL(range[1], 50, eps);

  cpt_destroy(cpt);
}
