#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-helper.h"

#include <stddef.h>

svg_t* build_svg(void)
{
  svg_t *svg;
  svg_stop_t stops[] =
    {
      {0.25, 0.5, {255,   0, 0}},
      {0.75, 0.5, {  0, 255, 0}}
    };
  size_t nstop = sizeof(stops)/sizeof(svg_stop_t);

  if ((svg = svg_new()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < nstop ; i++)
    if (svg_append(stops[i], svg) != 0)
      return NULL;

  return svg;
}
