#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-list.h"
#include "tests-comment-list-helper.h"

#include <cptutils/comment-list.h>


CU_TestInfo tests_comment_list[] = {
  {"constructor", test_comment_list_new},
  {"destroy NULL", test_comment_list_destroy_null},
  {"next", test_comment_list_next},
  {"revert", test_comment_list_revert},
  {"each", test_comment_list_each},
  {"find", test_comment_list_find},
  {"entry", test_comment_list_entry},
  CU_TEST_INFO_NULL,
};

void test_comment_list_new(void)
{
  comment_list_t *list = comment_list_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);
  comment_list_destroy(list);
}

void test_comment_list_destroy_null(void)
{
  comment_list_destroy(NULL);
}

void test_comment_list_next(void)
{
  comment_list_t *list = comment_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  for (size_t i = 0 ; i < 27 ; i++)
    {
      CU_ASSERT_PTR_NOT_NULL(comment_list_next(list));
      CU_ASSERT_EQUAL(comment_list_size(list), i + 1);
    }

  comment_list_destroy(list);
}

void test_comment_list_revert(void)
{
  comment_list_t *list = comment_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  CU_ASSERT_PTR_NOT_NULL(comment_list_next(list));
  CU_ASSERT_PTR_NOT_NULL(comment_list_next(list));
  CU_ASSERT_EQUAL(comment_list_revert(list), 0);
  CU_ASSERT_EQUAL(comment_list_revert(list), 0);
  CU_ASSERT_NOT_EQUAL(comment_list_revert(list), 0);

  comment_list_destroy(list);
}

static int count(comment_t *comment, void *v)
{
  int *n = v;
  (*n)++;
  return 0;
}

void test_comment_list_each(void)
{
  comment_list_t *list = build_comment_list(5);

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  int n = 0;

  CU_ASSERT_EQUAL(comment_list_each(list, count, &n), 0);
  CU_ASSERT_EQUAL(n, 5);

  comment_list_destroy(list);
}

static int break_third(comment_t *comment, void *v)
{
  int *n = v;
  return (++(*n) == 3);
}

void test_comment_list_find(void)
{
  comment_list_t *list = build_comment_list(50);

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  int n = 0;
  comment_t *comment = comment_list_find(list, break_third, &n);

  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

  comment_list_destroy(list);
}

void test_comment_list_entry(void)
{
  comment_list_t *list = build_comment_list(10);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  const comment_t *comment = comment_list_entry(list, 4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

  comment_list_destroy(list);
}
