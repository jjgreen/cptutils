#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-hash.h"

#include <cptutils/hash.h>


CU_TestInfo tests_hash[] = {
  {"hash32", test_hash_hash32},
  CU_TEST_INFO_NULL,
};

void test_hash_hash32(void)
{
  CU_ASSERT_EQUAL(hash32("Hello"), 0xf7d18982);
  CU_ASSERT_EQUAL(hash32("Goodbye"), 0xc232e1e8);
}
