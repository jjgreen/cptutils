#ifndef FIXTURE_SVG_H
#define FIXTURE_SVG_H

#include <cptutils/svg-list.h>

svg_list_t* load_svg_fixture(const char*);

#endif
