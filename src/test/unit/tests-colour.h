#include <CUnit/CUnit.h>

extern CU_TestInfo tests_colour[];

void test_colour_hsvD_to_rgbD(void);
void test_colour_rgbD_to_hsvD(void);
void test_colour_rgbD_to_rgb(void);
void test_colour_rgb_to_rgbD(void);
void test_colour_rgbD_to_hsvD(void);
void test_colour_hsvD_to_rgbD(void);
void test_colour_hsv_to_rgb(void);
void test_colour_grey_to_rgbD(void);
void test_colour_rgb_interpolate_rgb(void);
void test_colour_rgb_interpolate_hsv(void);
void test_colour_hsv_interpolate_rgb(void);
void test_colour_hsv_interpolate_hsv(void);
void test_colour_parse_rgb(void);
