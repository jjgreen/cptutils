#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-preview.h"

#include <cptutils/svg-preview.h>


CU_TestInfo tests_svg_preview[] = {
  {"geometry 50", test_svg_preview_geometry_50},
  {"geometry 50x40", test_svg_preview_geometry_50x40},
  {"geometry, use is false", test_svg_preview_geometry_false},
  CU_TEST_INFO_NULL,
};

void test_svg_preview_geometry_50(void)
{
  svg_preview_t preview = { .use = true };
  CU_ASSERT_EQUAL(svg_preview_geometry("50", &preview), 0);
  CU_ASSERT_EQUAL(preview.width, 50);
  CU_ASSERT_EQUAL(preview.height, 50);
}

void test_svg_preview_geometry_50x40(void)
{
  svg_preview_t preview = { .use = true };
  CU_ASSERT_EQUAL(svg_preview_geometry("50x40", &preview), 0);
  CU_ASSERT_EQUAL(preview.width, 50);
  CU_ASSERT_EQUAL(preview.height, 40);
}

void test_svg_preview_geometry_false(void)
{
  svg_preview_t preview = { .use = false };
  CU_ASSERT_EQUAL(svg_preview_geometry(NULL, &preview), 0);
}
