#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cpt[];

void test_cpt_new(void);
void test_cpt_seg_new(void);
void test_cpt_append(void);
void test_cpt_pop(void);
void test_cpt_shift(void);
void test_cpt_range_type_implicit(void);
void test_cpt_range_type_explicit(void);
void test_cpt_range_type_none(void);
void test_cpt_range_implicit_success(void);
void test_cpt_range_implicit_failure(void);
void test_cpt_range_explicit_success(void);
void test_cpt_range_explicit_failure(void);
void test_cpt_annotated_true(void);
void test_cpt_annotated_false(void);
