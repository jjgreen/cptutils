#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture.h"
#include "fixture-cpt.h"

#include <cptutils/cpt-read.h>

#include <stdlib.h>


cpt_t* load_cpt_fixture(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "cpt", file) >= n)
    return NULL;

  return cpt_read(path);
}
