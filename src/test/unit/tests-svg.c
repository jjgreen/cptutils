#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg.h"
#include "tests-svg-helper.h"
#include "tests-colour-helper.h"

#include <cptutils/svg.h>

#include <stdlib.h>

CU_TestInfo tests_svg[] = {
  {"create", test_svg_new},
  {"append", test_svg_append},
  {"prepend", test_svg_prepend},
  {"stop iterator", test_svg_each_stop},
  {"stop count", test_svg_num_stops},
  {"add explicit", test_svg_explicit},
  {"interpolate", test_svg_interpolate},
  {"flatten", test_svg_flatten},
  {"complete", test_svg_complete},
  {"basename", test_svg_basename},
  CU_TEST_INFO_NULL,
};

void test_svg_new(void)
{
  svg_t *svg = svg_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);
  CU_ASSERT_PTR_NULL(svg->nodes);
  CU_ASSERT_PTR_NOT_NULL(svg->name);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 0);

  svg_destroy(svg);
}

void test_svg_append(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  svg_stop_t
    stopA = { 0.25, 1.0, {255,   0, 0}},
    stopB = { 0.75, 1.0, {  0, 255, 0}};

  CU_ASSERT_EQUAL(svg_append(stopA, svg), 0);
  CU_ASSERT_EQUAL(svg_append(stopB, svg), 0);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 2);

  svg_destroy(svg);
}

void test_svg_prepend(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  svg_stop_t
    stopA = { 0.25, 1.0, {255,   0, 0}},
    stopB = { 0.75, 1.0, {  0, 255, 0}};

  CU_ASSERT_EQUAL(svg_prepend(stopA, svg), 0);
  CU_ASSERT_EQUAL(svg_prepend(stopB, svg), 0);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 2);

  svg_destroy(svg);
}

static int val_sum(svg_stop_t *stop, void *v)
{
  double *sum = v;
  *sum += stop->value;
  return 0;
}

void test_svg_each_stop(void)
{
  svg_t *svg = build_svg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  double sum = 0;

  CU_ASSERT_EQUAL(svg_each_stop(svg, val_sum, &sum), 0);
  CU_ASSERT_DOUBLE_EQUAL(sum, 1, 1e-10);

  svg_destroy(svg);
}

void test_svg_num_stops(void)
{
  svg_t *svg = build_svg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  CU_ASSERT_EQUAL(svg_num_stops(svg), 2);

  svg_destroy(svg);
}

void test_svg_explicit(void)
{
  svg_t *svg = build_svg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  CU_ASSERT_EQUAL(svg_num_stops(svg), 2);
  CU_ASSERT_EQUAL(svg_explicit(svg), 0);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 4);
  CU_ASSERT_EQUAL(svg_explicit(svg), 0);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 4);

  svg_destroy(svg);
}

void test_svg_interpolate(void)
{
  svg_t *svg = build_svg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  rgb_t rgb, rgb_expected = {128, 128, 0};
  double op, op_expected = 0.5;

  CU_ASSERT_EQUAL(svg_interpolate(svg, 0.5, &rgb, &op), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb_expected));
  CU_ASSERT_EQUAL(op, op_expected);

  CU_ASSERT_NOT_EQUAL(svg_interpolate(svg, 0.0, &rgb, &op), 0);

  svg_destroy(svg);
}

void test_svg_flatten(void)
{
  svg_t *svg = build_svg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  rgb_t rgb_bg = {0, 0, 0};

  CU_ASSERT_EQUAL(svg_flatten(svg, rgb_bg), 0);

  rgb_t rgb, rgb_expected = {64, 64, 0};
  double op, op_expected = 1.0;

  CU_ASSERT_EQUAL(svg_interpolate(svg, 0.5, &rgb, &op), 0);
  CU_ASSERT_TRUE(rgb_equal(rgb, rgb_expected));
  CU_ASSERT_EQUAL(op, op_expected);

  svg_destroy(svg);
}

static void test_svg_complete_name(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  sprintf((char*)svg->name, "07:arse");
  CU_ASSERT_EQUAL(svg_complete(svg), 0);
  CU_ASSERT_EQUAL(strcmp((char*)svg->id, "id-07-arse"), 0);

  svg_destroy(svg);
}

static void test_svg_complete_id_prefixed(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  sprintf((char*)svg->id, "id-07");
  CU_ASSERT_EQUAL(svg_complete(svg), 0);
  CU_ASSERT_EQUAL(strcmp((char*)svg->name, "07"), 0);

  svg_destroy(svg);
}

static void test_svg_complete_id_unprefixed(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  sprintf((char*)svg->id, "07");
  CU_ASSERT_EQUAL(svg_complete(svg), 0);
  CU_ASSERT_EQUAL(strcmp((char*)svg->name, "07"), 0);

  svg_destroy(svg);
}

void test_svg_complete(void)
{
  test_svg_complete_name();
  test_svg_complete_id_prefixed();
  test_svg_complete_id_unprefixed();
}

void test_svg_basename(void)
{
  svg_t *svg = svg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  sprintf((char*)svg->id, "id-base");

  char *base = svg_basename(svg);
  CU_ASSERT_PTR_NOT_NULL_FATAL(base);

  CU_ASSERT_EQUAL(strcmp(base, "base"), 0);

  svg_destroy(svg);
  free(base);
}
