#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt.h"
#include "tests-cpt-fill-helper.h"
#include "tests-cpt-helper.h"

#include "fixture-cpt.h"

#include <cptutils/cpt.h>


CU_TestInfo tests_cpt[] = {
  {"new", test_cpt_new},
  {"segment new", test_cpt_seg_new},
  {"segment append", test_cpt_append},
  {"segment pop", test_cpt_pop},
  {"segment shift", test_cpt_shift},
  {"range-type, implicit", test_cpt_range_type_implicit},
  {"range-type, explicit", test_cpt_range_type_explicit},
  {"range-type, none", test_cpt_range_type_none},
  {"range-implicit, success", test_cpt_range_implicit_success},
  {"range-implicit, failure", test_cpt_range_implicit_failure},
  {"range-explicit, success", test_cpt_range_explicit_success},
  {"range-explicit, failure", test_cpt_range_explicit_failure},
  {"annotated, true", test_cpt_annotated_true},
  {"annotated, false", test_cpt_annotated_false},
  CU_TEST_INFO_NULL,
};

void test_cpt_new(void)
{
  cpt_t *cpt = cpt_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_EQUAL(cpt->name, NULL);
  CU_ASSERT_EQUAL(cpt->model, model_rgb);
  CU_ASSERT_EQUAL(cpt->interpolate, model_rgb);
  CU_ASSERT_EQUAL(cpt->segment, NULL);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 0);

  cpt_destroy(cpt);
}

void test_cpt_seg_new(void)
{
  cpt_seg_t *cpt_seg = cpt_seg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt_seg);
  cpt_seg_destroy(cpt_seg);
}

void test_cpt_append(void)
{
  cpt_t *cpt = cpt_new();
  cpt_seg_t *cpt_seg = cpt_seg_new();

  CU_ASSERT_EQUAL(cpt_append(cpt_seg, cpt), 0);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 1);

  cpt_destroy(cpt);
}

void test_cpt_pop(void)
{
  cpt_fill_t
    grey100 = build_cpt_fill_grey(100),
    grey200 = build_cpt_fill_grey(200);
  cpt_seg_t
    *seg1 = build_segment(grey100, grey200, 0.0, 1.0),
    *seg2 = build_segment(grey100, grey200, 1.0, 2.0);

  cpt_t *cpt = cpt_new();

  CU_ASSERT_EQUAL(cpt_append(seg1, cpt), 0);
  CU_ASSERT_EQUAL(cpt_append(seg2, cpt), 0);

  CU_ASSERT_EQUAL(cpt_nseg(cpt), 2);

  cpt_seg_t *seg;

  seg = cpt_pop(cpt);

  CU_ASSERT_PTR_NOT_NULL(seg);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 1);
  CU_ASSERT_EQUAL(seg, seg1);

  seg = cpt_pop(cpt);

  CU_ASSERT_PTR_NOT_NULL(seg);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 0);
  CU_ASSERT_EQUAL(seg, seg2);

  cpt_seg_destroy(seg1);
  cpt_seg_destroy(seg2);

  cpt_destroy(cpt);
}

void test_cpt_shift(void)
{
  cpt_fill_t
    grey100 = build_cpt_fill_grey(100),
    grey200 = build_cpt_fill_grey(200);
  cpt_seg_t
    *seg1 = build_segment(grey100, grey200, 0.0, 1.0),
    *seg2 = build_segment(grey100, grey200, 1.0, 2.0);

  cpt_t *cpt = cpt_new();

  CU_ASSERT_EQUAL(cpt_append(seg1, cpt), 0);
  CU_ASSERT_EQUAL(cpt_append(seg2, cpt), 0);

  CU_ASSERT_EQUAL(cpt_nseg(cpt), 2);

  cpt_seg_t *seg;

  seg = cpt_shift(cpt);

  CU_ASSERT_PTR_NOT_NULL(seg);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 1);
  CU_ASSERT_EQUAL(seg, seg2);

  seg = cpt_shift(cpt);

  CU_ASSERT_PTR_NOT_NULL(seg);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 0);
  CU_ASSERT_EQUAL(seg, seg1);

  cpt_seg_destroy(seg1);
  cpt_seg_destroy(seg2);

  cpt_destroy(cpt);
}

void test_cpt_range_type_implicit(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);
  cpt_range_type_t type = cpt_range_type(cpt);
  CU_ASSERT_EQUAL(type, range_implicit);
  cpt_destroy(cpt);
}

void test_cpt_range_type_explicit(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-bathy.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);
  cpt_range_type_t type = cpt_range_type(cpt);
  CU_ASSERT_EQUAL(type, range_explicit);
  cpt_destroy(cpt);
}

void test_cpt_range_type_none(void)
{
  cpt_t *cpt = cpt_new();
  CU_ASSERT_PTR_NOT_NULL(cpt);
  cpt_range_type_t type = cpt_range_type(cpt);
  CU_ASSERT_EQUAL(type, range_none);
  cpt_destroy(cpt);
}

void test_cpt_range_implicit_success(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);

  double z[2];
  CU_ASSERT_EQUAL_FATAL(cpt_range_implicit(cpt, z), 0);

  CU_ASSERT_EQUAL(z[0], -7000);
  CU_ASSERT_EQUAL(z[1], 0);

  cpt_destroy(cpt);
}

void test_cpt_range_implicit_failure(void)
{
  cpt_t *cpt = cpt_new();
  CU_ASSERT_PTR_NOT_NULL(cpt);

  double z[2];
  CU_ASSERT_NOT_EQUAL(cpt_range_implicit(cpt, z), 0);

  cpt_destroy(cpt);
}

void test_cpt_range_explicit_success(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT5-bathy.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);

  double z[2];
  CU_ASSERT_EQUAL_FATAL(cpt_range_explicit(cpt, z), 0);

  CU_ASSERT_EQUAL(z[0], -8000);
  CU_ASSERT_EQUAL(z[1], 0);

  cpt_destroy(cpt);
}

void test_cpt_range_explicit_failure(void)
{
  cpt_t *cpt = cpt_new();
  CU_ASSERT_PTR_NOT_NULL(cpt);

  double z[2];
  CU_ASSERT_NOT_EQUAL(cpt_range_explicit(cpt, z), 0);

  cpt_destroy(cpt);
}

void test_cpt_annotated_true(void)
{
  cpt_t *cpt = load_cpt_fixture("honeycomb-20.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);
  CU_ASSERT_TRUE(cpt_annotated(cpt));
  cpt_destroy(cpt);
}

void test_cpt_annotated_false(void)
{
  cpt_t *cpt = load_cpt_fixture("blue.cpt");
  CU_ASSERT_PTR_NOT_NULL(cpt);
  CU_ASSERT_FALSE(cpt_annotated(cpt));
  cpt_destroy(cpt);
}
