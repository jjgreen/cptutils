#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-colour-model.h"

#include <cptutils/colour-model.h>


CU_TestInfo tests_colour_model[] = {
  {"model name", test_colour_model_name},
  CU_TEST_INFO_NULL,
};

void test_colour_model_name(void)
{
  CU_ASSERT_STRING_EQUAL("RGB", model_name(model_rgb));
  CU_ASSERT_STRING_EQUAL("HSV", model_name(model_hsv));
  CU_ASSERT_STRING_EQUAL("CMYK", model_name(model_cmyk));
}
