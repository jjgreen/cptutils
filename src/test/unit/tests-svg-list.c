#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-list.h"
#include "tests-svg-list-helper.h"

#include <cptutils/svg-list.h>


CU_TestInfo tests_svg_list[] = {
  {"constructor", test_svg_list_new},
  {"destroy NULL", test_svg_list_destroy_null},
  {"next", test_svg_list_next},
  {"revert", test_svg_list_revert},
  {"each", test_svg_list_each},
  {"find", test_svg_list_find},
  {"entry", test_svg_list_entry},
  CU_TEST_INFO_NULL,
};

void test_svg_list_new(void)
{
  svg_list_t *svglist = svg_list_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);
  svg_list_destroy(svglist);
}

void test_svg_list_destroy_null(void)
{
  svg_list_destroy(NULL);
}

void test_svg_list_next(void)
{
  svg_list_t *svglist = svg_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);

  for (size_t i = 0 ; i < 27 ; i++)
    {
      CU_ASSERT_PTR_NOT_NULL(svg_list_next(svglist));
      CU_ASSERT_EQUAL(svg_list_size(svglist), i + 1);
    }

  svg_list_destroy(svglist);
}

void test_svg_list_revert(void)
{
  svg_list_t *svglist = svg_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);

  CU_ASSERT_PTR_NOT_NULL(svg_list_next(svglist));
  CU_ASSERT_PTR_NOT_NULL(svg_list_next(svglist));
  CU_ASSERT_EQUAL(svg_list_revert(svglist), 0);
  CU_ASSERT_EQUAL(svg_list_revert(svglist), 0);
  CU_ASSERT_NOT_EQUAL(svg_list_revert(svglist), 0);

  svg_list_destroy(svglist);
}

static int count(svg_t *svg, void *v)
{
  int *n = v;
  (*n)++;
  return 0;
}

void test_svg_list_each(void)
{
  svg_list_t *svglist = build_svg_list(5);

  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);

  int n = 0;

  CU_ASSERT_EQUAL(svg_list_each(svglist, count, &n), 0);
  CU_ASSERT_EQUAL(n, 5);

  svg_list_destroy(svglist);
}

static int break_third(svg_t *svg, void *v)
{
  int *n = v;
  return (++(*n) == 3);
}

void test_svg_list_find(void)
{
  svg_list_t *svglist = build_svg_list(50);

  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);

  int n = 0;
  svg_t *svg = svg_list_find(svglist, break_third, &n);

  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);
  CU_ASSERT_STRING_EQUAL((const char*)svg->name, "id-02");

  svg_list_destroy(svglist);
}

void test_svg_list_entry(void)
{
  svg_list_t *svglist = build_svg_list(10);

  CU_ASSERT_PTR_NOT_NULL_FATAL(svglist);

  const svg_t *svg = svg_list_entry(svglist, 4);

  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);
  CU_ASSERT_STRING_EQUAL((const char*)svg->name, "id-04");

  svg_list_destroy(svglist);
}
