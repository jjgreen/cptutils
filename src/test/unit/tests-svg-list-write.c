#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-list-write.h"
#include "fixture-svg.h"

#include <cptutils/svg-list-write.h>

#include <stdio.h>
#include <unistd.h>

CU_TestInfo tests_svg_list_write[] = {
  {"fixtures", test_svg_list_write_fixtures},
  {"no such directory", test_svg_list_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

void test_svg_list_write_fixtures(void)
{
  const char *files[] = {
    "BrBG_10.svg",
    "eyes.svg",
    "french-flag.svg",
    "gradient-pastel-blue.svg",
    "style-space.svg",
    "lemon-lime.svg",
    "mad-ids.svg",
    "radial-eclipse.svg",
    "red-green-blue.svg",
    "subtle.svg"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  char
    dir[] = "tmp/test-svgwrite-XXXXXX",
    file[] = "tmp.svg";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      svg_list_t *list = load_svg_fixture(files[i]);

      CU_ASSERT_PTR_NOT_NULL_FATAL(list);

      svg_preview_t preview;

      if (svg_list_size(list) > 1)
        preview.use = false;
      else
        {
          preview.use = true;
          CU_ASSERT_EQUAL_FATAL(svg_preview_geometry("50x40", &preview), 0);
        }

      CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
      CU_ASSERT_EQUAL(svg_list_write(list, path, &preview, svg_version_11), 0);
      CU_ASSERT_EQUAL(access(path, F_OK), 0);
      unlink(path);

      CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
      CU_ASSERT_EQUAL(svg_list_write(list, path, &preview, svg_version_20), 0);
      CU_ASSERT_EQUAL(access(path, F_OK), 0);
      unlink(path);

      svg_list_destroy(list);
    }

  rmdir(dir);
}

void test_svg_list_write_nosuchdir(void)
{
  svg_list_t *list = load_svg_fixture("lemon-lime.svg");

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  svg_preview_t preview = { .use = true };

  CU_ASSERT_EQUAL(svg_preview_geometry("50x40", &preview), 0);

  const char path[] = "/no/such/directory/exists";

  CU_ASSERT_NOT_EQUAL_FATAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(svg_list_write(list, path, &preview, svg_version_11), 0);

  svg_list_destroy(list);
}
