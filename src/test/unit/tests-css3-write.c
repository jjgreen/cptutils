#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-css3-write.h"

#include <cptutils/css3-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_css3_write[] = {
  {"simple", test_css3_write_simple},
  CU_TEST_INFO_NULL,
};

/* build a css3 and write it  */

void test_css3_write_simple(void)
{
  css3_t *css3 = css3_new();

  CU_ASSERT_PTR_NOT_NULL(css3);
  CU_ASSERT_EQUAL(css3_stops_alloc(css3, 2), 0);
  CU_ASSERT_EQUAL(css3->n, 2);
  CU_ASSERT_EQUAL(css3->angle, 0);

  css3_stop_t stops[2] =
    {
      {  0, 1, {128,  64,   0}},
      {100, 1, {  0, 128, 255}}
    };

  for (size_t i = 0 ; i < 2 ; i++)
    css3->stop[i] = stops[i];

  char path[] = "tmp/test-css3writeXXXXXX";

  CU_ASSERT_NOT_EQUAL(mkstemp(path), -1);
  CU_ASSERT_EQUAL(css3_write(css3, path), 0);

  unlink(path);
  css3_destroy(css3);
}
