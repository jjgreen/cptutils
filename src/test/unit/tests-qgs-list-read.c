#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-qgs-list-read.h"
#include "fixture.h"

#include <cptutils/qgs-list-read.h>

CU_TestInfo tests_qgs_list_read[] = {
  {"fixtures", test_qgs_list_read_fixtures},
  {"fixtures in detail", test_qgs_list_read_fixture_elevation},
  {"file does not exist", test_qgs_list_read_nofile},
  {"part-defective", test_qgs_list_read_part_defective},
  CU_TEST_INFO_NULL,
};

/* check no errors on read for a number of fixtures */

void test_qgs_list_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "elevation.xml",
    "usgs-gswa2.xml",
    "vasarely.xml",
    "parula.xml"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT_FATAL(fixture(path, n, "qgs", files[i]) < n);
      qgs_list_t *list = qgs_list_read(path);
      CU_ASSERT_PTR_NOT_NULL_FATAL(list);
      qgs_list_destroy(list);
    }
}

/* one fixture in excruciating detail */

void test_qgs_list_read_fixture_elevation(void)
{
  size_t n = 1024;
  char path[n];

  CU_ASSERT_FATAL(fixture(path, n, "qgs", "elevation.xml") < n);

  qgs_list_t *list = qgs_list_read(path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);
  CU_ASSERT_EQUAL_FATAL(qgs_list_size(list), 2);

  double eps = 1e-8;
  const qgs_t *qgs;
  qgs_entry_t entry;

  qgs = qgs_list_entry(list, 0);
  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);

  CU_ASSERT_STRING_EQUAL(qgs->name, "elevation1");
  CU_ASSERT_EQUAL(qgs->type, QGS_TYPE_INTERPOLATED);
  CU_ASSERT_EQUAL_FATAL(qgs->n, 5);

  entry = qgs->entries[0];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 74);
  CU_ASSERT_EQUAL(entry.rgb.green, 156);
  CU_ASSERT_EQUAL(entry.rgb.blue, 15);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[1];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.25, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 250);
  CU_ASSERT_EQUAL(entry.rgb.blue, 104);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[2];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.5, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 179);
  CU_ASSERT_EQUAL(entry.rgb.blue, 38);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[3];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.75, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 146);
  CU_ASSERT_EQUAL(entry.rgb.green, 100);
  CU_ASSERT_EQUAL(entry.rgb.blue, 30);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[4];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 1.0, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 255);
  CU_ASSERT_EQUAL(entry.rgb.blue, 255);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  qgs = qgs_list_entry(list, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);

  CU_ASSERT_STRING_EQUAL(qgs->name, "elevation2");
  CU_ASSERT_EQUAL(qgs->type, QGS_TYPE_INTERPOLATED);
  CU_ASSERT_EQUAL_FATAL(qgs->n, 5);

  entry = qgs->entries[0];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 138);
  CU_ASSERT_EQUAL(entry.rgb.green, 205);
  CU_ASSERT_EQUAL(entry.rgb.blue, 121);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[1];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.25, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 250);
  CU_ASSERT_EQUAL(entry.rgb.blue, 191);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[2];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.5, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 220);
  CU_ASSERT_EQUAL(entry.rgb.blue, 143);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[3];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 0.75, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 160);
  CU_ASSERT_EQUAL(entry.rgb.green, 132);
  CU_ASSERT_EQUAL(entry.rgb.blue, 93);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  entry = qgs->entries[4];
  CU_ASSERT_DOUBLE_EQUAL(entry.value, 1.0, eps);
  CU_ASSERT_EQUAL(entry.rgb.red, 255);
  CU_ASSERT_EQUAL(entry.rgb.green, 255);
  CU_ASSERT_EQUAL(entry.rgb.blue, 255);
  CU_ASSERT_EQUAL(entry.opacity, 255);

  qgs_list_destroy(list);
}

void test_qgs_list_read_nofile(void)
{
  qgs_list_t *list = qgs_list_read("tmp/no-such-file");
  CU_ASSERT_PTR_NULL(list);
}

/*
  uses the missing-color1.xml, which has 2 colorramps, the first
  of which is defective (color1 removed), the point being that the
  other one (which is not defective) should be read OK
*/

void test_qgs_list_read_part_defective(void)
{
  size_t n = 1024;
  char path[n];

  CU_ASSERT_FATAL(fixture(path, n, "qgs", "missing-color1.xml") < n);

  qgs_list_t *list = qgs_list_read(path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);
  CU_ASSERT_EQUAL_FATAL(qgs_list_size(list), 1);

  const qgs_t *qgs = qgs_list_entry(list, 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);
  CU_ASSERT_STRING_EQUAL(qgs->name, "elevation2");

  qgs_list_destroy(list);
}
