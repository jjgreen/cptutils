#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-stdcol.h"

#include <cptutils/stdcol.h>


CU_TestInfo tests_stdcol[] = {
  {"lookup from name", test_stdcol_lookup},
  {"name is not in list", test_stdcol_badname},
  CU_TEST_INFO_NULL,
};

#define COLOUR "lightslategray"

void test_stdcol_lookup(void)
{
  const struct stdcol_t *col = stdcol(COLOUR);

  CU_ASSERT_PTR_NOT_NULL_FATAL(col);

  CU_ASSERT_STRING_EQUAL(col->name, COLOUR);
  CU_ASSERT_EQUAL(col->r, 119);
  CU_ASSERT_EQUAL(col->g, 136);
  CU_ASSERT_EQUAL(col->b, 153);
}

void test_stdcol_badname(void)
{
  CU_ASSERT_PTR_NULL(stdcol("nosuchcolour"));
}
