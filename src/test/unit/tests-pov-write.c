#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-pov-write.h"

#include <cptutils/pov-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_pov_write[] = {
  {"two-stop example", test_pov_write_2stop },
  CU_TEST_INFO_NULL,
};

static pov_t* build_pov(void)
{
  pov_t *pov;

  if ((pov = pov_new()) == NULL)
    return NULL;

  size_t changed;

  if (pov_set_name(pov, "frooble", &changed) != 0)
    return NULL;

  if (pov_stops_alloc(pov, 2) != 0)
    return NULL;

  pov_stop_t stop[2] =
    {
      {0.0, {0.0, 0.0, 0.0, 0.0}},
      {1.0, {1.0, 0.5, 0.0, 0.0}},
    };

  for (size_t i = 0 ; i < 2 ; i++)
    pov->stop[i] = stop[i];

  return pov;
}

void test_pov_write_2stop(void)
{
  pov_t *pov = build_pov();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pov);

  char
    dir[] = "tmp/test-povwrite-XXXXXX",
    file[] = "tmp.pov";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(pov_write(pov, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);

  pov_destroy(pov);
}
