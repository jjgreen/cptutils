#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-qgs-list.h"
#include "tests-qgs-list-helper.h"

#include <cptutils/qgs-list.h>


CU_TestInfo tests_qgs_list[] = {
  {"constructor", test_qgs_list_new},
  {"destroy NULL", test_qgs_list_destroy_null},
  {"next", test_qgs_list_next},
  {"revert", test_qgs_list_revert},
  {"each", test_qgs_list_each},
  {"find", test_qgs_list_find},
  {"entry", test_qgs_list_entry},
  CU_TEST_INFO_NULL,
};

void test_qgs_list_new(void)
{
  qgs_list_t *qgslist = qgs_list_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);
  qgs_list_destroy(qgslist);
}

void test_qgs_list_destroy_null(void)
{
  qgs_list_destroy(NULL);
}

void test_qgs_list_next(void)
{
  qgs_list_t *qgslist = qgs_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);

  for (size_t i = 0 ; i < 27 ; i++)
    {
      CU_ASSERT_PTR_NOT_NULL(qgs_list_next(qgslist));
      CU_ASSERT_EQUAL(qgs_list_size(qgslist), i + 1);
    }

  qgs_list_destroy(qgslist);
}

void test_qgs_list_revert(void)
{
  qgs_list_t *qgslist = qgs_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);

  CU_ASSERT_PTR_NOT_NULL(qgs_list_next(qgslist));
  CU_ASSERT_PTR_NOT_NULL(qgs_list_next(qgslist));
  CU_ASSERT_EQUAL(qgs_list_revert(qgslist), 0);
  CU_ASSERT_EQUAL(qgs_list_revert(qgslist), 0);
  CU_ASSERT_NOT_EQUAL(qgs_list_revert(qgslist), 0);

  qgs_list_destroy(qgslist);
}

static int count(qgs_t *qgs, void *v)
{
  int *n = v;
  (*n)++;
  return 0;
}

void test_qgs_list_each(void)
{
  qgs_list_t *qgslist = build_qgs_list(5);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);

  int n = 0;

  CU_ASSERT_EQUAL(qgs_list_each(qgslist, count, &n), 0);
  CU_ASSERT_EQUAL(n, 5);

  qgs_list_destroy(qgslist);
}

static int break_third(qgs_t *qgs, void *v)
{
  int *n = v;
  return (++(*n) == 3);
}

void test_qgs_list_find(void)
{
  qgs_list_t *qgslist = build_qgs_list(50);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);

  int n = 0;
  qgs_t *qgs = qgs_list_find(qgslist, break_third, &n);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);
  CU_ASSERT_STRING_EQUAL((const char*)qgs->name, "id-02");

  qgs_list_destroy(qgslist);
}

void test_qgs_list_entry(void)
{
  qgs_list_t *qgslist = build_qgs_list(10);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgslist);

  const qgs_t *qgs = qgs_list_entry(qgslist, 4);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);
  CU_ASSERT_STRING_EQUAL((const char*)qgs->name, "id-04");

  qgs_list_destroy(qgslist);
}
