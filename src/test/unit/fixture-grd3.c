#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture.h"
#include "fixture-grd3.h"

#include <cptutils/grd3-read.h>

#include <stdlib.h>


grd3_t* load_grd3_fixture(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "grd3", file) >= n)
    return NULL;

  return grd3_read(path);
}
