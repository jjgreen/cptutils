#include <stdbool.h>
#include <cptutils/colour.h>

bool rgb_equal(rgb_t, rgb_t);
bool hsv_equal(hsv_t, hsv_t, double);
bool triple_equal(double*, double*, double);
rgb_t build_rgb(int, int, int);
hsv_t build_hsv(double, double, double);
