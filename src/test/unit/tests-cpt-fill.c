#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-fill.h"
#include "tests-cpt-fill-helper.h"

#include <cptutils/cpt-fill.h>


CU_TestInfo tests_cpt_fill[] = {
  {"equality", test_cpt_fill_eq},
  {"interpolation", test_cpt_fill_interp},
  CU_TEST_INFO_NULL,
};

void test_cpt_fill_eq(void)
{
  cpt_fill_t
    g1 = build_cpt_fill_grey(1),
    g2 = build_cpt_fill_grey(2),
    rgb1 = build_cpt_fill_rgb(1, 1, 1),
    rgb2 = build_cpt_fill_rgb(2, 2, 2);

  CU_ASSERT_TRUE(cpt_fill_eq(g1, g1));
  CU_ASSERT_FALSE(cpt_fill_eq(g1, g2));
  CU_ASSERT_FALSE(cpt_fill_eq(g2, g1));

  CU_ASSERT_FALSE(cpt_fill_eq(g1, rgb1));
  CU_ASSERT_FALSE(cpt_fill_eq(rgb1, g1));

  CU_ASSERT_TRUE(cpt_fill_eq(rgb1, rgb1));
  CU_ASSERT_FALSE(cpt_fill_eq(rgb2, rgb1));
  CU_ASSERT_FALSE(cpt_fill_eq(rgb1, rgb2));
}

static void test_cpt_fill_interp_grey(void)
{
  cpt_fill_t g,
    g1 = build_cpt_fill_grey(1),
    g2 = build_cpt_fill_grey(2),
    g3 = build_cpt_fill_grey(3);

  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 0.0, g1, g3, &g), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(g, g1));
  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 0.5, g1, g3, &g), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(g, g2));
  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 1.0, g1, g3, &g), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(g, g3));
}

static void test_cpt_fill_interp_rgb(void)
{
  cpt_fill_t rgb,
    rgb1 = build_cpt_fill_rgb(1, 1, 1),
    rgb2 = build_cpt_fill_rgb(2, 2, 2),
    rgb3 = build_cpt_fill_rgb(3, 3, 3);

  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 0.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(rgb, rgb1));
  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 0.5, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(rgb, rgb2));
  CU_ASSERT_EQUAL(cpt_fill_interpolate(model_rgb, 1.0, rgb1, rgb3, &rgb), 0);
  CU_ASSERT_TRUE(cpt_fill_eq(rgb, rgb3));
}

void test_cpt_fill_interp(void)
{
  test_cpt_fill_interp_grey();
  test_cpt_fill_interp_rgb();
}
