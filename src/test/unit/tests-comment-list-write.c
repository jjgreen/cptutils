#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-list-write.h"
#include "tests-comment-list-helper.h"

#include <cptutils/comment-list-write.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


CU_TestInfo tests_comment_list_write[] = {
  {"single", test_comment_list_write_single},
  {"multiple", test_comment_list_write_multiple},
  {"no such directory", test_comment_list_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

void case_comment_list_write(size_t n)
{
  comment_list_t *list = build_comment_list(n);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  char
    dir[] = "tmp/test-comment-list-write-XXXXXX",
    file[] = "tmp.comments";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(comment_list_write(list, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
}

void test_comment_list_write_single(void)
{
  case_comment_list_write(1);
}

void test_comment_list_write_multiple(void)
{
  case_comment_list_write(10);
}

void test_comment_list_write_nosuchdir(void)
{
  char path[] = "tmp/no-such-dir/file.comments";
  comment_list_t *list = build_comment_list(10);

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);
  CU_ASSERT_NOT_EQUAL(comment_list_write(list, path), 0);

  comment_list_destroy(list);
}
