#include <CUnit/CUnit.h>

extern CU_TestInfo tests_comment[];

void test_comment_new(void);
void test_comment_destroy(void);
void test_comment_push(void);
void test_comment_process_write(void);
void test_comment_process_read(void);
void test_comment_process_generate(void);
void test_comment_process_none(void);
