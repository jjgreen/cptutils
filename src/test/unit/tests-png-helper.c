#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-png-helper.h"

#include <stdlib.h>


#define WIDTH 3
#define HEIGHT 4

png_t* build_png(void)
{
  png_t *png = png_new(WIDTH, HEIGHT);

  if (png == NULL)
    return NULL;

  unsigned char stops[WIDTH][4] = {
    {1, 2, 3, 255},
    {4, 5, 5, 200},
    {7, 8, 9, 100}
  };

  for (size_t i = 0 ; i < WIDTH ; i++)
    for (size_t j = 0 ; j < 4 ; j++)
      png->row[4 * i + j] = stops[i][j];

  return png;
}
