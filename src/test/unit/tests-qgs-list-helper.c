#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-qgs-list-helper.h"

#include <stdio.h>


qgs_list_t* build_qgs_list(size_t n)
{
  qgs_list_t *qgslist;

  if ((qgslist = qgs_list_new()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < n ; i++)
    {
      qgs_t *qgs;

      if ((qgs = qgs_list_next(qgslist)) == NULL)
	return NULL;

      char name[16];
      snprintf(name, 16, "id-%02zi", i);

      if (qgs_set_name(qgs, name) != 0)
        return NULL;
    }

  return qgslist;
}
