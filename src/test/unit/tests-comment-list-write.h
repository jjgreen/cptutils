#include <CUnit/CUnit.h>

extern CU_TestInfo tests_comment_list_write[];

void test_comment_list_write_single(void);
void test_comment_list_write_multiple(void);
void test_comment_list_write_nosuchdir(void);
