#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-read.h"
#include "fixture.h"

#include <cptutils/cpt-read.h>


CU_TestInfo tests_cpt_read[] = {
  {"fixtures", test_cpt_read_fixtures},
  {"range", test_cpt_read_range},
  {"hinge (explicit)", test_cpt_read_hinge_explicit},
  {"hinge (soft)", test_cpt_read_hinge_soft},
  {"hinge (hard)", test_cpt_read_hinge_hard},
  {"categorical", test_cpt_read_categorical},
  {"file does not exist", test_cpt_read_nofile},
  {"GMT5 HSV regression", test_cpt_read_hsv_regression},
  CU_TEST_INFO_NULL
};

void test_cpt_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "blue.cpt",
    "Exxon88.cpt",
    "GMT4-gebco.cpt",
    "GMT4-haxby.cpt",
    "GMT4-cyclic.cpt",
    "GMT5-cyclic.cpt",
    "GMT5-bathy.cpt",
    "GMT5-etopo1.cpt",
    "GMT6-mag.cpt",
    "GMT6-split.cpt",
    "labels.cpt",
    "Onion_Rings.cpt",
    "pakistan.cpt",
    "pakistan-solidus.cpt",
    "RdBu_10.cpt",
    "subtle.cpt",
    "test.cpt",
    "tpsfhm.cpt",
    "trailing-whitespace.cpt",
    "mixed-model-segment-hsv.cpt",
    "mixed-model-segment-rgb.cpt",
    "honeycomb-20.cpt",
    "leading-whitespace.cpt"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT(fixture(path, n, "cpt", files[i]) < n);
      cpt_t *cpt = cpt_read(path);
      CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
      cpt_destroy(cpt);
    }
}

void test_cpt_read_range(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "GMT5-etopo1.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT(cpt->range.present);
  CU_ASSERT_DOUBLE_EQUAL(cpt->range.min, -11000, 1e-5);
  CU_ASSERT_DOUBLE_EQUAL(cpt->range.max, 8500, 1e-5);
  cpt_destroy(cpt);
}

void test_cpt_read_hinge_explicit(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "GMT5-etopo1.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT(cpt->hinge.present);
  CU_ASSERT_EQUAL(cpt->hinge.type, hinge_explicit);
  CU_ASSERT_DOUBLE_EQUAL(cpt->hinge.value, -0.001, 1e-5);
  cpt_destroy(cpt);
}

void test_cpt_read_hinge_soft(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "GMT6-split.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT(cpt->hinge.present);
  CU_ASSERT_EQUAL(cpt->hinge.type, hinge_soft);
  CU_ASSERT_DOUBLE_EQUAL(cpt->hinge.value, 0, 1e-5);
  cpt_destroy(cpt);
}

void test_cpt_read_hinge_hard(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "GMT6-mag.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT(cpt->hinge.present);
  CU_ASSERT_EQUAL(cpt->hinge.type, hinge_hard);
  CU_ASSERT_DOUBLE_EQUAL(cpt->hinge.value, 0, 1e-5);
  cpt_destroy(cpt);
}

void test_cpt_read_categorical(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "categorical.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NULL(cpt);
}

void test_cpt_read_nofile(void)
{
  cpt_t *cpt = cpt_read("tmp/no/such/file");
  CU_ASSERT_PTR_NULL(cpt);
}

/*
  noticed working on something else, nonsense values when reading
  a GMT5 HSV file -- turned out to be that I was not setting the
  fill-type in the cpt parser in that case ...
*/

void test_cpt_read_hsv_regression(void)
{
  size_t n = 1024;
  char path[n];
  CU_ASSERT_FATAL(fixture(path, n, "cpt", "GMT5-cyclic.cpt") < n);
  cpt_t *cpt = cpt_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_EQUAL(cpt_nseg(cpt), 1);
  cpt_seg_t *seg = cpt_pop(cpt);
  CU_ASSERT_PTR_NOT_NULL_FATAL(seg);
  CU_ASSERT_EQUAL(seg->sample.left.fill.type, cpt_fill_colour_hsv);
  CU_ASSERT_EQUAL(seg->sample.right.fill.type, cpt_fill_colour_hsv);
  cpt_seg_destroy(seg);
  cpt_destroy(cpt);
}
