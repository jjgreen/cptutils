#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-ggr.h"
#include "fixture.h"
#include "fixture-ggr.h"

#include <cptutils/ggr.h>

#include <stdio.h>
#include <unistd.h>

CU_TestInfo tests_ggr[] = {
  {"colour at midpoint", test_ggr_colour_at_midpoint},
  {"colour outside range", test_ggr_colour_outside_range},
  {"colour null", test_ggr_colour_null},
  {"validity, valid fixture", test_ggr_valid_valid},
  {"validity, invalid fixture", test_ggr_valid_invalid},
  CU_TEST_INFO_NULL
};

void test_ggr_colour_at_midpoint(void)
{
  ggr_t *ggr = load_ggr_fixture("Sunrise.ggr");
  CU_ASSERT_PTR_NOT_NULL(ggr);

  double
    bg[3] = {0, 0, 0},
    rgbD[3],
    eps = 1e-6;

  CU_ASSERT_EQUAL(ggr_colour(0.5, ggr, bg, rgbD), 0);

  CU_ASSERT_DOUBLE_EQUAL(rgbD[0], 0.837332, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[1], 0.375108, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[2], 0.231127, eps);

  ggr_destroy(ggr);
}

void test_ggr_colour_outside_range(void)
{
  ggr_t *ggr = load_ggr_fixture("Sunrise.ggr");
  CU_ASSERT_PTR_NOT_NULL(ggr);

  double
    bg[3] = {0, 0, 0},
    rgbD[3],
    eps = 1e-8;

  CU_ASSERT_EQUAL(ggr_colour(-0.5, ggr, bg, rgbD), 0);

  CU_ASSERT_DOUBLE_EQUAL(rgbD[0], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[1], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[2], 1, eps);

  ggr_destroy(ggr);
}

void test_ggr_colour_null(void)
{
  double
    bg[3] = {0, 0.5, 1.0},
    rgbD[3],
    eps = 1e-8;

  CU_ASSERT_EQUAL(ggr_colour(0.5, NULL, bg, rgbD), 0);

  CU_ASSERT_DOUBLE_EQUAL(rgbD[0], 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[1], 0.5, eps);
  CU_ASSERT_DOUBLE_EQUAL(rgbD[2], 1.0, eps);
}

void test_ggr_valid_valid(void)
{
  ggr_t *ggr = load_ggr_fixture("Sunrise.ggr");
  CU_ASSERT_PTR_NOT_NULL(ggr);
  CU_ASSERT_EQUAL(ggr_valid(ggr), true);
  ggr_destroy(ggr);
}

void test_ggr_valid_invalid(void)
{
  ggr_t *ggr = load_ggr_fixture("afl-2023-10-25-00.ggr");
  CU_ASSERT_PTR_NOT_NULL(ggr);
  CU_ASSERT_EQUAL(ggr_valid(ggr), false);
  ggr_destroy(ggr);
}
