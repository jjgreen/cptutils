#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-pov.h"

#include <cptutils/pov.h>


CU_TestInfo tests_pov[] = {
  {"create", test_pov_create},
  {"set name (simple)", test_pov_set_name_simple},
  {"set name (complex)", test_pov_set_name_complex},
  {"allocate stops", test_pov_alloc_stops},
  CU_TEST_INFO_NULL,
};

void test_pov_create(void)
{
  pov_t *pov = pov_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(pov);
  CU_ASSERT_EQUAL(pov->n, 0);
  CU_ASSERT_PTR_NULL(pov->stop);
  CU_ASSERT_PTR_NOT_NULL_FATAL(pov->name);
  CU_ASSERT_EQUAL(pov->name[0], '\0');

  pov_destroy(pov);
}

void test_pov_set_name_simple(void)
{
  pov_t *pov = pov_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pov);

  size_t changed = 0;

  CU_ASSERT_EQUAL(pov_set_name(pov, "frooble", &changed), 0);
  CU_ASSERT_EQUAL(changed, 0);
  CU_ASSERT_STRING_EQUAL(pov->name, "frooble");

  pov_destroy(pov);
}

void test_pov_set_name_complex(void)
{
  pov_t *pov = pov_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pov);

  size_t changed = 0;

  CU_ASSERT_EQUAL(pov_set_name(pov, "bip bip b*p", &changed), 0);
  CU_ASSERT_EQUAL(changed, 3);
  CU_ASSERT_STRING_EQUAL(pov->name, "bip_bip_b_p");

  pov_destroy(pov);
}

void test_pov_alloc_stops(void)
{
  pov_t *pov = pov_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pov);

  CU_ASSERT_EQUAL(pov_stops_alloc(pov, 5), 0);
  CU_ASSERT_EQUAL(pov->n, 5);
  CU_ASSERT_PTR_NOT_NULL(pov->stop);

  pov_destroy(pov);
}
