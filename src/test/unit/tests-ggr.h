#include <CUnit/CUnit.h>

extern CU_TestInfo tests_ggr[];

void test_ggr_colour_at_midpoint(void);
void test_ggr_colour_outside_range(void);
void test_ggr_colour_null(void);
void test_ggr_valid_valid(void);
void test_ggr_valid_invalid(void);
