#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd5-helper.h"

#include <stdlib.h>
#include <string.h>


grd5_string_t* build_grd5_string(const char *content)
{
  grd5_string_t *str;

  if ((str = malloc(sizeof(grd5_string_t))) != NULL)
    {
      str->len = strlen(content);

      if ((str->content = malloc(str->len)) != NULL)
        {
          memcpy(str->content, content, str->len);
          return str;
        }

      free(str);
    }

  return NULL;
}
