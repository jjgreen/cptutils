#include <CUnit/CUnit.h>

extern CU_TestInfo tests_u32map[];

void test_u32map_new(void);
void test_u32map_put_absent(void);
void test_u32map_put_present(void);
void test_u32map_put_expand(void);
void test_u32map_get_empty(void);
void test_u32map_get_absent(void);
void test_u32map_get_present(void);
void test_u32map_update_absent(void);
void test_u32map_update_error(void);
void test_u32map_update_increment(void);
void test_u32map_size(void);
void test_u32map_collisions(void);
void test_u32map_capacity(void);
void test_u32map_each(void);
