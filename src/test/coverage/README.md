Coverage
--------

This is a subdirectory for coverage statistics, as extracted
from the code by `gcc --coverage`  and plotted by [gcovr][1].
The data is not committed to git, instead put into the `html`
subdirectory and git-ignored, since it is only really of
interest to developers.

To generate, at the bottom level of the repository, run

    make coverage-prepare
    make coverage-run

which runs `configure --enable-coverage` (setting `CFLAGS`),
builds the library, runs some of the test-suite, gathers the
coverage statistics and dumps the resulting report, finally
cleaning up.  Subsequently one can run

    make coverage-show

to launch your default browser to inspect the results.

The call to [gcovr][1] (producing readable HTML) is in the
Makefile in this directory; a similar command generates XML
for ingestion into GitLab, that can be found in the file
[.gitlab-ci.yml][2] in the root of the repository.

[1]: https://gcovr.com/en/stable/
[2]: ../../../.gitlab-ci.yml
