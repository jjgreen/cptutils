Assertion scripts
-----------------

A collection of scripts allowing us to make assertions about
cptutils program outputs, so things like

    equal-cpt generated.cpt expected.cpt

which succeeds (returns 0) if the two files are functionally
equal, fails (returns 1) otherwise.  On failure these write the
reason for failure to _stdout_.

We don't expect these to have any command-line options, and
they're not documented (outside this) so that we can knock
them out quickly.  They will be installed to `src/test/bin`
relative to the root of the archive, they are not installed
as part of the package.

The application is for acceptance testing, we have a "known
good" expected file, then acceptance tests can create a file
and check that it matches the expected output.
