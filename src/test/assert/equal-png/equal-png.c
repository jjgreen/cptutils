#include <png.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct
{
  uint32_t width, height;
  png_byte color_type;
  png_byte bit_depth;
  png_bytep *rows;
} pngeq_t;

static int pngeq_read_alloc(FILE *st, pngeq_t *p)
{
  png_structp
    reader = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                    NULL, NULL, NULL);
  if (reader == NULL)
    return 1;

  png_infop info = png_create_info_struct(reader);

  if (info == NULL)
    return 1;

  png_init_io(reader, st);
  png_read_info(reader, info);

  p->width = png_get_image_width(reader, info);
  p->height = png_get_image_height(reader, info);
  p->color_type = png_get_color_type(reader, info);
  p->bit_depth = png_get_bit_depth(reader, info);

  if (p->color_type == PNG_COLOR_TYPE_PALETTE)
    {
      png_set_palette_to_rgb(reader);

      if (p->bit_depth < 8)
        png_set_expand_gray_1_2_4_to_8(reader);
    }

  if (png_get_valid(reader, info, PNG_INFO_tRNS))
    png_set_tRNS_to_alpha(reader);

  if ((p->color_type == PNG_COLOR_TYPE_RGB) ||
      (p->color_type == PNG_COLOR_TYPE_GRAY) ||
      (p->color_type == PNG_COLOR_TYPE_PALETTE))
    png_set_filler(reader, 0xFF, PNG_FILLER_AFTER);

  if ((p->color_type == PNG_COLOR_TYPE_GRAY) ||
      (p->color_type == PNG_COLOR_TYPE_GRAY_ALPHA))
    png_set_gray_to_rgb(reader);

  png_read_update_info(reader, info);

  if ((p->rows = calloc(p->height, sizeof(png_bytep))) == NULL)
    return 1;

  for (size_t i = 0 ; i < p->height ; i++)
    {
      if ((p->rows[i] = malloc(png_get_rowbytes(reader, info))) == NULL)
        return 1;
    }

  png_read_image(reader, p->rows);
  png_destroy_read_struct(&reader, &info, NULL);

  return 0;
}

static pngeq_t* pngeq_read_stream(FILE *st)
{
  pngeq_t *p = NULL;

  if ((p = calloc(1, sizeof(pngeq_t))) != NULL)
    {
      if (pngeq_read_alloc(st, p) != 0)
        {
          free(p);
          p = NULL;
        }
    }

  return p;
}

static pngeq_t* pngeq_read(const char *path)
{
  FILE *st;
  pngeq_t *p = NULL;

  if ((st = fopen(path, "r")) != NULL)
    {
      p = pngeq_read_stream(st);
      fclose(st);
    }

  return p;
}

static void pngeq_destroy(pngeq_t *p)
{
  if (p != NULL)
    {
      if (p->rows != NULL)
        {
          for (size_t i = 0 ; i < p->height ; i++)
            free(p->rows[i]);
        }
      free(p->rows);
    }
  free(p);
}

static bool pngeq(const pngeq_t *p1, const pngeq_t *p2)
{
  if ((p1->width != p2->width) || (p1->height != p2->height))
    {
      printf("sizes: %u x %u, %u x %u\n",
             p1->width, p1->height,
             p2->width, p2->height);
      return false;
    }

  if (p1->color_type != p2->color_type)
    {
      printf("colour-types: %i, %i\n",
             p1->color_type,
             p2->color_type);
      return false;
    }

  if (p1->bit_depth != p2->bit_depth)
    {
      printf("bit-depths: %i, %i\n",
             p1->bit_depth,
             p2->bit_depth);
      return false;
    }

  for (size_t i = 0 ; i < p1->height ; i++)
    {
      for (size_t j = 0 ; j < p1->width ; j++)
        {
          if (p1->rows[i][j] != p2->rows[i][j])
            {
              printf("pixel (%zu, %zu)\n", i, j);
              return false;
            }
        }
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-png <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  pngeq_t *p1;
  bool equal = false;

  if ((p1 = pngeq_read(argv[1])) != NULL)
    {
      pngeq_t *p2;

      if ((p2 = pngeq_read(argv[2])) != NULL)
        {
          equal = pngeq(p1, p2);
          pngeq_destroy(p2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      pngeq_destroy(p1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
