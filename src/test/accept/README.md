Acceptance tests
----------------

These use [BATS][1] which is vendored in the `bats` subdirectory;
to update, see the script `bin/bats-update` in the root of this
repository.

There is a suite for each of the programs in the package in the
obviously named file.  The files `assert-*.bash` define functions
which make assertions about various file formats, typically their
functional equality.  We have a subdirectory for each program,
those contain the "expected output": the suite `cpthsv.bats` will
run **cpthsv** with some command-line options and then compare
the actual cpt output with a file in the `cpthsv` directory for
example.  The `assert-*.bash` scripts might call **diff**(1) in
the simplest case, or may use specialist C programs in the
neighbouring `assert` directory.

These tests are reasonably fast, around a minute, and are run in
GitLab CI.

[1]: https://github.com/bats-core
