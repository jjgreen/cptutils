#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-comments-valid'

setup()
{
    program='cptupdate'
    path="${src_dir}/${program}/${program}"
}

@test 'cptupdate, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptupdate, --help' {
    run "${path}" --help
}

@test 'cptupdate, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptupdate, too many input files' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" "${cpt1}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, input from stdin' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" < "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cptupdate, no comments source specified' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '0' ]
}

@test 'cptupdate, --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '1' ]
}

@test 'cptupdate, --comments-retain' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '7' ]
}

@test 'cptupdate, --comments-read (present)' {
    base='RdBu_10'
    comment="${fixture_dir}/xco/realistic.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '3' ]
}

@test 'cptupdate, --comments-read (absent)' {
    base='RdBu_10'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --comments-retain --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain --comments-generate \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --comments-write' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -6 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptupdate, --gmt4' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/version-gmt4.cpt'
}

@test 'cptupdate, --gmt5' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/version-gmt5.cpt'
}

@test 'cptupdate, --gmt6' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt6 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/version-gmt6.cpt'
}

@test 'cptupdate, --gmt4 --gmt5' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --gmt5 --hinge-hard' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        --hinge-hard \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --gmt5 --hinge-soft' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        --hinge-soft \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, GMT6 HARD_HINGE with --gmt6' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt6 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hard-hinge-gmt6.cpt'
}

@test 'cptupdate, GMT6 HARD_HINGE with --gmt5' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hard-hinge-gmt5.cpt'
}

@test 'cptupdate, GMT6 HARD_HINGE with --gmt4' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hard-hinge-gmt4.cpt'
}

@test 'cptupdate, GMT6 SOFT_HINGE with --gmt6' {
    base='GMT6-split'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt6 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/soft-hinge-gmt6.cpt'
}

@test 'cptupdate, GMT6 SOFT_HINGE with --gmt5' {
    base='GMT6-split'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/soft-hinge-gmt5.cpt'
}

@test 'cptupdate, GMT6 SOFT_HINGE with --gmt4' {
    base='GMT6-split'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/soft-hinge-gmt4.cpt'
}

@test 'cptupdate, GMT5 HINGE = 0 with --gmt6' {
    base='GMT5-earth'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt6 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hinge-zero-gmt6.cpt'
}

@test 'cptupdate, GMT5 HINGE = 0 with --gmt5' {
    base='GMT5-earth'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hinge-zero-gmt5.cpt'
}

@test 'cptupdate, GMT5 HINGE = 0 with --gmt4' {
    base='GMT5-earth'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hinge-zero-gmt4.cpt'
}

@test 'cptupdate, GMT5 HINGE != 0 with --gmt6' {
    base='GMT5-etopo1'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt6 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, GMT5 HINGE != 0 with --gmt5' {
    base='GMT5-etopo1'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt5 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hinge-nonzero-gmt5.cpt'
}

@test 'cptupdate, GMT5 HINGE != 0 with --gmt4' {
    base='GMT5-etopo1'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --hinge-hard' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-hard \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hard-hinge.cpt'
}

@test 'cptupdate, --hinge-hard, --z-normalise' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-hard \
        --z-normalise \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/hard-hinge-normal.cpt'
}

@test 'cptupdate, --hinge-hard, --gmt4' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-hard \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --hinge-soft' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-soft \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/soft-hinge.cpt'
}

@test 'cptupdate, --hinge-soft, --z-normalise' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-soft \
        --z-normalise \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/soft-hinge-normal.cpt'
}

@test 'cptupdate, --hinge-soft, --gmt4' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-soft \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --hinge-soft, --hinge-hard' {
    base='GMT4-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --hinge-soft \
        --hinge-hard \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --z-normalise' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --z-normalise \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/normal.cpt'
}

@test 'cptupdate, --z-normalise, --gmt4' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --z-normalise \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, --z-denormalise' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --z-denormalise \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/denormal.cpt'
}

@test 'cptupdate, --z-denormalise, --gmt4' {
    base='GMT6-globe'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --z-denormalise \
        --gmt4 \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/denormal-gmt4.cpt'
}

@test 'cptupdate, --z-normalise, --z-denormalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --z-normalise \
        --z-denormalise \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, mixed model --gmt4' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, mixed model --gmt4 --model rgb' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model rgb -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/mixed-model-v4-rgb.cpt'
}

@test 'cptupdate, mixed model --gmt4 --model hsv' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model hsv -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptupdate/mixed-model-v4-hsv.cpt'
}

@test 'cptupdate, mixed model --gmt4 --model nonesuch' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model nonesuch -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptupdate, output to stdout' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptupdate, output to stdout, --verbose' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cptupdate, backtrace, default format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptupdate, backtrace, plain format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptupdate, backtrace, XML format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptupdate, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptupdate, backtrace, unknown format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ ! -e "${backtrace}" ]
}
