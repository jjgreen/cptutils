#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'

setup()
{
    program='gplcpt'
    path="${src_dir}/${program}/${program}"
}

@test 'gplcpt, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'gplcpt, --help' {
    run "${path}" --help
}

@test 'gplcpt, --unknown' {

    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'gplcpt, no comments source' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '0' ]
}

@test 'gplcpt, --comments-generate' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '1' ]
}

@test 'gplcpt, --comments-read (present)' {
    base='Caramel'
    comment="${fixture_dir}/xco/realistic.xco"
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read  "${comment}" \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '3' ]
}

@test 'gplcpt, --comments-read (absent)' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-read  "${comment}" \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, multiple comment sources' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    echo 'some comment' > "${comment}"
    run "${path}" --comments-generate --comments-read "${comment}" \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, Caramel' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'gplcpt/default.cpt'
}

@test 'gplcpt, --z-normalise' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 --z-normalise -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'gplcpt/normal.cpt'
}

@test 'gplcpt, --gmt4' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -4 -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'gplcpt/version-gmt4.cpt'
}

@test 'gplcpt, --gmt5' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -5 -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'gplcpt/version-gmt5.cpt'
}

@test 'gplcpt, --gmt6' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -6 -v -o "${cpt}" "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'gplcpt/version-gmt6.cpt'
}

@test 'gplcpt, --gmt4 --gmt5' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -4 -5 -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, too many input files' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${gpl}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, input from stdin' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -o "${cpt}" < "${gpl}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'gplcpt, bad --background' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --background 2/3 -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, bad --foreground' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --foreground 2/3 -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, bad --nan' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --nan 2/3 -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'gplcpt, output to stdout' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    run "${path}" "${gpl}"
    [ "${status}" -eq '0' ]
}

@test 'gplcpt, output to stdout, --verbose' {
    base='Caramel'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    run "${path}" --verbose "${gpl}"
    [ "${status}" -ne '0' ]
}

@test 'gplcpt, backtrace, default format' {
    base='no-such-file'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'gplcpt, backtrace, plain format' {
    base='no-such-file'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'gplcpt, backtrace, XML format' {
    base='no-such-file'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'gplcpt, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'gplcpt, backtrace, unknown format' {
    base='no-such-file'
    gpl="${fixture_dir}/gpl/${base}.gpl"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${cpt}" "${gpl}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ ! -e "${backtrace}" ]
}
