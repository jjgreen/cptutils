#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-comments-valid'

setup()
{
    program='cptclip'
    path="${src_dir}/${program}/${program}"
}

@test 'cptclip, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptclip, --help' {
    run "${path}" --help
}

@test 'cptclip, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptclip, too many input files' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -R 2.5/5 -o "${cpt2}" "${cpt1}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, input from stdin' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -R 2.5/5 -o "${cpt2}" < "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cptclip, no comments source specified' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '0' ]
}

@test 'cptclip, --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '1' ]
}

@test 'cptclip, --comments-retain' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '7' ]
}

@test 'cptclip, --comments-read (present)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${fixture_dir}/xco/realistic.xco"
    run "${path}" --comments-read "${comment}" \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '3' ]
}

@test 'cptclip, --comments-read (absent)' {
    base='RdBu_10'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, --comments-retain --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain --comments-generate \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, --comments-write' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptclip, --gmt4' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/version-gmt4.cpt'
}

@test 'cptclip, --gmt5' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/version-gmt5.cpt'
}

@test 'cptclip, --gmt6' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/version-gmt6.cpt'
}

@test 'cptclip, --gmt4 --gmt5' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -5 -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, mixed model with --gmt4' {
    base='cptclip-mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -R 0/2 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, mixed model with --gmt4 --model rgb' {
    base='cptclip-mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -m rgb -R 0/2 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/mixed-model-v4-rgb.cpt'
}

@test 'cptclip, mixed model with --gmt4 --model hsv' {
    base='cptclip-mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -m hsv -R 0/2 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/mixed-model-v4-hsv.cpt'
}

@test 'cptclip, mixed model with --model nonesuch' {
    base='cptclip-mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -m nonesuch -R 0/2 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, normalised input' {
    base='GMT5-bathy'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/normal.cpt'
}

@test 'cptclip, normalised input, --z-normalise' {
    base='GMT5-bathy'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -z -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/normal-normal.cpt'
}

@test 'cptclip, normalised input, --z-denormalise' {
    base='GMT5-bathy'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -Z -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/normal-denormal.cpt'
}

@test 'cptclip, denormalised input' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/denormal.cpt'
}

@test 'cptclip, denormalised input, --z-normalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -z -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/denormal-normal.cpt'
}

@test 'cptclip, denormalised input, --z-denormalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -Z -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/denormal-denormal.cpt'
}

@test 'cptclip, --z-normalise and --z-denormalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -z -Z -R-4000/0 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, hinge excised from above' {
    base='hinge-hard-denorm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -R10/40 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/hinge-excised-above.cpt'
}

@test 'cptclip, hinge excised from below' {
    base='hinge-hard-denorm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 -R-40/-10 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/hinge-excised-below.cpt'
}

@test 'cptclip, z-range, RGB interpolate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -R 2.5/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/interpolate-rgb.cpt'
}

@test 'cptclip, z-range, HSV interpolate' {
    base='GMT5-cyclic'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -R 0.25/0.75 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/interpolate-hsv.cpt'
}

@test 'cptclip, absent --range' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, z-range, bad --range' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --range bad -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, z-range, reverse --range' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v --range 5/2.5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, --segment' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v --segment -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptclip/segment.cpt'
}

@test 'cptclip, --segment, bad --range' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v --segment --range hello -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, --segment, reverse --range' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v --segment --range 5/2 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptclip, output to stdout' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" -R 2.5/5 "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptclip, output to stdout, --verbose' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose -R 2.5/5 "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cptclip, backtrace, default format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -s -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptclip, backtrace, plain format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -s -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptclip, backtrace, XML format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -s -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptclip, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -s -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptclip, backtrace, unknown format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -s -R 2/5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ ! -e "${backtrace}" ]
}
