#!/usr/bin/env bats

load 'shared'
load 'assert-svg'
load 'assert-comments-valid'

setup()
{
    program='qgssvg'
    path="${src_dir}/${program}/${program}"
}

@test 'qgssvg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'qgssvg, --help' {
    run "${path}" --help
}

@test 'qgssvg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'qgssvg, no comments source specified' {
    base='elevation-comments'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'qgssvg, --comments-generate' {
    base='elevation-comments'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'qgssvg, --comments-retain' {
    base='elevation-comments'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'qgssvg, --comments-read (absent)' {
    base='elevation-comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'qgssvg, --comments-read (wrong size)' {
    base='elevation-comments'
    comment="${fixture_dir}/xco/single.xco"
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'qgssvg, --comments-read (present)' {
    base='elevation-comments'
    comment="${fixture_dir}/xco/${base}.xco"
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'qgssvg, --comments-retain --comments-generate' {
    base='elevation-comments'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain --comments-generate \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'qgssvg, --comments-write' {
    base='elevation-comments'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

testcase() {
    local -r base="$1"
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "qgssvg/${base}.svg"
}

@test 'qgssvg, fixture elevation' {
    testcase 'elevation'
}

@test 'qgssvg, fixture parula' {
    testcase 'parula'
}

@test 'qgssvg, fixture usgs-gswa2' {
    testcase 'usgs-gswa2'
}

@test 'qgssvg, --svg2' {
    base='parula'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --svg2 -o "${svg}" "${qgs}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'qgssvg, type preset (fails)' {
    base='vasarely'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'qgssvg, output to stdout' {
    base='elevation'
    qgs="${fixture_dir}/qgs/${base}.xml"
    run "${path}" "${qgs}"
    [ "${status}" -eq '0' ]
}

@test 'qgssvg, output to stdout, --verbose' {
    base='elevation'
    qgs="${fixture_dir}/qgs/${base}.xml"
    run "${path}" --verbose "${qgs}"
    [ "${status}" -ne '0' ]
}

@test 'qgssvg, backtrace, default format' {
    base='no-such-file'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'qgssvg, backtrace, plain format' {
    base='no-such-file'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'qgssvg, backtrace, XML format' {
    base='no-such-file'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'qgssvg, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'qgssvg, backtrace, unknown format' {
    skip_unless_with_json
    base='no-such-file'
    qgs="${fixture_dir}/qgs/${base}.xml"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.unknown"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${svg}" "${qgs}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ ! -e "${backtrace}" ]
}
