#!/usr/bin/env bats

load 'shared'
load 'assert-pg'
load 'assert-comments-valid'

setup()
{
    program='cptpg'
    path="${src_dir}/${program}/${program}"
}

@test 'cptpg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptpg, --help' {
    run "${path}" --help
}

@test 'cptpg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptpg, too many input files' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" "${cpt}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'cptpg, input from stdin' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" < "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
}

@test 'cptpg, --comments-write' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    comment="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" --comments-write "${comment}" \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptpg, default' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "cptpg/${base}-default.pg"
}

@test 'cptpg, percentage' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" --percentage "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "cptpg/${base}-percentage.pg"
}

@test 'cptpg, normalised, hard hinge' {
    base='GMT6-mag'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "cptpg/${base}.pg"
}

@test 'cptpg, normalised, soft hinge inactive' {
    base='hinge-soft-norm'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "cptpg/${base}-inactive.pg"
}

@test 'cptpg, normalised, soft hinge active' {
    base='hinge-soft-norm'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v --hinge-active -o "${pg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "cptpg/${base}-active.pg"
}

@test 'cptpg, input cpt has HSV interpolation' {
    base='GMT5-cyclic'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -o "${pg}" "${cpt}"
    # FIXME
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'cptpg, output to stdout' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptpg, output to stdout, --verbose' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cptpg, regression, non-monotone input creates empty file' {
    base='non-monotone'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" --verbose --output "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'cptpg, backtrace, default format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptpg, backtrace, plain format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptpg, backtrace, XML format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptpg, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptpg, backtrace, unknown format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${pg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
    [ ! -e "${backtrace}" ]
}
