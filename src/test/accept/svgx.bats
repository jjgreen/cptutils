#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-css3'
load 'assert-ggr'
load 'assert-gpt'
load 'assert-grd3'
load 'assert-inc'
load 'assert-map'
load 'assert-pg'
load 'assert-png'
load 'assert-qgs'
load 'assert-sao'
load 'assert-svg'
load 'assert-comments-valid'

setup()
{
    program='svgx'
    path="${src_dir}/${program}/${program}"
}

@test 'svgx, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'svgx, --help' {
    run "${path}" --help
}

@test 'svgx, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'svgx, --list' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    run "${path}" -l "${svg}"
    [ "${status}" -eq '0' ]
}

@test 'svgx, --list --all' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    run "${path}" -a -l "${svg}"
    [ "${status}" -ne '0' ]
}

# comments cpt

@test 'svgx, cpt, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        --select 'comments-01' \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/absent.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read (wrong size)' {
    base='comments'
    comment="${fixture_dir}/xco/multiple.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read (present)' {
    base='comments'
    comment="${fixture_dir}/xco/single.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read, --select (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/absent.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-02' -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read, --select (wrong size)' {
    base='comments'
    comment="${fixture_dir}/xco/multiple.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-02' -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read, --select (present)' {
    base='comments'
    comment="${fixture_dir}/xco/single.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-02' -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-read, --all (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt_dir="${BATS_TEST_TMPDIR}/cpt"
    mkdir "${cpt_dir}"
    run "${path}" --comments-read "${comment}" \
        --all -t cpt -v -o "${cpt_dir}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt_dir}/comments-01.cpt" ]
    [ ! -e "${cpt_dir}/comments-02.cpt" ]
    [ ! -e "${cpt_dir}/comments-03.cpt" ]
}

@test 'svgx, cpt, --comments-read, --all (wrong size)' {
    base='comments'
    comment="${fixture_dir}/xco/single.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt_dir="${BATS_TEST_TMPDIR}/cpt"
    mkdir "${cpt_dir}"
    run "${path}" --comments-read "${comment}" \
        --all -t cpt -v -o "${cpt_dir}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt_dir}/comments-01.cpt" ]
    [ ! -e "${cpt_dir}/comments-02.cpt" ]
    [ ! -e "${cpt_dir}/comments-03.cpt" ]
}

@test 'svgx, cpt, --comments-read, --all (present)' {
    base='comments'
    comment="${fixture_dir}/xco/multiple.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt_dir="${BATS_TEST_TMPDIR}/cpt"
    mkdir "${cpt_dir}"
    run "${path}" --comments-read "${comment}" \
        --all -t cpt -v -o "${cpt_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt_dir}/comments-01.cpt" ]
    [ -e "${cpt_dir}/comments-02.cpt" ]
    [ -e "${cpt_dir}/comments-03.cpt" ]
}

@test 'svgx, cpt, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, cpt, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t cpt -v -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, cpt, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt_dir="${BATS_TEST_TMPDIR}/cpt"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${cpt_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t cpt -v -o "${cpt_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt_dir}/comments-01.cpt" ]
    [ -e "${cpt_dir}/comments-02.cpt" ]
    [ -e "${cpt_dir}/comments-03.cpt" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments qgs

@test 'svgx, qgs, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" \
        --select 'comments-01' \
        -t qgs -v -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
}

@test 'svgx, qgs, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t qgs -v -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
}

@test 'svgx, qgs, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t qgs -v -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
}

@test 'svgx, qgs, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t qgs -v -o "${qgs}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${qgs}" ]
}

@test 'svgx, qgs, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${qgs}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${qgs}" ]
}

@test 'svgx, qgs, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t qgs -v -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, qgs, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs_dir="${BATS_TEST_TMPDIR}/qgs"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${qgs_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t qgs -v -o "${qgs_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs_dir}/comments-01.qgs" ]
    [ -e "${qgs_dir}/comments-02.qgs" ]
    [ -e "${qgs_dir}/comments-03.qgs" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments svg

@test 'svgx, svg, no comments source specified' {
    base='comments'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" \
        --select 'comments-01' \
        -t svg -v -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
}

@test 'svgx, svg, --comments-generate' {
    base='comments'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t svg -v -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
}

@test 'svgx, svg, --comments-retain' {
    base='comments'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t svg -v -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
}

@test 'svgx, svg, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t svg -v -o "${svgo}" "${svgi}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svgo}" ]
}

@test 'svgx, svg, --comments-retain --comments-generate' {
    base='comments'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${svgo}" "${svgi}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svgo}" ]
}

@test 'svgx, svg, --comments-write, --select' {
    base='comments'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t svg -v -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, svg, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    svg_dir="${BATS_TEST_TMPDIR}/svg"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${svg_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t svg -v -o "${svg_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${svg_dir}/comments-01.svg" ]
    [ -e "${svg_dir}/comments-02.svg" ]
    [ -e "${svg_dir}/comments-03.svg" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments tps

@test 'svgx, tps, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    run "${path}" \
        --select 'comments-01' \
        -t tps -v -o "${tps}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${tps}" ]
}

@test 'svgx, tps, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t tps -v -o "${tps}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${tps}" ]
}

@test 'svgx, tps, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t tps -v -o "${tps}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${tps}" ]
}

@test 'svgx, tps, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t tps -v -o "${tps}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${tps}" ]
}

@test 'svgx, tps, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${tps}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${tps}" ]
}

@test 'svgx, tps, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps="${BATS_TEST_TMPDIR}/${base}.tps"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t tps -v -o "${tps}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${tps}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, tps, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    tps_dir="${BATS_TEST_TMPDIR}/tps"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${tps_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t tps -v -o "${tps_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${tps_dir}/comments-01.map" ]
    [ -e "${tps_dir}/comments-02.map" ]
    [ -e "${tps_dir}/comments-03.map" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments pov

@test 'svgx, pov, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    run "${path}" \
        --select 'comments-01' \
        -t pov -v -o "${pov}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pov}" ]
}

@test 'svgx, pov, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t pov -v -o "${pov}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pov}" ]
}

@test 'svgx, pov, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t pov -v -o "${pov}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pov}" ]
}

@test 'svgx, pov, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t pov -v -o "${pov}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pov}" ]
}

@test 'svgx, pov, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${pov}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pov}" ]
}

@test 'svgx, pov, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov="${BATS_TEST_TMPDIR}/${base}.pov"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t pov -v -o "${pov}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pov}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, pov, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pov_dir="${BATS_TEST_TMPDIR}/pov"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${pov_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t pov -v -o "${pov_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pov_dir}/comments-01.inc" ]
    [ -e "${pov_dir}/comments-02.inc" ]
    [ -e "${pov_dir}/comments-03.inc" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments sao

@test 'svgx, sao, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" \
        --select 'comments-01' \
        -t sao -v -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
}

@test 'svgx, sao, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t sao -v -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
}

@test 'svgx, sao, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t sao -v -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
}

@test 'svgx, sao, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t sao -v -o "${sao}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${sao}" ]
}

@test 'svgx, sao, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${sao}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${sao}" ]
}

@test 'svgx, sao, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t sao -v -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, sao, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    sao_dir="${BATS_TEST_TMPDIR}/sao"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${sao_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t sao -v -o "${sao_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao_dir}/comments-01.sao" ]
    [ -e "${sao_dir}/comments-02.sao" ]
    [ -e "${sao_dir}/comments-03.sao" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments gpt

@test 'svgx, gpt, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" \
        --select 'comments-01' \
        -t gpt -v -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
}

@test 'svgx, gpt, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t gpt -v -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
}

@test 'svgx, gpt, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t gpt -v -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
}

@test 'svgx, gpt, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t gpt -v -o "${gpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${gpt}" ]
}

@test 'svgx, gpt, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${gpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${gpt}" ]
}

@test 'svgx, gpt, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    comment="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t gpt -v -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, gpt, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt_dir="${BATS_TEST_TMPDIR}/gpt"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${gpt_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t gpt -v -o "${gpt_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt_dir}/comments-01.gpt" ]
    [ -e "${gpt_dir}/comments-02.gpt" ]
    [ -e "${gpt_dir}/comments-03.gpt" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments css3

@test 'svgx, css3, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" \
        --select 'comments-01' \
        -t css3 -v -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
}

@test 'svgx, css3, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t css3 -v -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
}

@test 'svgx, css3, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t css3 -v -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
}

@test 'svgx, css3, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t css3 -v -o "${css3}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${css3}" ]
}

@test 'svgx, css3, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -v -o "${css3}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${css3}" ]
}

@test 'svgx, css3, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t css3 -v -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, css3, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    css3_dir="${BATS_TEST_TMPDIR}/css3"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${css3_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t css3 -v -o "${css3_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3_dir}/comments-01.c3g" ]
    [ -e "${css3_dir}/comments-02.c3g" ]
    [ -e "${css3_dir}/comments-03.c3g" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments ggr (errors)

@test 'svgx, ggr, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${ggr}" ]
}

@test 'svgx, ggr, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, ggr, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, ggr, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, ggr, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, ggr, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t ggr -v -o "${ggr}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${ggr}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, ggr, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr_dir="${BATS_TEST_TMPDIR}/ggr"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${ggr_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t ggr -v -o "${ggr_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${ggr_dir}/comments-01.ggr" ]
    [ -e "${ggr_dir}/comments-02.ggr" ]
    [ -e "${ggr_dir}/comments-03.ggr" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments pg (errors)

@test 'svgx, pg, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
}

@test 'svgx, pg, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'svgx, pg, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'svgx, pg, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'svgx, pg, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${pg}" ]
}

@test 'svgx, pg, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t pg -v -o "${pg}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, pg, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    pg_dir="${BATS_TEST_TMPDIR}/pg"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${pg_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t pg -v -o "${pg_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pg_dir}/comments-01.pg" ]
    [ -e "${pg_dir}/comments-02.pg" ]
    [ -e "${pg_dir}/comments-03.pg" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments jgd (errors)

@test 'svgx, jgd, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${jgd}" ]
}

@test 'svgx, jgd, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${jgd}" ]
}

@test 'svgx, jgd, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${jgd}" ]
}

@test 'svgx, jgd, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${jgd}" ]
}

@test 'svgx, jgd, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${jgd}" ]
}

@test 'svgx, jgd, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t jgd -v -o "${jgd}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${jgd}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, jgd, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd_dir="${BATS_TEST_TMPDIR}/jgd"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${jgd_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t jgd -v -o "${jgd_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${jgd_dir}/comments-01.grd" ]
    [ -e "${jgd_dir}/comments-02.grd" ]
    [ -e "${jgd_dir}/comments-03.grd" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

# comments png (errors)

@test 'svgx, png, no comments source specified' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png}" ]
}

@test 'svgx, png, --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --comments-generate \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, png, --comments-retain' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --comments-retain \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, png, --comments-read (absent)' {
    base='comments'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --comments-read "${comment}" \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, png, --comments-retain --comments-generate' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --comments-retain --comments-generate \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, png, --comments-write, --select' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        --select 'comments-01' \
        -t png -v -o "${png}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'svgx, png, --comments-write, --all' {
    base='comments'
    svg="${fixture_dir}/svg/${base}.svg"
    png_dir="${BATS_TEST_TMPDIR}/png"
    comment_dir="${BATS_TEST_TMPDIR}/comment"
    mkdir "${png_dir}" "${comment_dir}"
    run "${path}" --comments-write "${comment_dir}" \
        --all -t png -v -o "${png_dir}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png_dir}/comments-01.png" ]
    [ -e "${png_dir}/comments-02.png" ]
    [ -e "${png_dir}/comments-03.png" ]
    [ -e "${comment_dir}/comments-01.xco" ]
    [ -e "${comment_dir}/comments-02.xco" ]
    [ -e "${comment_dir}/comments-03.xco" ]
    assert_comments_valid "${comment_dir}/comments-01.xco"
    assert_comments_valid "${comment_dir}/comments-02.xco"
    assert_comments_valid "${comment_dir}/comments-03.xco"
}

testcase_cpt()  {
    local -r base="$1"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, style-space' {
    testcase_cpt 'style-space'
}

@test 'svgx, bad-colour' {
    testcase_cpt 'bad-colour'
}

@test 'svgx, rgb-percentage' {
    testcase_cpt 'rgb-percentage'
}

testcase_afl_2023_10_25()  {
    local -r n="$1"
    base="afl-2023-10-25-${n}"
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, AFL 2023/10/25 0' {
    testcase_afl_2023_10_25 '00'
}

@test 'svgx, AFL 2023/10/25 1' {
    testcase_afl_2023_10_25 '01'
}

testcase_type() {
    local -r type="$1"
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    result="${BATS_TEST_TMPDIR}/${base}.${type}"
    run "${path}" -v -t "${type}" -o "${result}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${result}" ]
}

@test 'svgx, --type cpt' {
    testcase_type 'cpt'
}

@test 'svgx, --type css3' {
    testcase_type 'css3'
}

@test 'svgx, --type ggr' {
    testcase_type 'ggr'
}

@test 'svgx, --type gnuplot' {
    testcase_type 'gnuplot'
}

@test 'svgx, --type map' {
    testcase_type 'map'
}

@test 'svgx, --type pg' {
    testcase_type 'pg'
}

@test 'svgx, --type png' {
    testcase_type 'png'
}

@test 'svgx, --type pov' {
    testcase_type 'pov'
}

@test 'svgx, --type psp' {
    testcase_type 'psp'
}

@test 'svgx, --type qgs' {
    testcase_type 'qgs'
}

@test 'svgx, --type sao' {
    testcase_type 'sao'
}

@test 'svgx, --type svg' {
    testcase_type 'svg'
}

@test 'svgx, --type unknown' {
    base='eyes'
    type='unknown'
    svg="${fixture_dir}/svg/${base}.svg"
    result="${BATS_TEST_TMPDIR}/${base}.${type}"
    run "${path}" -v -t "${type}" -o "${result}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${result}" ]
}

@test 'svgx, too many input files' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -o "${cpt}" "${svg}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, input from stdin (errors)' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -o "${cpt}" < "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, bad --background' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --background 2/3 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, bad --foreground' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --foreground 2/3 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, bad --nan' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --nan 2/3 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, --background for non-cpt output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --background 2/3/4 -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, --foreground for non-cpt output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --foreground 2/3/4 -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, --nan for non-cpt output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --nan 2/3/4 -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, --z-normalise for non-cpt output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --z-normalise -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, --svg2 for svg output' {
    base='eyes'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --svg2 -v -t svg -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
}

@test 'svgx, --svg2 for non-svg output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --svg2 -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, bad --transparency' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --transparency 2/3 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, --transparency for non alpha-capable output type' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" --transparency 2/3/4 -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ggr}" ]
}

@test 'svgx, --type css3, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" -v -t css3 -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
    assert_css3_equal "${css3}" "svgx/${base}.css3"
}

@test 'svgx, --type css3, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    css3="${BATS_TEST_TMPDIR}/${base}.css3"
    run "${path}" -v -t css3 -o "${css3}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${css3}" ]
    assert_css3_equal "${css3}" "svgx/${base}.css3"
}

@test 'svgx, --type ggr, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${ggr}" ]
    assert_ggr_equal "${ggr}" "svgx/${base}.ggr"
}

@test 'svgx, --type ggr, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    ggr="${BATS_TEST_TMPDIR}/${base}.ggr"
    run "${path}" -v -t ggr -o "${ggr}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${ggr}" ]
    assert_ggr_equal "${ggr}" "svgx/${base}.ggr"
}

@test 'svgx, --type gpt, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" -v -t gpt -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
    assert_gpt_equal "${gpt}" "svgx/${base}.gpt"
}

@test 'svgx, --type gpt, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    gpt="${BATS_TEST_TMPDIR}/${base}.gpt"
    run "${path}" -v -t gpt -o "${gpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${gpt}" ]
    assert_gpt_equal "${gpt}" "svgx/${base}.gpt"
}

@test 'svgx, --type inc, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    inc="${BATS_TEST_TMPDIR}/${base}.inc"
    run "${path}" -v -t inc -o "${inc}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${inc}" ]
    assert_inc_equal "${inc}" "svgx/${base}.inc"
}

@test 'svgx, --type inc, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    inc="${BATS_TEST_TMPDIR}/${base}.inc"
    run "${path}" -v -t inc -o "${inc}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${inc}" ]
    assert_inc_equal "${inc}" "svgx/${base}.inc"
}

@test 'svgx, --type jgd, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" -v -t jgd -o "${jgd}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${jgd}" ]
    assert_grd3_equal "${jgd}" "svgx/${base}.jgd"
}

@test 'svgx, --type jgd, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    jgd="${BATS_TEST_TMPDIR}/${base}.jgd"
    run "${path}" -v -t jgd -o "${jgd}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${jgd}" ]
    assert_grd3_equal "${jgd}" "svgx/${base}.jgd"
}

@test 'svgx, --type map, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    map="${BATS_TEST_TMPDIR}/${base}.map"
    run "${path}" -v -t map -o "${map}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${map}" ]
    assert_map_equal "${map}" "svgx/${base}.map"
}

@test 'svgx, --type map, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    map="${BATS_TEST_TMPDIR}/${base}.map"
    run "${path}" -v -t map -o "${map}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${map}" ]
    assert_map_equal "${map}" "svgx/${base}.map"
}

@test 'svgx, --type pg, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -t pg -o "${pg}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "svgx/${base}.pg"
}

@test 'svgx, --type pg, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    pg="${BATS_TEST_TMPDIR}/${base}.pg"
    run "${path}" -v -t pg -o "${pg}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${pg}" ]
    assert_pg_equal "${pg}" "svgx/${base}.pg"
}

@test 'svgx, --type png, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" -v -t png -o "${png}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png}" ]
    assert_png_equal "${png}" "svgx/${base}.png"
}

@test 'svgx, --type png, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" -v -t png -o "${png}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png}" ]
    assert_png_equal "${png}" "svgx/${base}.png"
}

@test 'svgx, --type qgs, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" -v -t qgs -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
    assert_qgs_equal "${qgs}" "svgx/${base}.qgs"
}

@test 'svgx, --type qgs, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    qgs="${BATS_TEST_TMPDIR}/${base}.qgs"
    run "${path}" -v -t qgs -o "${qgs}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${qgs}" ]
    assert_qgs_equal "${qgs}" "svgx/${base}.qgs"
}

@test 'svgx, --type sao, lemon-lime' {
    base='lemon-lime'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" -v -t sao -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
    assert_sao_equal "${sao}" "svgx/${base}.sao"
}

@test 'svgx, --type sao, french-flag' {
    base='french-flag'
    svg="${fixture_dir}/svg/${base}.svg"
    sao="${BATS_TEST_TMPDIR}/${base}.sao"
    run "${path}" -v -t sao -o "${sao}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${sao}" ]
    assert_sao_equal "${sao}" "svgx/${base}.sao"
}

@test 'svgx, --type svg, subtle (SVG 1.1)' {
    base='subtle'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -t svg -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
    assert_svg_equal "${svgo}" "svgx/${base}.svg"
}

@test 'svgx, --type svg, subtle-svg2 (SVG 2.0)' {
    base='subtle-svg2'
    svgi="${fixture_dir}/svg/${base}.svg"
    svgo="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -t svg -o "${svgo}" "${svgi}"
    [ "${status}" -eq '0' ]
    [ -e "${svgo}" ]
    assert_svg_equal "${svgo}" "svgx/${base}.svg"
}

@test 'svgx, --type cpt --gmt4' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -4 -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'svgx/version-gmt4.cpt'
}

@test 'svgx, --type cpt --gmt5' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -5 -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'svgx/version-gmt5.cpt'
}

@test 'svgx, --type cpt --gmt6' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -6 -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'svgx/version-gmt6.cpt'
}

@test 'svgx, --type cpt --gmt4 --gmt5' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -4 -5 -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, --type cpt --z-normalise' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -t cpt -6 --z-normalise -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'svgx/normal.cpt'
}

@test 'svgx, --type png --gmt4' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" -v -t png -4 -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, --type png --gmt6' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" -v -t png -6 -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, --type png --geometry valid' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --geometry 100x100 -v -t png -o "${png}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${png}" ]
}

@test 'svgx, --type png --geometry invalid' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    png="${BATS_TEST_TMPDIR}/${base}.png"
    run "${path}" --geometry x -v -t png -o "${png}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${png}" ]
}

@test 'svgx, --type svg --preview --geometry valid' {
    base='eyes'
    svg1="${fixture_dir}/svg/${base}.svg"
    svg2="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --preview --geometry 100x100 -v -t svg -o "${svg2}" "${svg1}"
    [ "${status}" -eq '0' ]
    [ -e "${svg2}" ]
    assert_svg_equal "${svg2}" "svgx/${base}.svg"
}

@test 'svgx, --type svg --preview --geometry invalid' {
    base='eyes'
    svg1="${fixture_dir}/svg/${base}.svg"
    svg2="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --preview --geometry x -v -t svg -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg2}" ]
}

@test 'svgx, --type cpt --geometry given' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --geometry 100x100 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'svgx, --type map --strict given' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    map="${BATS_TEST_TMPDIR}/${base}.map"
    run "${path}" --strict -v -t map -o "${map}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${map}" ]
}

@test 'svgx, --type inc --strict given' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    inc="${BATS_TEST_TMPDIR}/${base}.inc"
    run "${path}" --strict -v -t inc -o "${inc}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${inc}" ]
}

@test 'svgx, --select' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --select linearGradient1535 -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'svgx/select.cpt'
}

@test 'svgx, --select (with id)' {
    base='subtle'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --select id-subtle -v -t cpt -o "${cpt}" "${svg}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'svgx, --all' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    run "${path}" --all -v -t cpt -o "${BATS_TEST_TMPDIR}" "${svg}"
    [ "${status}" -eq '0' ]
    for n in 1612 1608 1546 1535 1530
    do
        cpt="${BATS_TEST_TMPDIR}/linearGradient${n}.cpt"
        [ -e "${cpt}" ]
        assert_cpt_equal "${cpt}" "svgx/all-${n}.cpt"
    done
}

@test 'svgx, --verbose without --output' {
    base='eyes'
    svg="${fixture_dir}/svg/${base}.svg"
    run "${path}" --verbose -t cpt "${svg}"
    [ "${status}" -ne '0' ]
}

@test 'svgx, backtrace, default format' {
    base='no-such-file'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -t cpt -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'svgx, backtrace, plain format' {
    base='no-such-file'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -t cpt -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'svgx, backtrace, XML format' {
    base='no-such-file'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -t cpt -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'svgx, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -t cpt -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'svgx, backtrace, unknown format' {
    base='no-such-file'
    svg="${fixture_dir}/svg/${base}.svg"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -t cpt -o "${svg2}" "${svg1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ ! -e "${backtrace}" ]
}
