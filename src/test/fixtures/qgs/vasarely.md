This is the "Varasely color ramp" by _Romaine_ kindly released
under a CC0 licence. Downloaded from [QGis plugins][1].  This
is interesting since a "preset" ramp, qgssvg fails to convert
this file.

[1]: https://plugins.qgis.org/styles/57/
