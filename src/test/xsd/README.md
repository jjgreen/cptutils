XML schemae
-----------

For validation of XML files, a 3rd party tool is required for these:
for example with **xmllint**
```
xmllint \
    --noout \
    --schema src/test/xsd/comments.xsd \
    src/test/fixtures/xco/single.xco
```
or with **xmlstarlet**
```
xmlstarlet val \
    --err \
    --xsd src/test/xsd/comments.xsd \
    src/test/fixtures/xco/realistic.xco
```

The file `metadata.xsd` is for the content of the SVG metadata section,
it is unused as yet.
