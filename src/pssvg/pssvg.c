#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ucs2utf8.h"
#include "pssvg.h"

#include <cptutils/grd5-read.h>
#include <cptutils/grd5.h>
#include <cptutils/svg-list-write.h>
#include <cptutils/svg.h>
#include <cptutils/comment-list.h>
#include <cptutils/comment-list-process.h>
#include <cptutils/gstack.h>
#include <cptutils/grdx-svg.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <inttypes.h>

/* convert psp to intermediate types */

static double grd5_rgb_it(double x)
{
  return round(x) / 256.0;
}

static double grd5_op_it(uint32_t x)
{
  return x / 100.0;
}

static unsigned int grd5_z_it(uint32_t z)
{
  return (unsigned int)z*100;
}

static unsigned int grd5_zmid_it(uint32_t z0,
				 uint32_t z1,
				 uint32_t M)
{
  return (unsigned int)z0*100 + ((unsigned int)z1 - (unsigned int)z0) * M;
}


/* trim off excessive stops */

static int trim_rgb(gstack_t *stack)
{
  size_t n = gstack_size(stack);
  rgb_stop_t stop;
  gstack_t *stack0;

  if ((stack0 = gstack_new(sizeof(rgb_stop_t), n, n)) == NULL)
    return 1;

  while (! gstack_empty(stack))
    {
      gstack_pop(stack, &stop);
      gstack_push(stack0, &stop);

      if (stop.z >= 409600)
	{
	  while (! gstack_empty(stack))
	    gstack_pop(stack, &stop);
	}
    }

  while (! gstack_empty(stack0))
    {
      gstack_pop(stack0, &stop);
      gstack_push(stack, &stop);
    }

  gstack_destroy(stack0);

  return 0;
}

static int trim_op(gstack_t *stack)
{
  size_t n = gstack_size(stack);
  op_stop_t stop;
  gstack_t *stack0;

  if ((stack0 = gstack_new(sizeof(op_stop_t), n, n)) == NULL)
    return 1;

  while (! gstack_empty(stack))
    {
      gstack_pop(stack, &stop);
      gstack_push(stack0, &stop);

      if (stop.z >= 409600)
	{
	  while (! gstack_empty(stack))
	    gstack_pop(stack,&stop);
	}
    }

  while (! gstack_empty(stack0))
    {
      gstack_pop(stack0,&stop);
      gstack_push(stack,&stop);
    }

  gstack_destroy(stack0);

  return 0;
}

/*
  map a value in [0,1] to a double which is an integer
  in 0 .. 255
*/

static double clamp_channel(double dval)
{
  int ival = floor(dval * 256);

  if (ival > 255)
    ival = 255;
  else if (ival < 0)
    ival = 0;

  return ival;
}

/*
  The following functions modify their argument, converting
  them to RGB colour space

  These do not, in general, give the same values as PS since
  we are doing a naive and ham-fisted conversion not taking
  into account colour profiles as PS does.  We could probably
  do better if we used the open-source and widely available
  Little CMS library (http://www.littlecms.com/) ...
*/

static int grsc_to_rgb(grd5_colour_stop_t *stop)
{
  double val = stop->grsc.Gry;

  stop->rgb.Rd = val;
  stop->rgb.Grn = val;
  stop->rgb.Bl = val;

  stop->type = GRD5_MODEL_RGB;

  return 0;
}

static int hsb_to_rgb(grd5_colour_stop_t *stop)
{
  /* use the GIMP hsvD_to_rgbD (cptutils/colour.h) function */

  double hsv[3];

  hsv[0] = stop->hsb.H / 360.0;
  hsv[1] = stop->hsb.Strt / 100.0;
  hsv[2] = stop->hsb.Brgh / 100.0;

  double rgb[3];

  hsvD_to_rgbD(hsv, rgb);

  stop->rgb.Rd = clamp_channel(rgb[0]);
  stop->rgb.Grn = clamp_channel(rgb[1]);
  stop->rgb.Bl = clamp_channel(rgb[2]);

  stop->type = GRD5_MODEL_RGB;

  return 0;
}

static int cmyc_to_rgb(grd5_colour_stop_t *stop)
{
  /* Naive implementation */

  double
    c = stop->cmyc.Cyn / 100.0,
    m = stop->cmyc.Mgnt / 100.0,
    y = stop->cmyc.Ylw / 100.0,
    k = stop->cmyc.Blck / 100.0;

  stop->rgb.Rd = clamp_channel((1 - c) * (1 - k));
  stop->rgb.Grn = clamp_channel((1 - m) * (1 - k));
  stop->rgb.Bl  = clamp_channel((1 - y) * (1 - k));

  stop->type = GRD5_MODEL_RGB;

  return 0;
}

static double lab_xyz_curve(double W)
{
  double W3 = pow(W, 3);

  if (W3 > 0.008856)
    return W3;

  return (W - 16.0 / 116.0) / 7.787;
}

static double xyz_rgb_curve(double W)
{
  if (W > 0.0031308)
    return 1.055 * pow(W, 1 / 2.4) - 0.055;

  return 12.92 * W;
}

#define REF_X  95.047
#define REF_Y 100.000
#define REF_Z 108.883

static int lab_to_rgb(grd5_colour_stop_t *stop)
{
  /* from easyrgb.com : CIE-L*ab -> XYZ */

  double
    L = stop->lab.Lmnc,
    A = stop->lab.A,
    B = stop->lab.B;

  double
    vY = (L+16.0) / 116.0,
    vX = A / 500.0 + vY,
    vZ = vY - B / 200.0;

  vX = lab_xyz_curve(vX);
  vY = lab_xyz_curve(vY);
  vZ = lab_xyz_curve(vZ);

  double
    X = REF_X * vX,
    Y = REF_Y * vY,
    Z = REF_Z * vZ;

  /* from easyrgb.com : XYZ -> RGB */

  vX = X / 100.0;
  vY = Y / 100.0;
  vZ = Z / 100.0;

  double
    vR =  3.2406 * vX - 1.5372 * vY - 0.4986 * vZ,
    vG = -0.9689 * vX + 1.8758 * vY + 0.0415 * vZ,
    vB =  0.0557 * vX - 0.2040 * vY + 1.0570 * vZ;

  vR = xyz_rgb_curve(vR);
  vG = xyz_rgb_curve(vG);
  vB = xyz_rgb_curve(vB);

  stop->rgb.Rd = clamp_channel(vR);
  stop->rgb.Grn = clamp_channel(vG);
  stop->rgb.Bl = clamp_channel(vB);

  stop->type = GRD5_MODEL_RGB;

  return 0;
}

/*
  convert the svg stops to the intermediate types, and rectify:
  replace the midpoints by explicit mid-point stops
*/

static int rectify_rgb2(grd5_grad_custom_t *gradc,
                        gstack_t *stack,
                        const pssvg_opt_t *opt)
{
  grd5_colour_stop_t *grd5_stop = gradc->colour.stops;
  int n = gradc->colour.n;
  rgb_stop_t stop;

  if (grd5_stop[0].Lctn > 0)
    {
      stop.z  = 0;
      stop.r = grd5_rgb_it(grd5_stop[0].rgb.Rd);
      stop.g = grd5_rgb_it(grd5_stop[0].rgb.Grn);
      stop.b = grd5_rgb_it(grd5_stop[0].rgb.Bl);

      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  for (size_t i = 0 ; i < n - 1 ; i++)
    {
      stop.z = grd5_z_it(grd5_stop[i].Lctn);
      stop.r = grd5_rgb_it(grd5_stop[i].rgb.Rd);
      stop.g = grd5_rgb_it(grd5_stop[i].rgb.Grn);
      stop.b = grd5_rgb_it(grd5_stop[i].rgb.Bl);

      if (gstack_push(stack, &stop) != 0)
	return 1;

      if (grd5_stop[i].Mdpn != 50)
	{
	  stop.z = grd5_zmid_it(grd5_stop[i].Lctn,
				grd5_stop[i + 1].Lctn,
				grd5_stop[i].Mdpn);
	  stop.r = 0.5 * (grd5_rgb_it(grd5_stop[i].rgb.Rd) +
                          grd5_rgb_it(grd5_stop[i + 1].rgb.Rd));
	  stop.g = 0.5 * (grd5_rgb_it(grd5_stop[i].rgb.Grn) +
                          grd5_rgb_it(grd5_stop[i + 1].rgb.Grn));
	  stop.b = 0.5 * (grd5_rgb_it(grd5_stop[i].rgb.Bl) +
                          grd5_rgb_it(grd5_stop[i + 1].rgb.Bl));

	  if (gstack_push(stack, &stop) != 0)
	    return 1;
	}
    }

  stop.z = grd5_z_it(grd5_stop[n - 1].Lctn);
  stop.r = grd5_rgb_it(grd5_stop[n - 1].rgb.Rd);
  stop.g = grd5_rgb_it(grd5_stop[n - 1].rgb.Grn);
  stop.b = grd5_rgb_it(grd5_stop[n - 1].rgb.Bl);

  if (gstack_push(stack, &stop) != 0)
    return 1;

  /* add implicit final stop */

  if (stop.z < 409600)
    {
      stop.z = 409600;

      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  if (gstack_reverse(stack) != 0)
    return 1;

  if (trim_rgb(stack) != 0)
    return 1;

  return 0;
}

static gstack_t* rectify_rgb(grd5_grad_custom_t *gradc,
                             const pssvg_opt_t *opt)
{
  grd5_colour_stop_t *grd5_stop = gradc->colour.stops;
  size_t n = gradc->colour.n;

  if (n < 2)
    {
      btrace("input (grd5) has %i rgb stop(s)", n);
      return NULL;
    }

  for (size_t i = 0 ; i < n ; i++)
    {
      grd5_colour_stop_t *stop = grd5_stop + i;

      switch(stop->type)
	{
	case GRD5_MODEL_RGB:
	  break;

	case GRD5_MODEL_GRSC:
	  grsc_to_rgb(stop);
	  break;

	case GRD5_MODEL_HSB:
	  hsb_to_rgb(stop);
	  break;

	case GRD5_MODEL_CMYC:
	  cmyc_to_rgb(stop);
	  break;

	case GRD5_MODEL_LAB:
	  lab_to_rgb(stop);
	  break;

	case GRD5_MODEL_BCKC:
	  stop->rgb.Rd = opt->bg.red;
	  stop->rgb.Grn = opt->bg.green;
	  stop->rgb.Bl = opt->bg.blue;
	  stop->type = GRD5_MODEL_RGB;
	  break;

	case GRD5_MODEL_FRGC:
	  stop->rgb.Rd = opt->fg.red;
	  stop->rgb.Grn = opt->fg.green;
	  stop->rgb.Bl = opt->fg.blue;
	  stop->type = GRD5_MODEL_RGB;
	  break;

	case GRD5_MODEL_BOOK:
	  btrace("stop %i (book colour) not converted", i);
	  return NULL;

	default:
	  btrace("stop %i unknown colour type %i", i, stop->type);
	  return NULL;
	}

      if (stop->type != GRD5_MODEL_RGB)
	{
	  btrace("stop %i is non-RGB (type %i)", i, stop->type);
	  return NULL;
	}
    }

  gstack_t *stack = gstack_new(sizeof(rgb_stop_t), 2 * n, n);

  if (stack != NULL)
    {
      if (rectify_rgb2(gradc, stack, opt) == 0)
	return stack;

      gstack_destroy(stack);
    }

  return NULL;
}

static int rectify_op2(grd5_grad_custom_t *gradc, gstack_t *stack)
{
  grd5_transp_stop_t *grd5_stop = gradc->transp.stops;
  size_t n = gradc->transp.n;
  op_stop_t stop;

  if (grd5_stop[0].Lctn > 0)
    {
      stop.z = 0;
      stop.op = grd5_op_it(grd5_stop[0].Opct);

      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  for (size_t i = 0 ; i < n - 1 ; i++)
    {
      stop.z = grd5_z_it(grd5_stop[i].Lctn);
      stop.op = grd5_op_it(grd5_stop[i].Opct);

      if (gstack_push(stack, &stop) != 0)
	return 1;

      if (grd5_stop[i].Mdpn != 50)
	{
	  stop.z = grd5_zmid_it(grd5_stop[i].Lctn,
                                grd5_stop[i + 1].Lctn,
                                grd5_stop[i].Mdpn);
	  stop.op = 0.5 * (grd5_op_it(grd5_stop[i].Opct) +
                           grd5_op_it(grd5_stop[i + 1].Opct));

	  if (gstack_push(stack, &stop) != 0)
	    return 1;
	}
    }

  stop.z = grd5_z_it(grd5_stop[n - 1].Lctn);
  stop.op = grd5_op_it(grd5_stop[n - 1].Opct);

  if (gstack_push(stack, &stop) != 0)
    return 1;

  if (stop.z < 409600)
    {
      stop.z = 409600;
      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  if (gstack_reverse(stack) != 0)
    return 1;

  if (trim_op(stack) != 0)
    return 1;

  return 0;
}

static gstack_t* rectify_op(grd5_grad_custom_t *gradc)
{
  size_t n = gradc->transp.n;

  if (n < 2)
    btrace("input (grd5) has %i opacity stop(s)", n);
  else
    {
      gstack_t *stack = gstack_new(sizeof(op_stop_t), 2 * n, n);

      if (stack != NULL)
	{
	  if (rectify_op2(gradc, stack) == 0)
	    return stack;

	  gstack_destroy(stack);
	}
    }

  return NULL;
}

static int pssvg_title(grd5_grad_t *grd5_grad,
		       svg_t *svg,
		       const pssvg_opt_t *opt,
		       int gradnum)
{
  size_t len;

  if (opt->id_format)
    len = snprintf((char*)(svg->id), SVG_NAME_LEN, opt->id_format, gradnum);
  else
    {
      grd5_string_t *ucs2_title_string = grd5_grad->title;
      size_t ucs2_title_len = ucs2_title_string->len;
      char *ucs2_title = ucs2_title_string->content;

      /*
	One would kind-of-expect that a UTF-8 string would use no
	more space than a UCS-2, but the former uses 1-4 bytes, the
	latter 2-4, so it is conceivable that there are 2-byte UCS-2
	codepoints which map to 4-byte UTF-8; at worst we might have
	a factor of two.  I have seen a real case where the factor
	seems to be strictly greater than one (iconv returns the
	error "Argument list too long" when utf8_title_len is equal
	to ucs2_title_len), this on Japanese characters, but gives
	no error for a factor 2.
      */

      size_t utf8_title_len = 2 * ucs2_title_len;
      char utf8_title[ucs2_title_len];

      if (ucs2_to_utf8(ucs2_title,
		       ucs2_title_len,
		       utf8_title,
		       utf8_title_len) != 0)
	{
	  btrace("failed ucs2 to utf8 conversion");
	  return 1;
	}

      len =
        snprintf((char*)svg->name,
                 SVG_NAME_LEN,
                 "%s",
                 (char*)utf8_title);
    }

  if (len >= SVG_NAME_LEN)
    {
      btrace("title truncated");
      return 1;
    }

  return 0;
}

static int pssvg_convert_one(grd5_grad_custom_t *grd5_gradc,
			     svg_t *svg,
			     const pssvg_opt_t *opt)
{
  gstack_t *rgbrec;
  int err = 1;

  if ((rgbrec = rectify_rgb(grd5_gradc, opt)) == NULL)
    btrace("failed rectify RGB");
  else
    {
      gstack_t *oprec;

      if ((oprec = rectify_op(grd5_gradc)) == NULL)
        btrace("failed rectify opacity");
      else
	{
	  err = grdxsvg(rgbrec, oprec, svg);
	  gstack_destroy(oprec);

          if (err == 0)
            err = svg_complete(svg);
	}

      gstack_destroy(rgbrec);
    }

  if (err)
    btrace("failed conversion of rectified stops to svg");
  else if (opt->verbose)
    printf("  '%s', %i%% smooth; %u colour, %u opacity converted to %i RGBA\n",
	   svg->name,
	   (int)round(grd5_gradc->interp / 40.96),
	   grd5_gradc->colour.n,
	   grd5_gradc->transp.n,
	   svg_num_stops(svg));

  return err;
}

/*
  here we do not error if we fail to convert a gradient, only if
  something goes profoundly wrong -- there may be other gradients
  in the list which can be converted ...
*/

static int pssvg_convert_ith(grd5_t *grd5, size_t i,
			     svg_list_t *list,
			     const pssvg_opt_t *opt)
{
  grd5_grad_t *grd5_grad = grd5->gradients + i;
  svg_t *svg;
  int err = 1;

  switch (grd5_grad->type)
    {
    case GRD5_GRAD_CUSTOM:

      if ((svg = svg_list_next(list)) != NULL)
	{
	  if (pssvg_title(grd5_grad, svg, opt, i + 1) == 0)
	    {
	      if (pssvg_convert_one(&(grd5_grad->custom), svg, opt) == 0)
                err = 0;
              else
		{
		  btrace("failed convert of gradient %zi", i);

                  if (svg_list_revert(list) == 0)
                    err = 0;
                  else
                    btrace("failed SVG list revert");
		}
	    }
          else
            btrace("failed svg title");
	}
      else
        btrace("failed svg list next");

      break;

    case GRD5_GRAD_NOISE:

      btrace("no conversion of (noise) gradient %zi", i);
      err = 0;
      break;

    default:

      btrace("bad type (%i) for gradient %zi", grd5_grad->type, i);
      err = 0;
    }

  return err;
}

static int pssvg_convert(grd5_t *grd5,
                         svg_list_t *list,
                         const pssvg_opt_t *opt)
{
  size_t n = grd5->n;

  for (size_t i = 0 ; i < n ; i++)
    {
      if (pssvg_convert_ith(grd5, i, list, opt) != 0)
	return 1;
    }

  if (svg_list_coerce_ids(list) != 0)
    {
      btrace("failed to coerce ids to uniqueness");
      return 1;
    }

  return 0;
}

typedef struct
{
  size_t n;
  const comment_list_t *comment_list;
} set_comment_t;

static int f_set_comment(svg_t *svg, void *varg)
{
  set_comment_t *arg = varg;
  const comment_t *comment;
  int err = 1;

  if ((comment = comment_list_entry(arg->comment_list, arg->n)) != NULL)
    {
      arg->n++;
      comment_deinit(svg->comment);
      if (comment_copy(comment, svg->comment) == 0)
        err = 0;
    }

  return err;
}

static int svg_list_set_comment(svg_list_t *svg_list,
                                 const comment_list_t* comment_list)
{
  set_comment_t arg = { .n = 0, .comment_list = comment_list };
  return svg_list_each(svg_list, f_set_comment, &arg);
}

static int svg_list_comment(svg_list_t *svg_list, const pssvg_opt_t *opt)
{
  comment_list_t *comment_list;
  int err = 1;

  if ((comment_list = comment_list_new()) == NULL)
    btrace("failed to create comment list");
  else
    {
      size_t n = svg_list_size(svg_list);

      for (size_t i = 0 ; i < n ; i++)
        comment_list_next(comment_list);

      if (comment_list_process(comment_list, &(opt->comment)) != 0)
        btrace("failed to process comments");
      else
        {
          if (svg_list_set_comment(svg_list, comment_list) != 0)
            btrace("failed to transfer comments to SVG");
          else
            err = 0;
        }

      comment_list_destroy(comment_list);
    }

  return err;
}

int pssvg(const pssvg_opt_t *opt)
{
  int err = 1;
  grd5_t *grd5;

  if ((grd5 = grd5_read(opt->input.path)) == NULL)
    btrace("failed to read %s", IFNULL(opt->input.path, "<stdin>"));
  else
    {
      if (grd5->n == 0)
        btrace("no gradients parsed");
      else
        {
          if (opt->verbose)
            printf("parsed %u grd5 gradient%s\n", grd5->n, PLURAL(grd5->n));

          svg_list_t *list;

          if ((list = svg_list_new()) == NULL)
            btrace("new svg list");
          else
            {
              if (pssvg_convert(grd5, list, opt) != 0)
                btrace("conversion failed");
              else
                {
                  if (svg_list_size(list) == 0)
                    btrace("no svg gradients converted");
                  else
                    {
                      if (svg_list_comment(list, opt) != 0)
                        btrace("failed comments");
                      else
                        {
                          svg_preview_t preview = { .use = false };
                          const char *path = opt->output.path;
                          svg_version_t version = opt->svg_version;

                          if (svg_list_write(list, path, &preview, version) != 0)
                            btrace("failed write of svg");
                          else
                            err = 0;
                        }
                    }
                }

              svg_list_destroy(list);
            }
        }

      grd5_destroy(grd5);
    }

  return err;
}
