/* autogenerated: do not edit */

#ifndef HELPER_SVG2_H
#define HELPER_SVG2_H

#include "pssvg.h"
#include "options.h"

int helper_svg2(const struct gengetopt_args_info*, pssvg_opt_t*);

#endif
