#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/sao.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct sao_stop_t sao_stop_t;

struct sao_stop_t
{
  double x, v;
  sao_stop_t *next;
};

struct sao_t
{
  sao_stop_t *red, *green, *blue;
  comment_t *comment;
};

static sao_stop_t* sao_stop_new(double x, double v)
{
  sao_stop_t *stop;

  if ( (stop = malloc(sizeof(sao_stop_t))) == NULL)
    return NULL;

  stop->x = x, stop->v = v;
  stop->next = NULL;

  return stop;
}

static int sao_channel_append(sao_stop_t **base, double x, double v)
{
  sao_stop_t *s, *stop = sao_stop_new(x, v);

  if (! stop)
    return 1;

  if (*base)
    {
      for (s = *base ; s->next ; s=s->next);
      s->next = stop;
    }
  else
    *base = stop;

  return 0;
}

int sao_red_push(sao_t *sao, double x, double v)
{
  return sao_channel_append(&(sao->red), x, v);
}

int sao_green_push(sao_t *sao, double x, double v)
{
  return sao_channel_append(&(sao->green), x, v);
}

int sao_blue_push(sao_t *sao, double x, double v)
{
  return sao_channel_append(&(sao->blue), x, v);
}

sao_t* sao_new(void)
{
  sao_t *sao;

  if ((sao = malloc(sizeof(sao_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = comment_new()) != NULL)
        {
          sao->red = sao->green = sao->blue = NULL;
          sao->comment = comment;

          return sao;
        }

      free(sao);
    }

  return NULL;
}

static void sao_stop_destroy(sao_stop_t *s)
{
  if (s)
    {
      sao_stop_destroy(s->next);
      free(s);
    }
}

void sao_destroy(sao_t *sao)
{
  if (sao != NULL)
    {
      sao_stop_destroy(sao->red);
      sao_stop_destroy(sao->green);
      sao_stop_destroy(sao->blue);
      comment_destroy(sao->comment);
    }

  free(sao);
}

static int sao_eachstop(const sao_stop_t *stop,
			int (*f)(double, double, void*),
			void *opt)
{
  return (stop ?
	  f(stop->x, stop->v, opt) + sao_eachstop(stop->next, f, opt) :
	  0);
}

int sao_eachred(const sao_t *sao, stop_fn_t *f, void *opt)
{
  return sao_eachstop(sao->red, f, opt);
}

int sao_eachgreen(const sao_t *sao, stop_fn_t *f, void *opt)
{
  return sao_eachstop(sao->green, f, opt);
}

int sao_eachblue(const sao_t *sao, stop_fn_t *f, void *opt)
{
  return sao_eachstop(sao->blue, f, opt);
}

comment_t* sao_comment(const sao_t *sao)
{
  return sao->comment;
}
