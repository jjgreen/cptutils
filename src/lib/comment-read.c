#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-read.h"
#include "cptutils/comment-list-read.h"
#include "cptutils/btrace.h"

comment_t* comment_read(const char *path)
{
  comment_t *comment = NULL;
  comment_list_t *list = comment_list_read(path);

  if (list != NULL)
    {
      size_t n = comment_list_size(list);

      if (n == 1)
        {
          const comment_t *comment0 = comment_list_entry(list, 0);

          if (comment0 != NULL)
            {
              comment = comment_new();

              if (comment != NULL)
                {
                  if (comment_copy(comment0, comment) != 0)
                    {
                      btrace("failed comment copy");
                      comment_destroy(comment);
                      comment = NULL;
                    }
                }
            }
          else
            btrace("failed to get comment 0 from list");
        }
      else
        btrace("file has %zu comments", n);

      comment_list_destroy(list);
    }
  else
    btrace("failed read of comment from %s", path);

  return comment;
}
