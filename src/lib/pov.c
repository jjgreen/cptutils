#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/pov.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

pov_t* pov_new(void)
{
  pov_t *pov;

  if ((pov = malloc(sizeof(pov_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = comment_new()) != NULL)
        {
          pov->n = 0;
          pov->stop = NULL;
          pov->name[0] = '\0';
          pov->comment = comment;

          return pov;
        }

      free(pov);
    }

  return NULL;
}

/* this can only be done once */

int pov_stops_alloc(pov_t *pov, size_t n)
{
  pov_stop_t *stop;

  if (n < 1) return 1;

  if (pov->n > 0) return 1;

  if ((stop = calloc(n, sizeof(pov_stop_t))) == NULL)
    return 1;

  pov->n = n;
  pov->stop = stop;

  return 0;
}

/*
  here we copy over the name and then normalise the
  name -- non-alphanumerics are converted to
  underscore (this needed for the pov-ray parser).
  The count passed to the function has the number
  of characters changed (caller may wish to warn)
*/

int pov_set_name(pov_t *pov, const char *name, size_t *n)
{
  char *c, *povname = pov->name;
  size_t m = 0;

  strncat(povname, name, POV_NAME_LEN-1);
  povname[POV_NAME_LEN-1] = '\0';

  for (c = povname, *n = 0 ; *c ; c++)
    if (! (isalnum(*c) || *c == '_'))
      {
	*c = '_';
	m++;
      }

  *n = m;

  return 0;
}

void pov_destroy(pov_t *pov)
{
  if (pov != NULL)
    {
      free(pov->stop);
      comment_destroy(pov->comment);
    }

  free(pov);
}
