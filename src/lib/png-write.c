#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/btrace.h"
#include "cptutils/png-write.h"

#include <png.h>

static int png_write3(png_structp pngH,
		      png_infop infoH,
		      FILE *stream,
		      const png_t *png,
		      const char *name)
{
  /*
    initialize rows of PNG, assigning each to the same
    simple row
  */

  png_byte **rows = malloc(png->height * sizeof(png_byte*));

  if (rows == NULL)
    {
      btrace("failed to allocate memory");
      return 1;
    }

  for (size_t j = 0 ; j < png->height ; j++)
    rows[j] = png->row;

  png_init_io(pngH, stream);
  png_set_rows(pngH, infoH, rows);
  png_write_png(pngH, infoH, PNG_TRANSFORM_IDENTITY, NULL);

  /* tidy up */

  free(rows);

  return 0;
}

static int png_write2(png_structp pngH,
		      png_infop infoH,
		      const png_t *png,
		      const char *path,
		      const char *name)
{
  if (setjmp(png_jmpbuf(pngH)))
    {
      btrace("failed to set lonjump");
      return 1;
    }

  /* Set image attributes. */

  png_set_IHDR(pngH,
	       infoH,
	       png->width,
	       png->height,
	       8,
	       PNG_COLOR_TYPE_RGBA,
	       PNG_INTERLACE_NONE,
	       PNG_COMPRESSION_TYPE_DEFAULT,
	       PNG_FILTER_TYPE_DEFAULT);

  /*
    since all rows are identical, using PNG_FILTER_UP
    gives us better compression
  */

  png_set_filter(pngH, PNG_FILTER_TYPE_BASE, PNG_FILTER_UP);

  /* Write the image data */

  FILE *st;
  int err = 1;

  if ((st = fopen(path, "wb")) != NULL)
    {
      err = png_write3(pngH, infoH, st, png, name);
      fclose(st);
    }

  return err;
}

int png_write(const png_t *png, const char *path, const char *name)
{
  int err = 1;
  png_structp pngH =
    png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (pngH == NULL)
    btrace("failed to create png write struct");
  else
    {
      png_infop infoH;

      if ((infoH = png_create_info_struct(pngH)) == NULL)
        btrace("failed to create png info struct");
      else
        err = png_write2(pngH, infoH, png, path, name);

      png_destroy_write_struct(&pngH, &infoH);
    }

  return err;
}
