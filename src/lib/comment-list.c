#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-list.h"
#include "cptutils/comment-type.h"

#include <stdlib.h>

struct comment_list_t
{
  size_t n, alloc;
  comment_t *comment;
};

#define LIST_INI 20
#define LIST_INC 20

comment_list_t* comment_list_new(void)
{
  comment_list_t *list;

  if ((list = calloc(1, sizeof(comment_list_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = calloc(LIST_INI, sizeof(comment_t))) != NULL)
        {
          list->comment = comment;
          list->alloc = LIST_INI;
          list->n = 0;

          return list;
        }

      free(list);
    }

  return NULL;
}

void comment_list_destroy(comment_list_t *list)
{
  if (list != NULL)
    {
      for (size_t i = 0 ; i < list->n ; i++)
        comment_deinit(list->comment + i);
      free(list->comment);
    }
  free(list);
}

size_t comment_list_size(const comment_list_t *list)
{
  return list->n;
}

comment_t* comment_list_next(comment_list_t *list)
{
  size_t
    n = list->n,
    a = list->alloc;

  if (n < a)
    {
      list->n++;

      comment_t *comment = list->comment + n;

      if (comment_init(comment) == 0)
        return comment;
    }
  else
    {
      a += LIST_INC;

      comment_t *comment;
      size_t size = sizeof(comment_t) * a;

      if ((comment = realloc(list->comment, size)) != NULL)
        {
          list->alloc = a;
          list->comment = comment;

          return comment_list_next(list);
        }
    }

  return NULL;
}

int comment_list_revert(comment_list_t *list)
{
  size_t n = list->n;

  if (n == 0)
    return 1;

  comment_t *comment = &(list->comment[n - 1]);
  comment_deinit(comment);
  list->n--;

  return 0;
}

int comment_list_each(comment_list_t *list,
                      int (*f)(comment_t*, void*),
                      void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      int err;
      comment_t *comment = list->comment + i;

      if ((err = f(comment, opt)) != 0)
        return err;
    }

  return 0;
}

comment_t* comment_list_find(comment_list_t *list,
                             int (*f)(comment_t*, void*),
                             void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      comment_t *comment = list->comment + i;

      if (f(comment, opt) != 0)
        return comment;
    }

  return NULL;
}

const comment_t* comment_list_entry(const comment_list_t *list, size_t i)
{
  if (i >= list->n)
    return NULL;

  return list->comment + i;
}
