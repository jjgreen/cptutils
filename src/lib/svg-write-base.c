#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/svg-write-base.h"

#include "cptutils/xml-id.h"
#include "cptutils/btrace.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#define ENCODING "utf-8"
#define BUFSZ 128

static int svg_attribute(xmlTextWriter *writer,
                         const char *name,
                         const char *value,
                         const char *element)
{
  if (xmlTextWriterWriteAttribute(writer,
                                  BAD_CAST name,
                                  BAD_CAST value) < 0)
    {
      btrace("setting %s %s attribute", element, name);
      return 1;
    }

  return 0;
}

static const char* timestring(void)
{
  time_t t;
  struct tm *stm;
  static char text[12];

  t = time(NULL);
  stm = localtime(&t);

  strftime(text, sizeof(text), "%Y-%m-%d", stm);

  return text;
}

static int svg_write_creator(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "xm:creator") < 0)
    btrace("open <xm:creator>");
  else
    {
      if ((svg_attribute(writer, "name", "cptutils", "xm:creator") != 0) ||
          (svg_attribute(writer, "version", VERSION, "xm:creator") != 0))
        btrace("write <xm:creator>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <xm:creator>");
          else
            err = 0;
        }
    }

  return err;
}

static int svg_write_created(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "xm:created") < 0)
    btrace("open <xm:created>");
  else
    {
      if (svg_attribute(writer, "date", timestring(), "xm:created") != 0)
        btrace("write <xm:created>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <xm:created>");
          else
            err = 0;
        }
    }

  return err;
}

static int svg_write_metadata(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "metadata") < 0)
    btrace("open <metadata>");
  else
    {
      if (xmlTextWriterStartElement(writer, BAD_CAST "rdf:RDF") < 0)
        btrace("open <rdf:RDF>");
      else
        {
          if ((svg_attribute(writer,
                             "xmlns:rdf",
                             "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                             "rdf:RDF") != 0) ||
              (svg_attribute(writer,
                             "xmlns:xm",
                             "http://jjg.gitlab.io/2025/metadata",
                             "rdf:RDF") != 0))
            btrace("RDF attributes");
          else
            {
              if (xmlTextWriterStartElement(writer, BAD_CAST
                                            "rdf:Description") < 0)
                btrace("open <rdf:Deescription>");
              else
                {
                  if ((svg_write_creator(writer) != 0) ||
                      (svg_write_created(writer) != 0))
                    btrace("write <metadata>");
                  else
                    {
                      if (xmlTextWriterEndElement(writer) < 0)
                        btrace("close <metadata>");
                      else
                        err = 0;
                    }
                }
            }
        }
    }

  return err;
}

static int svg_write_stop(xmlTextWriter *writer, svg_stop_t stop)
{
  char obuf[BUFSZ], scbuf[BUFSZ], sobuf[BUFSZ];
  rgb_t col = stop.colour;
  double
    value = stop.value,
    opacity = stop.opacity;

  snprintf(obuf, BUFSZ, "%.4f%%", value);
  snprintf(scbuf, BUFSZ, "rgb(%i, %i, %i)", col.red, col.green, col.blue);
  snprintf(sobuf, BUFSZ, "%.4f", opacity);

  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "stop") < 0)
    btrace("open <stop>");
  else
    {
      if ((svg_attribute(writer, "offset", obuf, "stop") != 0) ||
          (svg_attribute(writer, "stop-color", scbuf, "stop") != 0) ||
          (svg_attribute(writer, "stop-opacity", sobuf, "stop") != 0))
        btrace("write <stop>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <stop>");
          else
            err = 0;
        }
    }

  return err;
}

static int svg_write_comment(const char *line, void *arg)
{
  xmlTextWriter *writer = arg;

  if (line != NULL)
    {
      if (xmlTextWriterWriteFormatComment(writer, " %s ", line) < 0)
        {
          btrace("single-line comment");
          return 1;
        }
    }

  return 0;
}

#define SPACE4 "    "
#define SPACE6 "      "

static int svg_write_comments(const char *line, void *arg)
{
  xmlTextWriter *writer = arg;

  if (line == NULL)
    {
      if (xmlTextWriterWriteString(writer, BAD_CAST "\n") < 0)
        {
          btrace("comment");
          return 1;
        }
    }
  else
    {
      size_t n = strlen(line) + 8;
      char buffer[n];
      snprintf(buffer, n, SPACE6 "%s\n", line);

      if (xmlTextWriterWriteString(writer, BAD_CAST buffer) < 0)
        {
          btrace("comment");
          return 1;
        }
    }

  return 0;
}

static int svg_write_lineargradient(xmlTextWriter *writer, const svg_t *svg)
{
  if (xmlTextWriterStartElement(writer, BAD_CAST "linearGradient") < 0)
    {
      btrace("open <linearGradient>");
      return 1;
    }

  if ((svg_attribute(writer, "id", (const char*)svg->id,
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "data-name", (const char*)svg->name,
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "gradientUnits", "objectBoundingBox",
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "spreadMethod", "pad",
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "x1", "0%",
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "x2", "100%",
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "y1", "0%",
                     "linearGradient") != 0) ||
      (svg_attribute(writer, "y2", "0%",
                     "linearGradient") != 0))
    return 1;

  if (comment_count(svg->comment) == 1)
    {
      if (comment_each(svg->comment, svg_write_comment, writer) != 0)
        return 1;
    }
  else if (comment_count(svg->comment) > 1)
    {
      if (xmlTextWriterStartComment(writer) < 0)
        {
          btrace("open comment");
          return 1;
        }

      if (xmlTextWriterWriteString(writer, BAD_CAST "\n") < 0)
        {
          btrace("initial newline");
          return 1;
        }

      if (comment_each(svg->comment, svg_write_comments, writer) != 0)
        {
          btrace("comment body");
          return 1;
        }

      if (xmlTextWriterWriteString(writer, BAD_CAST SPACE4) < 0)
        {
          btrace("comment end indent");
          return 1;
        }

      if (xmlTextWriterEndComment(writer) < 0)
        {
          btrace("close comment");
          return 1;
        }
    }

  for (svg_node_t *node = svg->nodes ; node ; node = node->r)
    if (svg_write_stop(writer, node->stop) != 0)
      return 1;

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("close <linearGradient>");
      return 1;
    }

  return 0;
}

static int svg_write_mem(xmlTextWriter *writer,
                         size_t n,
                         const svg_t **svg,
                         const svg_preview_t *preview,
                         svg_version_t version)
{
  if (n < 1)
    {
      btrace("no gradients to write");
      return 1;
    }

  if ((n > 1) && preview->use)
    {
      btrace("no previews with multi-gradient output (yet)");
      return 1;
    }

  for (size_t i = 0 ; i < n ; i++)
    {
      if (svg[i]->nodes == NULL)
        {
          btrace("svg %zu has no segments", i);
          return 1;
        }
    }

  /* start xml */

  if (xmlTextWriterStartDocument(writer, NULL, ENCODING, NULL) < 0)
    {
      btrace("start document");
      return 1;
    }

  /* svg */

  if (xmlTextWriterStartElement(writer, BAD_CAST "svg") < 0)
    {
      btrace("open <svg>");
      return 1;
    }

  /*
    the version attribute is deprecated in 2.0 and widely ignored
    by browsers in 1.1 (they use the XMLNS instead), but there may
    be other software which does use it ...
  */

  if ((version == svg_version_11) &&
      (svg_attribute(writer, "version", "1.1", "svg") != 0))
    {
      btrace("version attribute for <svg>");
      return 1;
    }

  const char *xmlns;

  switch (version)
    {
    case svg_version_11:
      xmlns = "http://www.w3.org/2000/svg";
      break;
    case svg_version_20:
      xmlns = "http://www.w3.org/2008/svg";
      break;
    default:
      btrace("bad SVG version");
      return 1;
    }

  if (svg_attribute(writer, "xmlns", xmlns, "svg") != 0)
    {
      btrace("xmlns attribute for <svg>");
      return 1;
    }

  if (preview->use)
    {
      char str[BUFSZ];
      size_t
        w = preview->width,
        h = preview->height;

      if ((snprintf(str, BUFSZ, "%zupx", w) >= BUFSZ) ||
          (svg_attribute(writer, "width", str, "svg") != 0) ||
          (snprintf(str, BUFSZ, "%zupx", h) >= BUFSZ) ||
          (svg_attribute(writer, "height", str, "svg") != 0) ||
          (snprintf(str, BUFSZ, "0 0 %zu %zu", w, h) >= BUFSZ) ||
          (svg_attribute(writer, "viewBox", str, "svg") != 0))
        {
          btrace("preview header");
          return 1;
        }

      if (xmlTextWriterStartElement(writer, BAD_CAST "g") < 0)
        {
          btrace("open <g>");
          return 1;
        }

      /* defs */

      if (xmlTextWriterStartElement(writer, BAD_CAST "defs") < 0)
        {
          btrace("open <defs>");
          return 1;
        }

      for (size_t i = 0 ; i < n ; i++)
        {
          if (svg_write_lineargradient(writer, svg[i]) != 0)
            return 1;
        }

      if (xmlTextWriterEndElement(writer) < 0)
        {
          btrace("close <defs>");
          return 1;
        }

      /* preview rectangle for first gradient */

      if (xmlTextWriterStartElement(writer, BAD_CAST "rect") < 0)
        {
          btrace("open <rect>");
          return 1;
        }

      char *id;

      if ((id = xml_id_sanitise((const char*)(svg[0]->name))) != NULL)
        {
          int n = snprintf(str, BUFSZ, "url(#%s)", id);

          free(id);

          if (n >= BUFSZ)
            {
              btrace("buffer overflow");
              return 1;
            }
        }
      else
        {
          btrace("sanitise id %s", svg[0]->name);
          return 1;
        }

      if ((svg_attribute(writer, "fill", str, "rect") != 0) ||
          (svg_attribute(writer, "x", "0", "rect") != 0) ||
          (svg_attribute(writer, "y", "0", "rect") != 0) ||
          (snprintf(str, BUFSZ, "%zu", w) >= BUFSZ) ||
          (svg_attribute(writer, "width", str, "rect") != 0) ||
          (snprintf(str, BUFSZ, "%zu", h) >= BUFSZ) ||
          (svg_attribute(writer, "height", str, "rect") != 0))
        {
          btrace("write <rect>");
          return 1;
        }

      if (xmlTextWriterEndElement(writer) < 0)
        {
          btrace("close <rect>");
          return 1;
        }

      if (xmlTextWriterEndElement(writer) < 0)
        {
          btrace("close <g>");
          return 1;
        }

      if (svg_write_metadata(writer) != 0)
        return 1;
    }
  else
    {
      for (size_t i = 0 ; i < n ; i++)
        {
          if (svg_write_lineargradient(writer, svg[i]) != 0)
            return 1;
        }

      if (svg_write_metadata(writer) != 0)
        return 1;
    }

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("close <svg>");
      return 1;
    }

  if (xmlTextWriterEndDocument(writer) < 0)
    {
      btrace("end document");
      return 1;
    }

  return 0;
}

int svg_write_base(const svg_t **svg,
                   size_t n,
                   const char *file,
                   const svg_preview_t *preview,
                   svg_version_t version)
{
  xmlBuffer *buffer;
  int err = 1;

  if ((buffer = xmlBufferCreate()) == NULL)
    btrace("creating xml writer buffer");
  else
    {
      xmlTextWriter *writer;

      if ((writer = xmlNewTextWriterMemory(buffer, 0)) == NULL)
        btrace("creating the xml writer");
      else
        {
          /* set two-space indentation, not fatal if this fails */

          if (xmlTextWriterSetIndent(writer, 1) != 0)
            btrace("failed enable indent");

          unsigned char indent[] = {' ', ' ', '\0'};

          if (xmlTextWriterSetIndentString(writer, indent) != 0)
            btrace("failed to set indent string");

          /*
            we free the writer at the start of each of these branches
            since this must be done before using the buffer
          */

          if (svg_write_mem(writer, n, svg, preview, version) != 0)
            {
              xmlFreeTextWriter(writer);
              btrace("memory write");
            }
          else
            {
              xmlFreeTextWriter(writer);

              /*
                don't take a

                  const char *buffer = buffer->content

                to use in place of buffer->content here, that trips
                some weird behaviour in gcc and leads to corrupt xml
              */

              if (file)
                {
                  FILE *fp;

                  if ((fp = fopen(file, "w")) == NULL)
                    btrace("opening file %s", file);
                  else
                    {
                      fprintf(fp, "%s", buffer->content);

                      if (fclose(fp) != 0)
                        btrace("closing file %s", file);
                      else
                        err = 0;
                    }
                }
              else
                {
                  fprintf(stdout, "%s", buffer->content);
                  err = 0;
                }
            }
        }

      xmlBufferFree(buffer);
    }

  return err;
}
