#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/ggr.h"
#include "cptutils/btrace.h"
#include "cptutils/macro.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#define EPSILON 1e-10

static void seg_free_segment(ggr_segment_t*);
static void seg_free_segments(ggr_segment_t*);
static ggr_segment_t* seg_get_segment_at(ggr_t*, double);
static bool seg_valid(ggr_segment_t*);

static double calc_linear_factor(double, double);
static double calc_curved_factor(double, double);
static double calc_sine_factor(double, double);
static double calc_sphere_increasing_factor(double, double);
static double calc_sphere_decreasing_factor(double, double);

ggr_t *ggr_new(void)
{
  ggr_t *grad;

  if ((grad = malloc(sizeof(ggr_t))) == NULL)
    return NULL;

  grad->name = NULL;
  grad->filename = NULL;
  grad->segments = NULL;
  grad->last_visited = NULL;

  return grad;
}

void ggr_destroy(ggr_t *grad)
{
  if (grad == NULL) return;

  if (grad->name) free(grad->name);
  if (grad->filename) free(grad->filename);
  if (grad->segments) seg_free_segments(grad->segments);

  free(grad);
}

bool ggr_valid(const ggr_t *ggr)
{
  if (ggr == NULL)
    return true;
  else
    return seg_valid(ggr->segments);
}

int ggr_segment_colour(double z,
                       const ggr_segment_t *seg,
                       const double *bgD,
                       double *rgbD)
{
  double alpha;
  int err;

  if ((err = ggr_segment_rgba(z, seg, rgbD, &alpha)) != 0)
    return err;

  for (size_t i = 0 ; i < 3 ; i++)
    rgbD[i] = alpha * rgbD[i] + (1 - alpha) * bgD[i];

  return 0;
}

int ggr_segment_rgba(double z,
                     const ggr_segment_t *seg,
                     double *rgbD,
                     double *alpha)
{
  double factor = 0;
  double seg_len, middle;

  seg_len = seg->right - seg->left;

  if (seg_len < EPSILON)
    {
      middle = 0.5;
      z = 0.5;
    }
  else
    {
      middle = (seg->middle - seg->left) / seg_len;
      z = (z - seg->left) / seg_len;
    }

  switch (seg->type)
    {
    case GGR_LINEAR:
      factor = calc_linear_factor(middle, z);
      break;
    case GGR_CURVED:
      factor = calc_curved_factor(middle, z);
      break;
    case GGR_SINE:
      factor = calc_sine_factor(middle, z);
      break;
    case GGR_SPHERE_INCREASING:
      factor = calc_sphere_increasing_factor(middle, z);
      break;
    case GGR_SPHERE_DECREASING:
      factor = calc_sphere_decreasing_factor(middle, z);
      break;
    default:
      btrace("Corrupt gradient");
      return 1;
    }

  /* alpha channel is easy */

  *alpha = seg->a0 + (seg->a1 - seg->a0)*factor;

  /* Calculate color components */

  if (seg->color == GGR_RGB)
    {
      rgbD[0] = seg->r0 + (seg->r1 - seg->r0)*factor;
      rgbD[1] = seg->g0 + (seg->g1 - seg->g0)*factor;
      rgbD[2] = seg->b0 + (seg->b1 - seg->b0)*factor;
    }
  else
    {
      double  h0, s0, v0, h1, s1, v1;
      double hsvD[3];

      rgbD[0] = seg->r0;
      rgbD[1] = seg->g0;
      rgbD[2] = seg->b0;

      rgbD_to_hsvD(rgbD, hsvD);

      h0 = hsvD[0];
      s0 = hsvD[1];
      v0 = hsvD[2];

      rgbD[0] = seg->r1;
      rgbD[1] = seg->g1;
      rgbD[2] = seg->b1;

      rgbD_to_hsvD(rgbD, hsvD);

      h1 = hsvD[0];
      s1 = hsvD[1];
      v1 = hsvD[2];

      s0 = s0 + (s1 - s0) * factor;
      v0 = v0 + (v1 - v0) * factor;

      switch (seg->color)
	{
	case GGR_HSV_CCW:
	  if (h0 < h1)
	    h0 = h0 + (h1 - h0) * factor;
	  else
	    {
	      h0 = h0 + (1.0 - (h0 - h1)) * factor;
	      if (h0 > 1.0)
		h0 -= 1.0;
	    }
	  break;
	case GGR_HSV_CW:
	  if (h1 < h0)
	    h0 = h0 - (h0 - h1)*factor;
	  else
	    {
	      h0 = h0 - (1.0 - (h1 - h0)) * factor;
	      if (h0 < 0.0)
		h0 += 1.0;
	    }
	  break;
	default:
	  btrace("unknown colour model %i", seg->color);
	  return 1;
	}

      hsvD[0] = h0;
      hsvD[1] = s0;
      hsvD[2] = v0;

      hsvD_to_rgbD(hsvD, rgbD);
   }

  return 0;
}


int ggr_colour(double z, ggr_t *gradient, double *bg, double *rgbD)
{
  /* if there is no gradient return the background colour */

  if (gradient == NULL)
    {
      for (size_t i = 0 ; i < 3 ; i++)
        rgbD[i] = bg[i];
      return 0;
    }

  if (z < 0.0)
    z = 0.0;
  else if (z > 1.0)
    z = 1.0;

  ggr_segment_t *seg;

  if ((seg = seg_get_segment_at(gradient, z)) == NULL)
    return 1;

  return ggr_segment_colour(z, seg, bg, rgbD);
}

ggr_segment_t* ggr_segment_new(void)
{
  ggr_segment_t *seg;

  if ((seg = malloc(sizeof(ggr_segment_t))) == NULL)
    return NULL;

  seg->left = 0.0;
  seg->middle = 0.5;
  seg->right = 1.0;

  seg->r0 = seg->g0 = seg->b0 = 0.0;
  seg->r1 = seg->g1 = seg->b1 = seg->a0 = seg->a1 = 1.0;

  seg->type  = GGR_LINEAR;
  seg->color = GGR_RGB;

  seg->prev = seg->next = NULL;

  return seg;
}

static bool seg_valid(ggr_segment_t *segments)
{
  const ggr_segment_t *seg1, *seg2;

  if (((seg1 = segments) == NULL) || ((seg2 = seg1->next) == NULL))
    return true;

  while (seg1 && seg2)
    {
      if (seg1->right != seg2->left)
        {
          btrace("invalid segments [%f, %f] [%f, %f]",
                 seg1->left, seg1->right,
                 seg2->left, seg2->right);
          return false;
        }

      seg1 = seg2;
      seg2 = seg2->next;
    }

  return true;
}

static void seg_free_segment(ggr_segment_t *seg)
{
  free(seg);
}

static void seg_free_segments(ggr_segment_t *seg)
{
  ggr_segment_t *tmp;

  if (seg == NULL) return;

  while (seg)
    {
      tmp = seg->next;
      seg_free_segment(seg);
      seg = tmp;
    }
}

static ggr_segment_t* seg_get_segment_at(ggr_t *grad, double z)
{
  ggr_segment_t *seg;

  z = MIN(z, 1.0);
  z = MAX(z, 0.0);

  if (grad->last_visited)
    seg = grad->last_visited;
  else
    seg = grad->segments;

  while (seg)
    {
      if (z >= seg->left)
	{
	  if (z <= seg->right)
	    {
	      grad->last_visited = seg;
	      return seg;
	    }
	  else
	    seg = seg->next;
	}
      else
	seg = seg->prev;
    }

  btrace("no matching segment for z %0.15f", z);

  return NULL;
}

/*
  calculation functions
*/

static double calc_linear_factor(double middle, double z)
{
  if (z <= middle)
    {
      if (middle < EPSILON)
	return 0;
      else
	return 0.5 * z / middle;
    }
  else
    {
      z -= middle;
      middle = 1 - middle;

      if (middle < EPSILON)
	return 1;
      else
	return 0.5 + 0.5 * z / middle;
    }
}

static double calc_curved_factor(double middle, double z)
{
  if (middle < EPSILON)
    middle = EPSILON;

  return pow(z, log(0.5) / log(middle));
}

static double calc_sine_factor(double middle, double z)
{
  z = calc_linear_factor(middle, z);

  return (sin((-M_PI / 2) + M_PI * z) + 1) / 2;
}

/* Works for convex increasing and concave decreasing */

static double calc_sphere_increasing_factor(double middle, double z)
{
  z = calc_linear_factor(middle, z) - 1.0;

  return sqrt(1 - z * z);
}

/* Works for convex decreasing and concave increasing */

static double calc_sphere_decreasing_factor(double middle, double z)
{
  z = calc_linear_factor(middle, z);

  return 1 - sqrt(1 - z * z);
}
