#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/gstack.h"
#include "cptutils/btrace.h"
#include "cptutils/pg-type.h"
#include "cptutils/pg-write.h"

#include <unistd.h>

#include <stddef.h>
#include <math.h>


static int leq_last(void *vstop, void *vlast)
{
  pg_stop_t *stop = vstop;
  double *last = vlast;

  if (stop->value <= *last)
    {
      *last = stop->value;
      return 1;
    }
  else
    return -1;
}

static int geq_last(void *vstop, void *vlast)
{
  pg_stop_t *stop = vstop;
  double *last = vlast;

  if (stop->value >= *last)
    {
      *last = stop->value;
      return 1;
    }
  else
    return -1;
}

static bool pg_is_decreasing(pg_t *pg)
{
  double last = INFINITY;
  return gstack_foreach(pg->stack, leq_last, &last) == 0;
}

static bool pg_is_increasing(pg_t *pg)
{
  double last = -INFINITY;
  return gstack_foreach(pg->stack, geq_last, &last) == 0;
}

static int pg_coerce_increasing(pg_t *pg)
{
  if (pg_is_increasing(pg))
    return 0;

  if (pg_is_decreasing(pg))
    {
      gstack_reverse(pg->stack);
      return 0;
    }

  btrace("neither increasing nor decreasing, corrupt input?");

  return 1;
}

typedef struct
{
  double min, max;
} range_t;

static int get_range(void *vstop, void *vrange)
{
  pg_stop_t *stop = vstop;
  range_t *range = vrange;

  if (stop->value > range->max)
    range->max = stop->value;

  if (stop->value < range->min)
    range->min = stop->value;

  return 1;
}

typedef struct
{
  double m, c;
} affine_t;

static int affine_transform(void *vstop, void *vaffine)
{
  pg_stop_t *stop = vstop;
  affine_t *affine = vaffine;
  stop->value = affine->m * stop->value + affine->c;
  return 1;
}

static int pg_coerce_range(pg_t *pg, double min, double max)
{
  range_t range = { .max = -INFINITY, .min = INFINITY };

  if (gstack_foreach(pg->stack, get_range, &range) != 0)
    {
      btrace("failed to get range");
      return 1;
    }

  double
    m = (max - min) / (range.max - range.min),
    c = min - range.min * m;
  affine_t affine = { .m = m, .c = c };

  if (gstack_foreach(pg->stack, affine_transform, &affine) != 0)
    {
      btrace("failed to transform values");
      return 1;
    }

  return 0;
}

static int pg_write_stream(pg_t *pg, FILE *st)
{
  if (pg_coerce_increasing(pg) != 0)
    {
      btrace("failed to coerce increasing");
      return 1;
    }

  pg_stop_t stop;
  const char *format;

  if (pg->percentage)
    {
      if (pg_coerce_range(pg, 0, 100) != 0)
	{
	  btrace("failed to coerce into range [0, 100]");
	  return 1;
	}
      format = "%7.3f%% %3i %3i %3i %u\n";
    }
  else
    format = "%-7g %3i %3i %3i %u\n";

  while (gstack_pop(pg->stack, &stop) == 0)
    {
      fprintf(st, format,
	      stop.value,
	      stop.rgb.red,
	      stop.rgb.green,
	      stop.rgb.blue,
	      stop.alpha);
    }

  return 0;
}

int pg_write(const pg_t *pg, const char *path)
{
  int err = 1;
  pg_t *pg2;

  if ((pg2 = pg_clone(pg)) != NULL)
    {
      if (path)
        {
          FILE *st = fopen(path, "w");

          if (st != NULL)
            {
              err = pg_write_stream(pg2, st);
              fclose(st);
              if (err != 0)
                unlink(path);
            }
          else
            btrace("failed opening %s", path);
        }
      else
        err = pg_write_stream(pg2, stdout);

      pg_destroy(pg2);
    }

  return err;
}
