#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/btrace.h"
#include "cptutils/png.h"

png_t* png_new(size_t w, size_t h)
{
  if (w * h == 0)
    return NULL;

  png_t *png = malloc(sizeof(png_t));

  if (png == NULL)
    btrace("failed to allocate memory for png structure");
  else
    {
      unsigned char *row = malloc(4 * w);

      if (row == NULL)
        btrace("failed to allocate memory for png rows");
      else
	{
          png->row = row;
	  png->width = w;
	  png->height = h;

	  return png;
	}

      free(png);
    }

  return NULL;
}

void png_destroy(png_t *png)
{
  if (png != NULL)
    free(png->row);
  free(png);
}
