#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/svg-write.h"
#include "cptutils/svg-write-base.h"

int svg_write(const svg_t *svg,
              const char *path,
              const svg_preview_t *preview,
              svg_version_t version)
{
  return svg_write_base(&svg, 1, path, preview, version);
}
