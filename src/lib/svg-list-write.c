#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/svg-list-write.h"
#include "cptutils/svg-write-base.h"
#include "cptutils/btrace.h"

int svg_list_write(const svg_list_t *list,
                   const char *path,
                   const svg_preview_t *preview,
                   svg_version_t version)
{
  size_t n = svg_list_size(list);

  if (n == 0)
    return 1;

  const svg_t *svg[n];

  for (size_t i = 0 ; i < n ; i++)
    {
      if ((svg[i] = svg_list_entry(list, i)) == NULL)
        return 1;
    }

  return svg_write_base(svg, n, path, preview, version);
}
