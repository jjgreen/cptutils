#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-list-read.h"
#include "cptutils/xml-error-handler.h"
#include "cptutils/btrace.h"

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define ENCODING "utf-8"


static int comment_read(xmlNodePtr comments_node, comment_t *comment)
{
  xmlNode
    *comment_nodes = comments_node->children,
    *comment_node;

  for (comment_node = comment_nodes ;
       comment_node ;
       comment_node = comment_node->next)
    {
      if (comment_node->type == XML_ELEMENT_NODE)
        {
          if (strcmp((const char*)comment_node->name, "line") != 0)
            {
              btrace("unexpected %s node", comment_node->name);
              return 1;
            }

          xmlChar *content = xmlNodeGetContent(comment_node);
          int err = comment_push(comment, (char*)content);

          xmlFree(content);

          if (err != 0)
            {
              btrace("error pushing line");
              return 1;
            }
        }
    }

  if (comment_finalise(comment) != 0)
    return 1;

  return 0;
}

static int comment_list_read3(xmlNodeSetPtr nodes, comment_list_t *list)
{
  size_t size = (nodes ? nodes->nodeNr : 0);

  for (size_t i = 0 ; i < size ; ++i)
    {
      xmlNodePtr node = nodes->nodeTab[i];

      assert(node);

      if (node->type != XML_ELEMENT_NODE)
        {
          btrace("bad comments: 'line' is not a node");
          return 1;
        }

      comment_t *comment;

      if ((comment = comment_list_next(list)) == NULL)
        {
          btrace("failed to get comments object from list");
          return 1;
        }

      if (comment_read(node, comment) != 0)
        return 1;
    }

  return 0;
}

static int comment_list_read2(const char *file, comment_list_t *list)
{
  int err = 1;
  xmlDocPtr doc;

  xmlInitParser();
  xmlSetStructuredErrorFunc(NULL, xml_error_handler);

  if ((doc = xmlParseFile(file)) == NULL)
    btrace("error: unable to parse file %s", file);
  else
    {
      xmlXPathContextPtr xpc;

      if ((xpc = xmlXPathNewContext(doc)) == NULL)
        btrace("error: unable to create new XPath context");
      else
        {
          const unsigned char
            prefix[] = "cs",
            xmlns[] = "http://jjg.gitlab.io/2025/comments";

          if (xmlXPathRegisterNs(xpc, prefix, xmlns) != 0)
            btrace("namespace error for %s:%s", prefix, xmlns);
          else
            {
              const xmlChar xpe[] = "/cs:comments/cs:comment";
              xmlXPathObjectPtr xpo;

              if ((xpo = xmlXPathEvalExpression(xpe, xpc)) == NULL)
                btrace("unable to evaluate xpath expression %s", xpe);
              else
                {
                  err = comment_list_read3(xpo->nodesetval, list);
                  xmlXPathFreeObject(xpo);
                }
            }

          xmlXPathFreeContext(xpc);
        }

      xmlFreeDoc(doc);
    }

  xmlCleanupParser();

  return err;
}

comment_list_t* comment_list_read(const char *path)
{
  comment_list_t *list;

  if ((list = comment_list_new()) != NULL)
    {
      if (comment_list_read2(path, list) == 0)
        return list;

      comment_list_destroy(list);
    }

  return NULL;
}
