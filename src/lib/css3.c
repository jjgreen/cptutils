#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/css3.h"
#include "cptutils/btrace.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

css3_t* css3_new(void)
{
  css3_t *css3;

  if ((css3 = malloc(sizeof(css3_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = comment_new()) != NULL)
        {
          css3->angle = 0.0;
          css3->n = 0;
          css3->stop = NULL;
          css3->comment = comment;

          return css3;
        }
      else
        btrace("failed to create comment");

      free(css3);
    }
  else
    btrace("failed css3 memory allocation");

  return NULL;
}

int css3_stops_alloc(css3_t *css3, size_t n)
{
  css3_stop_t *stop;

  if (n < 1)
    return 1;

  if (css3->n > 0)
    return 1;

  if ((stop = calloc(n, sizeof(css3_stop_t))) == NULL)
    {
      btrace("failed memory allocation");
      return 1;
    }

  css3->n = n;
  css3->stop = stop;

  return 0;
}

void css3_destroy(css3_t *css3)
{
  if (css3 != NULL)
    {
      free(css3->stop);
      comment_destroy(css3->comment);
    }

  free(css3);
}
