#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-write-base.h"

#include "cptutils/xml-id.h"
#include "cptutils/btrace.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#define ENCODING "utf-8"
#define XMLNS "http://jjg.gitlab.io/2025/comments"
#define XMLNS_VER "0.9"

static int attribute(xmlTextWriter *writer,
                     const char *name,
                     const char *value,
                     const char *element)
{
  if (xmlTextWriterWriteAttribute(writer,
                                  BAD_CAST name,
                                  BAD_CAST value) < 0)
    {
      btrace("setting %s %s attribute", element, name);
      return 1;
    }

  return 0;
}

static int write_comment(const char *text, void *arg)
{
  xmlTextWriter *writer = arg;
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "line") < 0)
    btrace("open <line>");
  else
    {
      if ((text != NULL) &&
          (xmlTextWriterWriteString(writer, BAD_CAST text) < 0))
        btrace("writing comment body");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <line>");
          else
            err = 0;
        }
    }

  return err;
}

static int write_comments(xmlTextWriter *writer, const comment_t *comment)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "comment") < 0)
    btrace("open <comment>");
  else
    {
      if (comment_each(comment, write_comment, writer) != 0)
        btrace("writing <comment>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <comment>");
          else
            err = 0;
        }
    }

  return err;
}

static int write_creator(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "creator") < 0)
    btrace("open <creator>");
  else
    {
      if ((attribute(writer, "name", "cptutils", "creator") != 0) ||
          (attribute(writer, "version", VERSION, "creator") != 0))
        btrace("write <creator>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <creator>");
          else
            err = 0;
        }
    }

  return err;
}

static const char* timestring(void)
{
  time_t  tm;
  char *tmstr;
  static char ts[25];

  time(&tm);
  tmstr = ctime(&tm);

  sprintf(ts, "%.24s", tmstr);

  return ts;
}

static int write_created(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "created") < 0)
    btrace("open <created>");
  else
    {
      if (attribute(writer, "date", timestring(), "created") != 0)
        btrace("write <created>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <created>");
          else
            err = 0;
        }
    }

  return err;
}

static int write_metadata(xmlTextWriter *writer)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "metadata") < 0)
    btrace("open <metadata>");
  else
    {
      if ((write_creator(writer) != 0) ||
          (write_created(writer) != 0))
        btrace("write <metadata>");
      else
        {
          if (xmlTextWriterEndElement(writer) < 0)
            btrace("close <metadata>");
          else
            err = 0;
        }
    }

  return err;
}

static int write_comment_list(xmlTextWriter *writer,
                              size_t n,
                              const comment_t **list)
{
  for (size_t i = 0 ; i < n ; i++)
    if (write_comments(writer, list[i]) != 0)
      return 1;

  return 0;
}

static int write_mem(xmlTextWriter *writer,
                     size_t n,
                     const comment_t **list)
{
  if (n < 1)
    {
      btrace("no comments to write");
      return 1;
    }

  int err = 1;

  if (xmlTextWriterStartDocument(writer, NULL, ENCODING, NULL) < 0)
    btrace("start document");
  else
    {
      if (xmlTextWriterStartElement(writer, BAD_CAST "comments") < 0)
        btrace("open <comments>");
      else
        {
          if ((attribute(writer, "version", XMLNS_VER, "comments") != 0) ||
              (attribute(writer, "xmlns", XMLNS, "comments") != 0))
            btrace("attributes");
          else
            {
              if (write_comment_list(writer, n, list) != 0)
                btrace("writing comments");
              else
                {
                  if (write_metadata(writer) != 0)
                    btrace("writing metadata");
                  else
                    {
                      if (xmlTextWriterEndElement(writer) < 0)
                        btrace("close <comments>");
                      else
                        {
                          if (xmlTextWriterEndDocument(writer) < 0)
                            btrace("end document");
                          else
                            err = 0;
                        }
                    }
                }
            }
        }
    }

  return err;
}

int comment_write_base(const comment_t **list,
                       size_t n,
                       const char *file)
{
  xmlBuffer *buffer;
  int err = 1;

  if ((buffer = xmlBufferCreate()) == NULL)
    btrace("creating xml writer buffer");
  else
    {
      xmlTextWriter *writer;

      if ((writer = xmlNewTextWriterMemory(buffer, 0)) == NULL)
        btrace("creating the xml writer");
      else
        {
          /* set two-space indentation, not fatal if this fails */

          if (xmlTextWriterSetIndent(writer, 1) != 0)
            btrace("failed enable indent");

          unsigned char indent[] = {' ', ' ', '\0'};

          if (xmlTextWriterSetIndentString(writer, indent) != 0)
            btrace("failed to set indent string");

          /*
            we free the writer at the start of each of these branches
            since this must be done before using the buffer
          */

          if (write_mem(writer, n, list) != 0)
            {
              xmlFreeTextWriter(writer);
              btrace("memory write");
            }
          else
            {
              xmlFreeTextWriter(writer);

              /*
                don't take a

                  const char *buffer = buffer->content

                to use in place of buffer->content here, that trips
                some weird behaviour in gcc and leads to corrupt xml
              */

              if (file)
                {
                  FILE *fp;

                  if ((fp = fopen(file, "w")) == NULL)
                    btrace("opening file %s", file);
                  else
                    {
                      fprintf(fp, "%s", buffer->content);

                      if (fclose(fp) != 0)
                        btrace("closing file %s", file);
                      else
                        err = 0;
                    }
                }
              else
                {
                  fprintf(stdout, "%s", buffer->content);
                  err = 0;
                }
            }
        }

      xmlBufferFree(buffer);
    }

  return err;
}
