#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/grd5-string.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void grd5_string_destroy(grd5_string_t *gstr)
{
  if (gstr)
    {
      if ((gstr->len > 0) && (gstr->content))
	free(gstr->content);
      free(gstr);
    }
}

bool grd5_string_matches(const grd5_string_t *gstr, const char *other)
{
  return strncmp(gstr->content, other, gstr->len) == 0;
}
