#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/pov.h"
#include "cptutils/btrace.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>

static bool has_name(const pov_t *pov)
{
  return pov->name[0] != '\0';
}

static int write_comment(const char *line, void *arg)
{
  FILE *st = arg;

  if (line)
    fprintf(st, "// %s\n", line);
  else
    fprintf(st, "//\n");

  return 0;
}

int pov_write_stream(const pov_t *pov, FILE *st)
{
  if (comment_each(pov->comment, write_comment, st) != 0)
    {
      btrace("failed to write comment");
      return 1;
    }

  if (has_name(pov))
    fprintf(st, "#declare %s =\n", pov->name);

  fprintf(st, "color_map {\n");

  for (size_t i = 0 ; i < pov->n ; i++)
    {
      pov_stop_t stop = pov->stop[i];

      fprintf(st, "  [%7.5f color rgbf <%.4f,%.4f,%.4f,%.4f>]\n",
	      stop.z,
	      stop.rgbt[0],
	      stop.rgbt[1],
	      stop.rgbt[2],
	      stop.rgbt[3]);
    }

  fprintf(st, "}\n");

  return 0;
}

int pov_write(const pov_t *pov, const char *file)
{
  if (pov->n < 2)
    {
      btrace("povray does not support %zi stops", pov->n);
      return 1;
    }

  int err = 1;

  if (file)
    {
      FILE *st;

      if ((st = fopen(file, "w")) != NULL)
        {
          err = pov_write_stream(pov, st);
          fclose(st);
        }
    }
  else
    err = pov_write_stream(pov, stdout);

  return err;
}
