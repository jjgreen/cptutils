#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/ggr-write.h"
#include "cptutils/btrace.h"

#include <string.h>
#include <errno.h>

int ggr_write_stream(const ggr_t *ggr, FILE *stream)
{
  /*
     File format is:

     GIMP Gradient
     Name: <name>
     number_of_segments
     left middle right r0 g0 b0 a0 r1 g1 b1 a1 type coloring
     left middle right r0 g0 b0 a0 r1 g1 b1 a1 type coloring
     ...
  */

  fprintf(stream, "GIMP Gradient\n");
  fprintf(stream, "Name: %s\n", ggr->name);

  /* Count number of segments */

  int num_segments = 0;

  for (ggr_segment_t *seg = ggr->segments ; seg ; seg = seg->next)
    num_segments++;

  /* Write rest of file */

  fprintf(stream, "%d\n", num_segments);

  for (ggr_segment_t *seg = ggr->segments ; seg ; seg = seg->next)
    {
      if ( ( seg->ect_left == GGR_FIXED) &&
	   ( seg->ect_right == GGR_FIXED) )
	{
	  fprintf(stream, "%f %f %f %f %f %f %f %f %f %f %f %d %d\n",
		  seg->left, seg->middle, seg->right,
		  seg->r0, seg->g0, seg->b0, seg->a0,
		  seg->r1, seg->g1, seg->b1, seg->a1,
		  (int) seg->type, (int) seg->color);
	}
      else
	{
	  fprintf(stream, "%f %f %f %f %f %f %f %f %f %f %f %d %d %d %d\n",
		  seg->left, seg->middle, seg->right,
		  seg->r0, seg->g0, seg->b0, seg->a0,
		  seg->r1, seg->g1, seg->b1, seg->a1,
		  (int) seg->type, (int) seg->color,
		  (int) seg->ect_left, (int) seg->ect_right);
	}
    }

  return 0;
}

int ggr_write(const ggr_t *ggr, const char *path)
{
  if (path)
    {
      FILE *stream = fopen(path, "wb");

      if (stream == NULL)
        {
          btrace("failed to open %s: %s", path, strerror(errno));
          return 1;
        }

      int err = ggr_write_stream(ggr, stream);

      fclose(stream);

      return err;
    }
  else
    return ggr_write_stream(ggr, stdout);
}
