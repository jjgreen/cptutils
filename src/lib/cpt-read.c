#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/cpt-read.h"

#include "cptutils/gmtcol.h"
#include "cptutils/path-base.h"
#include "cptutils/btrace.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <ctype.h>

static int cpt_read2(const char*, cpt_t*);

cpt_t* cpt_read(const char *path)
{
  cpt_t *cpt;

  if ((cpt = cpt_new()) != NULL)
    {
      if (cpt_read2(path, cpt) == 0)
        {
          if (comment_finalise(cpt->comment) == 0)
            return cpt;
        }

      cpt_destroy(cpt);
    }

  return NULL;
}

static int cpt_parse(FILE*, cpt_t*);

static int cpt_read2(const char *path, cpt_t *cpt)
{
  int err = 1;

  if (path)
    {
      cpt->name = path_base(path, "cpt");

      FILE *stream;

      if ((stream = fopen(path, "r")) == NULL)
        btrace("error reading %s: %s", path, strerror(errno));
      else
        {
          if (cpt_parse(stream, cpt) != 0)
            btrace("error on cpt parse");
          else
            err = 0;

          fclose(stream);
	}
    }
  else
    err = cpt_parse(stdin, cpt);

  return err;
}

/*
  this modifies the input string, terminating it at the final
  block of whitespace, then returns a pointer to the point in
  the string after leading whitespace
*/

static char* trim(char *str)
{
  char *p = str + strlen(str);

  while ((p > str) && isspace((unsigned char)*(p - 1)))
    p--;

  *p = '\0';

  p = str;

  while ((*p != '\0') && isspace((unsigned char)*p))
    p++;

  return p;
}

static int cpt_parse_comment(const char*, cpt_t*);
static int cpt_parse_global(const char*, model_t, cpt_fill_t*);
static int cpt_parse_segment(const char*, cpt_t*);

#define LINELEN 2048

static int cpt_parse(FILE *stream, cpt_t *cpt)
{
  char line[LINELEN];

  for (int lineno = 1 ; fgets(line, LINELEN, stream) ; lineno++)
    {
      int err = 0;
      char *cursor = trim(line);

      switch (cursor[0])
	{
	case '\0':
	  break;
	case '#':
	  err = cpt_parse_comment(cursor + 1, cpt);
	  break;
	case 'B':
	  err = cpt_parse_global(cursor + 1, cpt->model, &(cpt->bg));
	  break;
	case 'F':
	  err = cpt_parse_global(cursor + 1, cpt->model, &(cpt->fg));
	  break;
	case 'N':
	  err = cpt_parse_global(cursor + 1, cpt->model, &(cpt->nan));
	  break;
	default:
	  err = cpt_parse_segment(cursor, cpt);
	}

      if (err)
	{
	  btrace("parse error at line %i", lineno);
	  return 1;
	}
    }

  return 0;
}

#define STR_EQ(a, b) (strcmp((a), (b)) == 0)

static int cpt_parse_colour_model(const char*, cpt_t*);
static int cpt_parse_range(const char*, cpt_t*);
static int cpt_parse_hinge(const char*, cpt_t*);
static int cpt_parse_hinge_soft(cpt_t*);
static int cpt_parse_hinge_hard(cpt_t*);

static int cpt_parse_comment(const char *line, cpt_t *cpt)
{
  char name[32], arg[32];

  errno = 0;
  switch (sscanf(line, " %31s = %31s ", name, arg))
    {
    case 0:
      break;

    case 1:
      if (STR_EQ(name, "SOFT_HINGE"))
        return cpt_parse_hinge_soft(cpt);
      if (STR_EQ(name, "HARD_HINGE"))
        return cpt_parse_hinge_hard(cpt);
      break;

    case 2:
      if (STR_EQ(name, "COLOR_MODEL"))
        return cpt_parse_colour_model(arg, cpt);
      if (STR_EQ(name, "RANGE"))
        return cpt_parse_range(arg, cpt);
      if (STR_EQ(name, "HINGE"))
        return cpt_parse_hinge(arg, cpt);
      break;

    case EOF:
      if (errno != 0)
        {
          btrace("%s", strerror(errno));
          return 1;
        }
      break;

    default:
      btrace("unexpected return from sscanf");
      return 1;
    }

  return comment_push(cpt->comment, line);
}

static int cpt_parse_colour_model(const char *str, cpt_t *cpt)
{
  if (STR_EQ(str, "RGB"))
    {
      cpt->model = model_rgb;
      cpt->interpolate = model_rgb;
    }
  else if (STR_EQ(str, "+RGB") || STR_EQ(str, "rgb"))
    {
      cpt->model = model_rgb;
      cpt->interpolate = model_rgb;
    }
  else if (STR_EQ(str, "HSV"))
    {
      cpt->model = model_hsv;
      cpt->interpolate = model_rgb;
    }
  else if (STR_EQ(str, "+HSV") || STR_EQ(str, "hsv"))
    {
      cpt->model = model_hsv;
      cpt->interpolate = model_hsv;
    }
  else if (STR_EQ(str, "CMYK"))
    {
      cpt->model = model_cmyk;
      cpt->interpolate = model_rgb;
    }
  else if (STR_EQ(str, "+CMYK") || STR_EQ(str, "cmyk"))
    {
      cpt->model = model_cmyk;
      cpt->interpolate = model_cmyk;
    }
  else
    {
      btrace("bad colour model: %s", str);
      return 1;
    }

  return 0;
}

static int cpt_parse_range(const char *str, cpt_t *cpt)
{
  double min, max;
  int n = sscanf(str, "%lf/%lf", &min, &max);

  if (n != 2)
    {
      btrace("bad RANGE: %s", str);
      return 1;
    }

  if (max <= min)
    {
      btrace("non-monotone RANGE: %s", str);
      return 1;
    }

  if (cpt->range.present)
    {
      btrace("multiple RANGEs");
      return 1;
    }

  cpt->range.present = true;
  cpt->range.min = min;
  cpt->range.max = max;

  return 0;
}

static int cpt_parse_hinge(const char *str, cpt_t *cpt)
{
  double value;
  int n = sscanf(str, "%lf", &value);

  if (n != 1)
    {
      btrace("bad HINGE: %s", str);
      return 1;
    }

  if (cpt->hinge.present)
    {
      btrace("multiple HINGEs");
      return 1;
    }

  cpt->hinge.present = true;
  cpt->hinge.type = hinge_explicit;
  cpt->hinge.value = value;

  return 0;
}

static int cpt_parse_hinge_soft(cpt_t *cpt)
{
  if (cpt->hinge.present)
    {
      btrace("multiple HINGEs");
      return 1;
    }

  cpt->hinge.present = true;
  cpt->hinge.type = hinge_soft;
  cpt->hinge.value = 0;

  return 0;
}

static int cpt_parse_hinge_hard(cpt_t *cpt)
{
  if (cpt->hinge.present)
    {
      btrace("multiple HINGEs");
      return 1;
    }

  cpt->hinge.present = true;
  cpt->hinge.type = hinge_hard;
  cpt->hinge.value = 0;

  return 0;
}

#undef STR_EQ

static int cpt_parse_1fill(const char*, model_t, cpt_fill_t*);
static int cpt_parse_3fill(const char*, const char*, const char*,
			   model_t, cpt_fill_t*);

static int cpt_parse_global(const char *line, model_t model, cpt_fill_t *fill)
{
  /* make a copy of the line */

  size_t n = strlen(line);
  char buf[n + 1];

  memcpy(buf, line, n + 1);

  /* chop up the buffer */

  int ntok = 0;
  char *tok[3];

  if ((tok[ntok] = strtok(buf, " \t")) != NULL)
    {
      for (ntok = 1 ;
	   (ntok < 3) && ((tok[ntok] = strtok(NULL, " \t")) != NULL) ;
	   ntok++);
    }

  int err = 0;

  switch (ntok)
    {
    case 1:
      err += cpt_parse_1fill(tok[0], model, fill);
      break;
    case 3:
      err += cpt_parse_3fill(tok[0], tok[1], tok[2], model, fill);
      break;
    default:
      btrace("fill with %i tokens", ntok);
      err++;
    }

  return (err ? 1 : 0);
}

static int cpt_parse_value(const char *str, double *val);
static int cpt_parse_annote(const char *str, cpt_annote_t *annote);

static int cpt_parse_segment(const char *line, cpt_t *cpt)
{
  cpt_seg_t *seg;

  if (! (seg = cpt_seg_new()))
    {
      btrace("failed to create new segment");
      return 1;
    }

  /* make a copy of the line */

  size_t n = strlen(line);
  char buf[n + 1];

  memcpy(buf, line, n + 1);

  /* see if there is a label */

  char *label;

  if ((label = strrchr(buf, ';')) != NULL)
    {
      /* trim off the label */

      *label = '\0';

      /* skip leading whitespace */

      while (label++)
	{
	  int c = *label;

	  if (!c)
	    break;

	  if (!isspace(c))
	    {
	      /* copy the label into the segment */
	      seg->label = strdup(label);
	      break;
	    }
	}
    }

  /* chop up the buffer */

  int ntok = 0;
  char *tok[9];

  if ((tok[ntok] = strtok(buf, " \t")) != NULL)
    {
      for (ntok = 1 ;
	   (ntok < 9) && ((tok[ntok] = strtok(NULL, " \t")) != NULL) ;
	   ntok++);
    }

  /* interpret the pieces */

  int err = 0;

  switch (ntok)
    {
    case 9:
      err += cpt_parse_annote(tok[8], &(seg->annote));
    case 8:
      err += cpt_parse_value(tok[0], &(seg->sample.left.val));
      err += cpt_parse_3fill(tok[1], tok[2], tok[3],
			     cpt->model, &(seg->sample.left.fill));
      err += cpt_parse_value(tok[4], &(seg->sample.right.val));
      err += cpt_parse_3fill(tok[5], tok[6], tok[7],
			     cpt->model, &(seg->sample.right.fill));
      break;

    case 5:
      err += cpt_parse_annote(tok[4], &(seg->annote));
    case 4:
      err += cpt_parse_value(tok[0], &(seg->sample.left.val));
      err += cpt_parse_1fill(tok[1], cpt->model, &(seg->sample.left.fill));
      err += cpt_parse_value(tok[2], &(seg->sample.right.val));
      err += cpt_parse_1fill(tok[3], cpt->model, &(seg->sample.right.fill));
      break;

      /*
        this 2 rather than 2, 3 since an annotation alignment in a
        categorical map wouldn't make sense, confirmed on the forum
        https://forum.generic-mapping-tools.org/t/4410
      */

    case 2:
      btrace("categorical cpt files not yet supported");
      err++;
      break;

    default:
      btrace("segment with strange number of tokens (%i)", ntok);
      err++;
    }

  if (err)
    {
      cpt_seg_destroy(seg);
      return 1;
    }

  return cpt_append(seg, cpt);
}

static int cpt_parse_value(const char *str, double *val)
{
  *val = atof(str);
  return 0;
}

static int cpt_parse_annote(const char *str, cpt_annote_t *annote)
{
  switch (str[0])
    {
    case 'L': *annote = annote_lower; break;
    case 'U': *annote = annote_upper; break;
    case 'B': *annote = annote_both;  break;
    default:
      btrace("strange annotation: %s", str);
      return 1;
    }

  return 0;
}

static int set_hsv(double, double, double, hsv_t*);
static int set_rgb(int, int, int, rgb_t*);

static int cpt_parse_1fill(const char *str, model_t model, cpt_fill_t *fill)
{
  /* empty */

  if (strcmp(str, "-") == 0)
    {
      fill->type = cpt_fill_empty;
      return 0;
    }

  /* named GMT colour */

  const struct gmtcol_t *gc;

  if ((gc = gmtcol(str)) != NULL)
    {
      if (model != model_rgb)
	{
	  btrace("RGB colour %s with non-RGB colour model", str);
	  return 1;
	}

      fill->type = cpt_fill_colour_rgb;
      fill->colour.rgb.red = gc->r;
      fill->colour.rgb.green = gc->g;
      fill->colour.rgb.blue = gc->b;

      return 0;
    }

  /* hatch */

  hatch_t hatch;

  if (sscanf(str, "p%i/%i", &(hatch.dpi), &(hatch.n)) == 2)
    {
      hatch.sign = 1;
      fill->type = cpt_fill_hatch;
      fill->hatch = hatch;
      return 0;
    }

  if (sscanf(str, "P%i/%i", &(hatch.dpi), &(hatch.n)) == 2)
    {
      hatch.sign = -1;
      fill->type = cpt_fill_hatch;
      fill->hatch = hatch;
      return 0;
    }

  /*
    handle GMT5 r/g/b or h-s-v strings, these do not need to agree
    with the colour model
  */

  {
    double rf, gf, bf;

    if (sscanf(str, "%lf/%lf/%lf", &rf, &gf, &bf) == 3)
      {
        int
          r = lround(rf),
          g = lround(gf),
          b = lround(bf);

        fill->type = cpt_fill_colour_rgb;

        return set_rgb(r, g, b, &(fill->colour.rgb));
      }
  }

  {
    double h, s, v;

    if (sscanf(str, "%lf-%lf-%lf", &h, &s, &v) == 3)
      {
        fill->type = cpt_fill_colour_hsv;

        return set_hsv(h, s, v, &(fill->colour.hsv));
      }
  }

  /* integer (greyscale) */

  {
    char *endptr;
    long val = strtol(str, &endptr, 10);

    if (endptr != str)
      {
        if ((val < 0) || (val > 255))
          {
            btrace("integer %li outside range 0,...,255", val);
            return 1;
          }

        fill->type = cpt_fill_grey;
        fill->grey = val;

        return 0;
      }
  }

  btrace("fill not recognised: %s", str);

  return 1;
}

/*
  three space-separated numbers is a GMT4 colour, one cannot
  identify the model of the colour from the values (this is
  I guess the motivation for the r/g/b and h-s-v colours of
  GMT5), so the assignment does depend on the global model
  value
*/

static int cpt_parse_3fill(const char *s1,
			   const char *s2,
			   const char *s3,
			   model_t model,
			   cpt_fill_t *fill)
{
  int err = 0;

  switch (model)
    {
    case model_rgb:
      fill->type = cpt_fill_colour_rgb;
      err += set_rgb(atoi(s1), atoi(s2), atoi(s3), &(fill->colour.rgb));
      break;
    case model_hsv:
      fill->type = cpt_fill_colour_hsv;
      err += set_hsv(atof(s1), atof(s2), atof(s3), &(fill->colour.hsv));
      break;
    default:
      btrace("bad fill type");
      err++;
    }

  return err;
}

static int set_rgb(int r, int g, int b, rgb_t *rgb)
{
  if ((r < 0) || (r > 255))
    {
      btrace("red %i out of range", r);
      return 1;
    }

  if ((g < 0) || (g > 255))
    {
      btrace("green %i out of range", g);
      return 1;
    }

  if ((b < 0) || (b > 255))
    {
      btrace("blue %i out of range", b);
      return 1;
    }

  rgb->red = r;
  rgb->green = g;
  rgb->blue = b;

  return 0;
}

static int set_hsv(double h, double s, double v, hsv_t *hsv)
{
  if ((h < 0.0) || (h > 360.0))
    {
      btrace("hue %.3f out of range", h);
      return 1;
    }

  if ((s < 0.0) || (s > 1.0))
    {
      btrace("saturation %.3f out of range", s);
      return 1;
    }

  if ((v < 0.0) || (v > 1.0))
    {
      btrace("value %.3f out of range", v);
      return 1;
    }

  hsv->hue = h;
  hsv->sat = s;
  hsv->val = v;

  return 0;
}
