#ifndef PNG_WRITE_H
#define PNG_WRITE_H

#include <cptutils/png.h>

int png_write(const png_t*, const char*, const char*);

#endif
