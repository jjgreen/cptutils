#ifndef SVG_WRITE_H
#define SVG_WRITE_H

#include <cptutils/svg.h>
#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>

int svg_write(const svg_t*,
              const char*,
              const svg_preview_t*,
              svg_version_t);

#endif
