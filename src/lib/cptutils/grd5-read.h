#ifndef GRD5_READ_H
#define GRD5_READ_H

#define GRD5_MAX_GRADIENTS 65536
#define GRD5_MAX_STOPS     65536

#include <cptutils/grd5.h>

#define GRD5_READ_OK       0
#define GRD5_READ_FOPEN    1
#define GRD5_READ_FREAD    2
#define GRD5_READ_PARSE    3
#define GRD5_READ_NOT_GRD  4
#define GRD5_READ_NOT_GRD5 5
#define GRD5_READ_MALLOC   6
#define GRD5_READ_BUG      9

grd5_t* grd5_read(const char*);

#endif
