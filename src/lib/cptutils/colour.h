#ifndef COLOUR_H
#define COLOUR_H

#include <cptutils/colour-model.h>

typedef struct rgb_t
{
  int red, green, blue;
} rgb_t;

typedef struct hsv_t
{
  double hue, sat, val;
} hsv_t;

typedef union colour_t
{
  rgb_t rgb;
  hsv_t hsv;
} colour_t;

/*
  gimp colour types rgbD and hsvD are triples of doubles
  int the range 0-1
*/

int hsvD_to_rgbD(const double[static 3], double[static 3]);
int rgbD_to_hsvD(const double[static 3], double[static 3]);

int rgbD_to_rgb(const double[static 3], rgb_t*);
int rgb_to_rgbD(rgb_t, double[static 3]);

int hsvD_to_hsv(const double[static 3], hsv_t*);
int hsv_to_hsvD(hsv_t, double[static 3]);

int grey_to_rgbD(int, double[static 3]);

int rgb_to_hsv(rgb_t, hsv_t*);
int hsv_to_rgb(hsv_t, rgb_t*);

int parse_rgb(const char*, rgb_t*);

double rgb_dist(rgb_t, rgb_t);
double hsv_dist(hsv_t, hsv_t);

int rgb_interpolate(model_t, double, rgb_t, rgb_t, rgb_t*);
int hsv_interpolate(model_t, double, hsv_t, hsv_t, hsv_t*);

#endif
