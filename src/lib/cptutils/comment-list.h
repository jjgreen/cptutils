#ifndef COMMENT_LIST_H
#define COMMENT_LIST_H

#include <cptutils/comment.h>

#include <stddef.h>

typedef struct comment_list_t comment_list_t;
typedef int (comment_list_iter_t)(comment_t*, void*);

comment_list_t* comment_list_new(void);
void comment_list_destroy(comment_list_t*);
comment_t* comment_list_next(comment_list_t*);
int comment_list_revert(comment_list_t*);
size_t comment_list_size(const comment_list_t*);
const comment_t* comment_list_entry(const comment_list_t*, size_t);
int comment_list_each(comment_list_t*, comment_list_iter_t*, void*);
comment_t* comment_list_find(comment_list_t*, comment_list_iter_t*, void*);

#endif
