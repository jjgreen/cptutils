#ifndef GPT_WRITE_H
#define GPT_WRITE_H

#include <cptutils/gpt.h>

int gpt_write(const gpt_t*, const char*);

#endif
