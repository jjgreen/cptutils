#ifndef PG_WRITE_H
#define PG_WRITE_H

#include <cptutils/pg.h>

int pg_write(const pg_t*, const char*);

#endif
