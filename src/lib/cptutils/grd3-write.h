#ifndef GRD3_WRITE_H
#define GRD3_WRITE_H

#include <cptutils/grd3.h>

int grd3_write(const grd3_t*, const char*);

#endif
