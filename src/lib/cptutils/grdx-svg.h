#ifndef GRDX_SVG_H
#define GRDX_SVG_H

#include <cptutils/gstack.h>
#include <cptutils/svg.h>

typedef struct
{
  unsigned int z;
  double r, g, b;
} rgb_stop_t;

typedef struct
{
  unsigned int z;
  double op;
} op_stop_t;

typedef struct
{
  unsigned int z;
  double r, g, b, op;
} rgbop_stop_t;

int grdxsvg(gstack_t*, gstack_t*, svg_t*);

#endif
