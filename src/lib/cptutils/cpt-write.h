#ifndef CPTWRITE_H
#define CPTWRITE_H

#include <stdbool.h>

#include <cptutils/cpt.h>

typedef enum {
  cpt_colour_join_space,
  cpt_colour_join_glyph
} cpt_colour_join_t;

typedef enum {
  cpt_model_case_upper,
  cpt_model_case_lower
} cpt_model_case_t;

typedef struct
{
  cpt_colour_join_t colour_join;
  cpt_model_case_t model_case;
  struct {
    bool hard, soft, explicit;
  } hinge;
  bool range;
} cptwrite_opt_t;

int cpt_write_options(int, cptwrite_opt_t*);
int cpt_write(const cpt_t*, const cptwrite_opt_t*, const char*);

#endif
