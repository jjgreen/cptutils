#ifndef COMMENT_WRITE_BASE_H
#define COMMENT_WRITE_BASE_H

#include <cptutils/comment.h>

#include <stddef.h>

int comment_write_base(const comment_t**, size_t, const char*);

#endif
