#ifndef SVG_LIST_WRITE_H
#define SVG_LIST_WRITE_H

#include <cptutils/svg.h>
#include <cptutils/svg-list.h>
#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>

int svg_list_write(const svg_list_t*,
                   const char*,
                   const svg_preview_t*,
                   svg_version_t);

#endif
