#ifndef QGS_LIST_READ_H
#define QGS_LIST_READ_H

#include <cptutils/qgs-list.h>

qgs_list_t* qgs_list_read(const char*);

#endif
