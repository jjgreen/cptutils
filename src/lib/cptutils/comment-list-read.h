#ifndef COMMENT_LIST_READ_H
#define COMMENT_LIST_READ_H

#include <cptutils/comment-list.h>

comment_list_t* comment_list_read(const char*);

#endif
