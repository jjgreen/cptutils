#ifndef PG_TYPE_H
#define PG_TYPE_H

#include <cptutils/gstack.h>
#include <cptutils/colour.h>

#include <stdbool.h>

struct pg_t
{
  bool percentage;
  gstack_t *stack;
};

typedef struct
{
  double value;
  rgb_t rgb;
  unsigned char alpha;
} pg_stop_t;

#endif
