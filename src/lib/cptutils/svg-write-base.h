#ifndef SVG_WRITE_BASE_H
#define SVG_WRITE_BASE_H

#include <cptutils/svg.h>
#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>

#include <stddef.h>

int svg_write_base(const svg_t**, size_t,
                   const char*,
                   const svg_preview_t*,
                   svg_version_t);

#endif
