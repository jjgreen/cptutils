#ifndef CSS3_H
#define CSS3_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>

#include <stddef.h>

typedef struct
{
  double z;
  double alpha;
  rgb_t rgb;
} css3_stop_t;

typedef struct
{
  double angle;
  size_t n;
  css3_stop_t *stop;
  comment_t *comment;
} css3_t;

css3_t* css3_new(void);
void css3_destroy(css3_t*);
int css3_stops_alloc(css3_t*, size_t);

#endif
