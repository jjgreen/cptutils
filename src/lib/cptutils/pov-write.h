#ifndef POV_WRITE_H
#define POV_WRITE_H

#include <cptutils/pov.h>

int pov_write(const pov_t*, const char*);

#endif
