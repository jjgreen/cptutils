#ifndef HASH_H
#define HASH_H

#include <stdint.h>

uint32_t hash32(const char*);

#endif
