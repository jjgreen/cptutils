#ifndef COMMENT_READ_H
#define COMMENT_READ_H

#include <cptutils/comment.h>

comment_t* comment_read(const char*);

#endif
