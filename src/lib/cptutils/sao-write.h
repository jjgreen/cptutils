#ifndef SAO_WRITE_H
#define SAO_WRITE_H

#include <cptutils/sao.h>

int sao_write(const sao_t*, const char*, const char*);

#endif
