#ifndef QGS_LIST_H
#define QGS_LIST_H

#include <cptutils/qgs.h>

#include <stddef.h>

typedef struct qgs_list_t qgs_list_t;

qgs_list_t* qgs_list_new(void);
void qgs_list_destroy(qgs_list_t*);
qgs_t* qgs_list_next(qgs_list_t*);
int qgs_list_revert(qgs_list_t*);
size_t qgs_list_size(const qgs_list_t*);
const qgs_t* qgs_list_entry(const qgs_list_t*, size_t);
int qgs_list_each(qgs_list_t*, int (*)(qgs_t*, void*), void*);
qgs_t* qgs_list_find(qgs_list_t*, int (*)(qgs_t*, void*), void*);

#endif
