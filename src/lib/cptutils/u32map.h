#ifndef U32MAP_H
#define U32MAP_H

#include <stddef.h>
#include <stdint.h>

typedef struct u32map_t u32map_t;

u32map_t* u32map_new(size_t);
void u32map_destroy(u32map_t*);

size_t u32map_size(const u32map_t*);
size_t u32map_collisions(const u32map_t*);
size_t u32map_capacity(const u32map_t*);

int u32map_put(u32map_t*, const char*, uint32_t);
int u32map_get(const u32map_t*, const char*, uint32_t*);

typedef int (u32map_each_t)(const char*, uint32_t, void*);
int u32map_each(const u32map_t*, u32map_each_t*, void*);

typedef int (u32map_update_t)(uint32_t, uint32_t*, void*);
int u32map_update(const u32map_t*, const char*, u32map_update_t*, void*);

#endif
