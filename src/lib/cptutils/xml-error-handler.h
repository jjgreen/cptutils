#ifndef XML_ERROR_HANDLER_H
#define XML_ERROR_HANDLER_H

#include <libxml/xmlerror.h>
#include <libxml/xmlversion.h>

#if LIBXML_VERSION < 21200
void xml_error_handler(void*, xmlError*);
#else
void xml_error_handler(void*, const xmlError*);
#endif

#endif
