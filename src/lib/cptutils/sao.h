#ifndef SAO_H
#define SAO_H

#include <cptutils/comment.h>

typedef struct sao_t sao_t;

sao_t* sao_new(void);
void sao_destroy(sao_t*);

comment_t* sao_comment(const sao_t*);

int sao_red_push(sao_t*, double, double);
int sao_blue_push(sao_t*, double, double);
int sao_green_push(sao_t*, double, double);

typedef int (stop_fn_t)(double, double, void*);

int sao_eachred(const sao_t*, stop_fn_t*, void*);
int sao_eachgreen(const sao_t*, stop_fn_t*, void*);
int sao_eachblue(const sao_t*, stop_fn_t*, void*);

#endif
