#ifndef COMMENT_LIST_WRITE_H
#define COMMENT_LIST_WRITE_H

#include <cptutils/comment-list.h>

int comment_list_write(const comment_list_t*, const char*);

#endif
