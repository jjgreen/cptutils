#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/colour-model.h"

#include <stddef.h>

const char* model_name(model_t model)
{
  const char *name = NULL;

  switch (model)
    {
    case model_rgb: name = "RGB"; break;
    case model_hsv: name = "HSV"; break;
    case model_cmyk: name = "CMYK"; break;
    }

  return name;
}
