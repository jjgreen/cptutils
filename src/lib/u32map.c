#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/u32map.h"
#include "cptutils/hash.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

typedef struct entry_t entry_t;

struct entry_t
{
  const char *key;
  uint32_t hash, value;
  entry_t *next;
};

struct u32map_t
{
  entry_t **buckets;
  size_t count, size;
};

u32map_t* u32map_new(size_t initial_size)
{
  u32map_t *map;

  if ((map = malloc(sizeof(u32map_t))) != NULL)
    {
      size_t
        count = 1,
        min_count = initial_size * 4 / 3;

      while (count <= min_count)
        count <<= 1;

      entry_t **buckets;

      if ((buckets = calloc(count, sizeof(entry_t*))) != NULL)
        {
          map->count = count;
          map->size = 0;
          map->buckets = buckets;

          return map;
        }

      free(map);
    }

  return NULL;
}

void u32map_destroy(u32map_t *map)
{
  if (map)
    {
      for (size_t i = 0 ; i < map->count ; i++)
        {
          entry_t *entry = map->buckets[i];

          while (entry != NULL)
            {
              entry_t *next = entry->next;
              free(entry);
              entry = next;
            }
        }
      free(map->buckets);
    }

  free(map);
}

size_t u32map_size(const u32map_t *map)
{
  return map->size;
}

static inline size_t index_of_hash(size_t count, uint32_t hash)
{
  return ((size_t) hash) & (count - 1);
}

static inline bool keys_equal(const char *keyA, const char *keyB)
{
  return strcmp(keyA, keyB) == 0;
}

static entry_t* entry_new(const char *key, uint32_t hash, uint32_t value)
{
  entry_t *entry;

  if ((entry = malloc(sizeof(entry_t))) == NULL)
    return NULL;

  entry->key = key;
  entry->hash = hash;
  entry->value = value;
  entry->next = NULL;

  return entry;
}

static void u32map_conditional_expand(u32map_t *map)
{
  if (map->size <= u32map_capacity(map))
    return;

  size_t count_new = map->count << 1;
  entry_t **buckets_new;

  if ((buckets_new = calloc(count_new, sizeof(entry_t*))) == NULL)
    {
      errno = ENOMEM;
      return;
    }

  for (size_t i = 0 ; i < map->count ; i++)
    {
      entry_t *entry = map->buckets[i];

      while (entry != NULL)
        {
          entry_t *next = entry->next;
          size_t index = index_of_hash(count_new, entry->hash);

          entry->next = buckets_new[index];
          buckets_new[index] = entry;
          entry = next;
        }
    }

  free(map->buckets);
  map->buckets = buckets_new;
  map->count = count_new;
}

int u32map_put(u32map_t *map, const char *key, uint32_t value)
{
  uint32_t hash = hash32(key);
  size_t index = index_of_hash(map->count, hash);
  entry_t **p = &(map->buckets[index]);

  while (true)
    {
      entry_t *current = *p;

      if (current == NULL)
        {
          *p = entry_new(key, hash, value);

          if (*p == NULL)
            {
              errno = ENOMEM;
              return 1;
            }

          map->size++;
          u32map_conditional_expand(map);

          return 0;
        }

      if (keys_equal(current->key, key))
        {
          current->value = value;
          return 0;
        }

      p = &(current->next);
    }
}

int u32map_get(const u32map_t *map, const char *key, uint32_t *value)
{
  uint32_t hash = hash32(key);
  size_t index = index_of_hash(map->count, hash);

  for (entry_t *entry = map->buckets[index] ; entry ; entry = entry->next)
    {
      if (keys_equal(entry->key, key))
        {
          *value = entry->value;
          return 0;
        }
    }

  return 1;
}

size_t u32map_collisions(const u32map_t *map)
{
  size_t collisions = 0;

  for (size_t i = 0 ; i < map->count ; i++)
    {
      entry_t *entry = map->buckets[i];
      while (entry != NULL)
        {
          if (entry->next != NULL)
            collisions++;
          entry = entry->next;
        }
    }

  return collisions;
}

size_t u32map_capacity(const u32map_t *map)
{
  return map->count * 3 / 4;
}

int u32map_each(const u32map_t *map, u32map_each_t *f, void *arg)
{
  for (size_t i = 0 ; i < map->count ; i++)
    {
      entry_t *entry = map->buckets[i];

      while (entry != NULL)
        {
          int result;

          if ((result = f(entry->key, entry->value, arg)) != 0)
            return result;

          entry = entry->next;
        }
    }

  return 0;
}

/*
  get the value in the map with the specified key, pass that as the
  first argument to f which should assign the second argument. If f
  returns 0, that second argument will be assigned to the value in
  the map.  If f is non-zero then the value is not assigned.  The
  return value of f is the return value of this function.  The void*
  argument passed to this function is passed to f as context, in the
  usual fashion.  If the key is not found, returns -1, so one should
  use positive error return values in f.
*/

int u32map_update(const u32map_t *map,
                  const char *key,
                  u32map_update_t *f,
                  void *arg)
{
  uint32_t hash = hash32(key);
  size_t index = index_of_hash(map->count, hash);

  for (entry_t *entry = map->buckets[index] ; entry ; entry = entry->next)
    {
      if (keys_equal(entry->key, key))
        {
          uint32_t value;
          int err =  f(entry->value, &value, arg);

          if (err == 0)
            entry->value = value;

          return err;
        }
    }

  return -1;
}
