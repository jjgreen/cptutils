#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/cpt-normalise.h"
#include "cptutils/btrace.h"

#include <assert.h>
#include <math.h>

#define HINGE_ERR_MAX 1e-8

#define ZL(seg) seg->sample.left.val
#define AFFINE_LEFT(seg, A, B) ZL(seg) = (A * ZL(seg) + B)

#define ZR(seg) seg->sample.right.val
#define AFFINE_RIGHT(seg, A, B) ZR(seg) = (A * ZR(seg) + B)

static int handle_hinge(cpt_t *cpt,
                        bool hinge_active,
                        double hinge_value,
                        int (*f_hinged)(cpt_t*, double),
                        int (*f_unhinged)(cpt_t*))
{
  if (cpt->hinge.present)
    {
      switch (cpt->hinge.type)
        {
        case hinge_explicit:
          if (cpt->hinge.value == 0)
            return f_hinged(cpt, hinge_value);
          else
            {
              /* https://gitlab.com/jjg/cptutils/-/issues/104 */
              btrace("cannot (de)normalise GMT5 HINGE = %g files at present",
                     cpt->hinge.value);
              return 1;
            }
        case hinge_hard:
          return f_hinged(cpt, hinge_value);
        case hinge_soft:
          if (hinge_active)
            return f_hinged(cpt, hinge_value);
          else
            return f_unhinged(cpt);
        default:
          assert(false);
        }
    }
  else
    return f_unhinged(cpt);
}

/* [min, hinge] [hinge, max] -> [-1, 0] [0, 1] */

static int normalise_hinged(cpt_t *cpt, double hinge)
{
  double range[2];

  if (cpt_range_implicit(cpt, range) != 0)
    {
      btrace("getting cpt implicit z-range");
      return 1;
    }

  double
    min = range[0],
    max = range[1];

  if (min >= max)
    {
      btrace("bad z-range %lf/%lf", min, max);
      return 1;
    }

  if (! ((min < hinge) && (hinge < max)))
    {
      btrace("hinge %lf not in z-range %lf/%lf", hinge, min, max);
      return 1;
    }

  cpt_seg_t *seg = cpt->segment;

  {
    double
      A = 1 / (hinge - min),
      B = -A * hinge;

    while (seg && cpt_seg_z_mid(seg) < hinge)
      {
        AFFINE_LEFT(seg, A, B);
        AFFINE_RIGHT(seg, A, B);
        seg = seg->right;
      }
  }

  if (seg == NULL)
    {
      btrace("no zero in input");
      return 1;
    }

  if (fabs(ZL(seg) - hinge) > HINGE_ERR_MAX)
    {
      btrace("hinge %g is not an input stop", hinge);
      return 1;
    }

  {
    double
      A = 1 / (max - hinge),
      B = -A * hinge;

    while (seg)
      {
        AFFINE_LEFT(seg, A, B);
        AFFINE_RIGHT(seg, A, B);
        seg = seg->right;
      }
  }

  if (! cpt->range.present)
    {
      cpt->range.present = true;
      cpt->range.min = min;
      cpt->range.max = max;
    }

  return 0;
}

/* [min, max] -> ]0, 1] */

static int normalise_unhinged(cpt_t *cpt)
{
  double range[2];

  if (cpt_range_implicit(cpt, range) != 0)
    {
      btrace("getting cpt implicit z-range");
      return 1;
    }

  double
    min = range[0],
    max = range[1];

  if (min >= max)
    {
      btrace("bad z-range %f/%f", min, max);
      return 1;
    }

  double
    A = 1 / (max - min),
    B = -A * min;

  for (cpt_seg_t *seg = cpt->segment ; seg ; seg = seg->right)
    {
      AFFINE_LEFT(seg, A, B);
      AFFINE_RIGHT(seg, A, B);
    }

  /*
    if there is no explicit range present, then the input may be a
    gmt4 file which cannot have a range, or a gmt5 file which just
    so happens to not have a range, in either case we set the range
    to present and values to the min/max of input z-values.  If
    there is a range present, then the input must be gmt5, and we
    don't touch those values.
  */

  if (! cpt->range.present)
    {
      cpt->range.present = true;
      cpt->range.min = min;
      cpt->range.max = max;
    }

  return 0;
}

/*
  the hinge-active argument here comes from the command-line,
  it acts to enable a soft hinge (if present).
*/

int cpt_normalise(cpt_t *cpt, bool hinge_active, double hinge_value)
{
  return
    handle_hinge(cpt,
                 hinge_active,
                 hinge_value,
                 normalise_hinged,
                 normalise_unhinged);
}

/*
  denormalisation shifts and scales the input gradient to that given
  by the explicit RANGE, then renoves it.  if there is no RANGE then
  this is a no-op.  note that this function does not assume that the
  input is normalised (i.e., that the z-range is [0, 1]).
*/

/* [imin, 0] [0, imax] -> [emin, hinge] [hinge, max] */

static int denormalise_hinged(cpt_t *cpt, double hinge)
{
  if (! cpt->range.present)
    return 0;

  double erange[2], irange[2];

  if ((cpt_range_explicit(cpt, erange) != 0) ||
      (cpt_range_implicit(cpt, irange) != 0))
    return 1;

  double
    emin = erange[0],
    emax = erange[1],
    imin = irange[0],
    imax = irange[1];

  if (! ((emin < hinge) && (hinge < emax)))
    {
      btrace("hinge %lf not in z-range %lf/%lf", hinge, emin, emax);
      return 1;
    }

  cpt_seg_t *seg = cpt->segment;

  {
    double
      A = (emin - hinge) / imin,
      B = hinge;

    while (seg && cpt_seg_z_mid(seg) < 0)
      {
        AFFINE_LEFT(seg, A, B);
        AFFINE_RIGHT(seg, A, B);
        seg = seg->right;
      }
  }

  if (seg == NULL)
    {
      btrace("no zero in input");
      return 1;
    }

  if (fabs(ZL(seg)) > HINGE_ERR_MAX)
    {
      btrace("hinge %g is not an input stop", hinge);
      return 1;
    }

  {
    double
      A = (emax - hinge) / imax,
      B = hinge;

    while (seg)
      {
        AFFINE_LEFT(seg, A, B);
        AFFINE_RIGHT(seg, A, B);
        seg = seg->right;
      }
  }

  cpt->range.present = false;

  return 0;
}

/* [imin, imax] -> [emin, emax] */

static int denormalise_unhinged(cpt_t *cpt)
{
  if (! cpt->range.present)
    return 0;

  double erange[2], irange[2];

  if ((cpt_range_explicit(cpt, erange) != 0) ||
      (cpt_range_implicit(cpt, irange) != 0))
    return 1;

  double
    emin = erange[0],
    emax = erange[1],
    imin = irange[0],
    imax = irange[1];

  double
    A = (emax - emin) / (imax - imin),
    B = emin - imin * A;

  for (cpt_seg_t *seg = cpt->segment ; seg ; seg = seg->right)
    {
      AFFINE_LEFT(seg, A, B);
      AFFINE_RIGHT(seg, A, B);
    }

  cpt->range.present = false;

  return 0;
}

int cpt_denormalise(cpt_t *cpt, bool hinge_active, double hinge_value)
{
  return
    handle_hinge(cpt,
                 hinge_active,
                 hinge_value,
                 denormalise_hinged,
                 denormalise_unhinged);
}
