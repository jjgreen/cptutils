#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef VERSION
#define VERSION "unknown"
#endif

#include "cptutils/tpm-write.h"
#include "cptutils/btrace.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

typedef struct {
  int n;
  FILE *stream;
} write_point_context_t;

/* a gstack_foreach callback function */

static int write_point(void *vpoint, void *vcontext)
{
  tpm_point_t *point = vpoint;
  write_point_context_t *context = vcontext;
  const char fmt[] =
    "    CONTROLPOINT %i\n"
    "      {\n"
    "      COLORMAPFRACTION = %.6f\n"
    "      LEADRGB\n"
    "        {\n"
    "          R = %i\n"
    "          G = %i\n"
    "          B = %i\n"
    "        }\n"
    "      TRAILRGB\n"
    "        {\n"
    "          R = %i\n"
    "          G = %i\n"
    "          B = %i\n"
    "        }\n"
    "      }\n";

  fprintf(context->stream, fmt,
          context->n,
          point->fraction,
          point->lead.red,
          point->lead.green,
          point->lead.blue,
          point->trail.red,
          point->trail.green,
          point->trail.blue);

  context->n += 1;

  return 1;
}

static int write_comment(const char *line, void *arg)
{
  FILE *stream = arg;

  if (line)
    fprintf(stream, "# %s\n", line);
  else
    fprintf(stream, "#\n");

  return 0;
}

static int write_stream(const tpm_t *tpm, FILE *stream)
{
  const char
    fmt_shbang[] =
    "#!MC 1410\n",
    fmt_header[] =
    "$!COLORMAP\n"
    "  CONTOURCOLORMAP = UserDef\n"
    "$!COLORMAPCONTROL RESETTOFACTORY\n"
    "$!COLORMAP\n"
    "  USERDEFINED\n"
    "    {\n"
    "    NUMCONTROLPOINTS = %zu\n",
    fmt_footer[] =
    "    }\n";

  fprintf(stream, fmt_shbang);

  if (comment_each(tpm_comment(tpm), write_comment, stream) != 0)
    {
      btrace("failed to write comment");
      return 1;
    }

  fprintf(stream, fmt_header, tpm_npoint(tpm));

  write_point_context_t context = { .n = 1, .stream = stream };
  int err = tpm_each_point(tpm, write_point, &context);

  fprintf(stream, fmt_footer);

  return err;
}

int tpm_write(const tpm_t *tpm, const char *path)
{
  int err = 1;

  if (path)
    {
      FILE *stream;

      if ((stream = fopen(path, "w")) != NULL)
        {
          err = write_stream(tpm, stream);
          fclose(stream);
        }
      else
        btrace("failed to open %s (%s)", path, strerror(errno));
    }
  else
    err = write_stream(tpm, stdout);

  return err;
}
