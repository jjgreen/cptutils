#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/btrace.h"
#include "cptutils/gpt.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

gpt_t* gpt_new(void)
{
  gpt_t *gpt;

  if ((gpt = malloc(sizeof(gpt_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = comment_new()) != NULL)
        {
          gpt->n = 0;
          gpt->stop = NULL;
          gpt->comment = comment;

          return gpt;
        }

      free(gpt);
    }

   return NULL;
}

/* this can only be done once */

int gpt_stops_alloc(gpt_t *gpt, size_t n)
{
  gpt_stop_t *stop;

  if (n < 1)
    {
      btrace("bad number of gpt stops (%i)", n);
      return 1;
    }

  if (gpt->n > 0)
    {
      btrace("stops already allocated");
      return 1;
    }

  if ((stop = calloc(n, sizeof(gpt_stop_t))) == NULL)
    return 1;

  gpt->n = n;
  gpt->stop = stop;

  return 0;
}

void gpt_destroy(gpt_t *gpt)
{
  if (gpt != NULL)
    {
      free(gpt->stop);
      comment_destroy(gpt->comment);
    }

  free(gpt);
}
