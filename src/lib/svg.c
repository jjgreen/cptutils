#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/svg.h"
#include "cptutils/xml-id.h"
#include "cptutils/btrace.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/*
  modifies the first argument, replacing transparency
  with the specified rgb value
*/

static int svg_flatten_stop(svg_stop_t *stop, rgb_t *rgb)
{
  double op = stop->opacity;

  if (op < 1)
    {
      if (rgb_interpolate(model_rgb, op,
                          *rgb, stop->colour, &(stop->colour)) != 0)
	{
	  btrace("failed RGB interpolate");
	  return 1;
	}
      stop->opacity = 1.0;
    }

  return 0;
}

int svg_flatten(svg_t *svg, rgb_t rgb)
{
  return
    svg_each_stop(svg, (int (*)(svg_stop_t*, void*))svg_flatten_stop, &rgb);
}

/*
  modifies its argument in making explicit the initial and final
  implicit stops
*/

int svg_explicit(svg_t *svg)
{
  svg_node_t *node = svg->nodes;

  if (node == NULL)
    {
      btrace("gradient has no stops");
      return 1;
    }

  if (node->stop.value > 0.0)
    {
      svg_stop_t stop = node->stop;

      stop.value = 0.0;

      if (svg_prepend(stop, svg) != 0) return 1;
    }

  for ( ; node->r ; node = node->r ) ;

  if (node->stop.value < 100.0)
    {
      svg_stop_t stop = node->stop;

      stop.value = 100.0;

      if (svg_append(stop, svg) != 0) return 1;
    }

  return 0;
}

static int svg_complete_name(const unsigned char *id, unsigned char *name)
{
  size_t n = strlen((const char*)id);

  if (n > 3)
    {
      if (strncmp((const char*)id, "id-", 3) == 0)
        memcpy(name, id + 3, n - 2);
      else
        memcpy(name, id, n + 1);
    }
  else
    memcpy(name, id, n + 1);

  return 0;
}

static int svg_complete_id(const unsigned char *name, unsigned char *id)
{
  char *id2 = xml_id_sanitise((const char*)name);

  if (id2 != NULL)
    {
      size_t n = strlen(id2);
      memcpy(id, id2, n + 1);
      free(id2);

      return 0;
    }

  return 1;
}

char* svg_basename(const svg_t *svg)
{
  const char *id = (char*)svg->id, *basename = id;
  size_t n = strlen(id);

  if ((n > 3) && (strncmp(id, "id-", 3) == 0))
    basename = id + 3;

  return strdup(basename);
}

/*
  modifies the argument, if the id is present but the name is
  not, copies the id excepting an 'id-' prefix if present; if
  the name is present but the id not, slugifies the name and
  prefixes with 'id-' to create the id.
*/

int svg_complete(svg_t *svg)
{
  if (svg->id[0] == '\0')
    {
      if (svg->name[0] == '\0')
        {
          btrace("both id and name absent");
          return 1;
        }

      return svg_complete_id(svg->name, svg->id);
    }
  else
    {
      if (svg->name[0] == '\0')
        return svg_complete_name(svg->id, svg->name);

      return 0;
    }
}

/*
  this operates on each stop : if f returns
  0 - continue
  1 - short circuit success
  x - short circuit failure, return x

  not tested
*/

int svg_each_stop(const svg_t *svg,
                  int (*f)(svg_stop_t*, void*),
                  void *opt)
{
  svg_node_t *node;

  for (node = svg->nodes ; node ; node = node->r)
    {
      int err = f(&(node->stop), opt);

      switch (err)
	{
	case 0:
          break;
	case 1:
          return 0;
	default:
          return err;
	}
    }

  return 0;
}

/*
  interpolate the colour and opacity values of the svg gradient
  at the specified z value.
*/

static int svg_interpolate_stops(svg_stop_t ls,
				 svg_stop_t rs,
				 double z,
				 rgb_t *rgb,
				 double *op)
{
  if ((rs.value - ls.value) <= 0)
    {
      btrace("non-increasing stops");
      return 1;
    }

  double t = (z - ls.value) / (rs.value - ls.value);

  *op = ls.opacity * (1 - t) + rs.opacity * t;

  return rgb_interpolate(model_rgb, t, ls.colour, rs.colour, rgb);
}

/* interpolate an svg with explicit initial and final stops */

int svg_interpolate(const svg_t *svg, double z, rgb_t *rgb, double *op)
{
  if ((z < 0.0) || (z > 100.0))
    {
      btrace("z out of range: %f", z);
      return 1;
    }

  svg_node_t *node;

  /* find first node where node.z => z */

  for (node = svg->nodes ;
       node && (node->stop.value < z) ;
       node = node->r);

  /*
    this will happen if the initial stop is implicit
  */

  if (!node)
    {
      btrace("implicit initial stop");
      return 1;
    }

  /*
    if there is a leftward node then the z-value of
    it is less than z, so we can inrerploate.
    if there is no leftward node then we are at the
    first node, which should have z-value zero
  */

  if (node->l)
    {
      return svg_interpolate_stops(node->l->stop,
				   node->stop,
				   z, rgb, op);
    }
  else
    {
      if (node->stop.value <= z)
	{
	  *rgb = node->stop.colour;
	  *op = node->stop.opacity;

	  return 0;
	}
    }

  /* this will happen if the final stop is implicit */

  btrace("implicit final stop");

  return 1;
}

svg_t* svg_new(void)
{
  svg_t *svg;

  if ((svg = malloc(sizeof(svg_t))) != NULL)
    {
      if (svg_init(svg) == 0)
        return svg;

      free(svg);
    }

  return NULL;
}

int svg_init(svg_t *svg)
{
  svg->nodes = NULL;
  svg->name[0] = svg->id[0] = '\0';

  if ((svg->comment = comment_new()) == NULL)
    return 1;

  return 0;
}

static svg_node_t* svg_node_new(svg_stop_t stop)
{
  svg_node_t *n;

  if ((n = malloc(sizeof(svg_node_t))) == NULL)
    return NULL;

  n->stop = stop;

  return n;
}

static void svg_node_destroy(svg_node_t *node)
{
  free(node);
}

int svg_prepend(svg_stop_t stop, svg_t *svg)
{
  svg_node_t *node, *root;

  root = svg->nodes;

  if ((node = svg_node_new(stop)) == NULL) return 1;

  node->l = NULL;
  node->r = root;

  if (root) root->l = node;

  svg->nodes = node;

  return 0;
}

int svg_append(svg_stop_t stop, svg_t *svg)
{
  svg_node_t *node, *n;

  n = svg->nodes;

  if (!n)
    return svg_prepend(stop, svg);

  while (n->r)
    n = n->r;

  if ((node = svg_node_new(stop)) == NULL)
    return 1;

  n->r = node;
  node->l = n;
  node->r = NULL;

  return 0;
}

void svg_deinit(svg_t *svg)
{
  svg_node_t *node = svg->nodes;

  while (node)
    {
      svg_node_t *next = node->r;
      svg_node_destroy(node);
      node = next;
    }

  comment_destroy(svg->comment);
}

void svg_destroy(svg_t *svg)
{
  if (svg != NULL)
    {
      svg_deinit(svg);
      free(svg);
    }
}

static int inc(svg_stop_t *stop, int *n)
{
  (*n)++;

  return 0;
}

int svg_num_stops(const svg_t *svg)
{
  int n = 0;

  if (svg_each_stop(svg, (int (*)(svg_stop_t*, void*))inc, &n) != 0)
    return -1;

  return n;
}
