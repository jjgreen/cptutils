#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/svg-list.h"
#include "cptutils/u32map.h"
#include "cptutils/btrace.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct svg_list_t
{
  size_t n, alloc;
  svg_t *svg;
};

#define LIST_INI 20
#define LIST_INC 20

svg_list_t* svg_list_new(void)
{
  svg_list_t *list;

  if ((list = calloc(1, sizeof(svg_list_t))) != NULL)
    {
      svg_t *svg;

      if ((svg = calloc(LIST_INI, sizeof(svg_t))) != NULL)
	{
	  list->svg = svg;
	  list->alloc = LIST_INI;
	  list->n = 0;

	  return list;
	}

      free(list);
    }

  return NULL;
}

void svg_list_destroy(svg_list_t *list)
{
  if (list != NULL)
    {
      for (size_t i = 0 ; i < list->n ; i++)
        svg_deinit(list->svg + i);
      free(list->svg);
    }
  free(list);
}

int svg_list_each(svg_list_t *list,
                  int (*f)(svg_t*, void*),
                  void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      int err;
      svg_t *svg = list->svg + i;

      if ((err = f(svg, opt)) != 0)
        return err;
    }

  return 0;
}

svg_t* svg_list_find(svg_list_t *list,
                     int (*f)(svg_t*, void*),
                     void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      svg_t *svg = list->svg + i;

      if (f(svg, opt) != 0)
        return svg;
    }

  return NULL;
}

svg_t* svg_list_next(svg_list_t *list)
{
  size_t
    n = list->n,
    a = list->alloc;

  if (n < a)
    {
      list->n++;

      svg_t *svg = list->svg + n;

      if (svg_init(svg) == 0)
        return svg;
      else
        return NULL;
    }
  else
    {
      a += LIST_INC;

      svg_t *svg;

      if ((svg = realloc(list->svg, sizeof(svg_t) * a)) != NULL)
        {
          list->alloc = a;
          list->svg = svg;

          return svg_list_next(list);
        }
      else
        return NULL;
    }
}

int svg_list_revert(svg_list_t *list)
{
  size_t n = list->n;

  if (n == 0)
    return 1;

  svg_t *svg = &(list->svg[n - 1]);
  svg_deinit(svg);
  list->n--;

  return 0;
}

size_t svg_list_size(const svg_list_t *list)
{
  return list->n;
}

const svg_t* svg_list_entry(const svg_list_t *list, size_t i)
{
  if (i >= list->n)
    return NULL;
  return list->svg + i;
}

typedef struct
{
  size_t i;
  char **ids;
} id_list_t;

static int f_get_ids(svg_t *svg, void *arg)
{
  id_list_t *id_list = arg;
  id_list->ids[id_list->i++] = (char*)svg->id;
  return 0;
}

static int f_inc(uint32_t v1, uint32_t *v2, void *arg)
{
  *v2 = v1 + 1;

  if (arg != NULL)
    {
      uint32_t *v = arg;
      *v = *v2;
    }

  return 0;
}

int svg_list_coerce_ids(svg_list_t *list)
{
  size_t n = svg_list_size(list);

  if (n < 2)
    return 0;

  char* ids[n];
  id_list_t id_list = { .i = 0, .ids = ids };

  if (svg_list_each(list, f_get_ids, &id_list) != 0)
    return 1;

  /* first get the multiplicity of ids */

  bool multiple[n];

  {
    u32map_t *map = u32map_new(n);

    if (map == NULL)
      return 1;

    for (size_t i = 0 ; i < n ; i++)
      if (u32map_put(map, ids[i], 0) != 0)
        return 1;

    for (size_t i = 0 ; i < n ; i++)
      if (u32map_update(map, ids[i], f_inc, NULL) != 0)
        return 1;

    for (size_t i = 0 ; i < n ; i++)
      {
        uint32_t count;

        if (u32map_get(map, ids[i], &count) != 0)
          return 1;

        multiple[i] = count > 1;
      }

    u32map_destroy(map);
  }

  /* count multiples */

  size_t m = 0;

  for (size_t i = 0 ; i < n ; i++)
    if (multiple[i])
      m++;

  if (m == 0)
    return 0;

  /* create the list of suffixes (zero for no suffix) */

  uint32_t suffix[n];

  {
    u32map_t *map = u32map_new(m);

    if (map == NULL)
      return 1;

    for (size_t i = 0 ; i < n ; i++)
      if (multiple[i] && (u32map_put(map, ids[i], 0) != 0))
        return 1;

    for (size_t i = 0 ; i < n ; i++)
      {
        if (multiple[i])
          {
            if (u32map_update(map, ids[i], f_inc, suffix + i) != 0)
              return 0;
          }
        else
          suffix[i] = 0;
      }

    u32map_destroy(map);
  }

  /* add the suffix to those ids where it is positive */

  for (size_t i = 0 ; i < n ; i++)
    {
      if (suffix[i] > 0)
        {
          char id_new[SVG_NAME_LEN];
          int k = snprintf(id_new, SVG_NAME_LEN, "%s-%u", ids[i], suffix[i]);

          if (k >= SVG_NAME_LEN)
            {
              btrace("suffixed name too long");
              return 1;
            }

          strcpy(ids[i], id_new);
        }
    }

  return 0;
}
