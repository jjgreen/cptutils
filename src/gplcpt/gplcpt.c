#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gplcpt.h"

#include <cptutils/cpt-write.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/btrace.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

static int gplcpt_st(const gplcpt_opt_t*, cpt_t*, FILE*);

int gplcpt(const gplcpt_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_new()) != NULL)
    {
      if (comment_process(cpt->comment, &(opt->comment)) == 0)
        {
          if (opt->input.path)
            {
              FILE *st;

              if ((st = fopen(opt->input.path, "r")) == NULL)
                btrace("failed to open %s", opt->input.path);
              else
                {
                  err = gplcpt_st(opt, cpt, st);
                  fclose(st);
                }
            }
          else
            err = gplcpt_st(opt, cpt, stdin);
        }
      else
        btrace("failed to process comments");

      cpt_destroy(cpt);
    }

  return err;
}

#define BUFSIZE 1024

static int gplcpt_write(const gplcpt_opt_t*, cpt_t*);
static int skipline(const char*);

int gplcpt_st(const gplcpt_opt_t *opt, cpt_t *cpt, FILE *st)
{
  char buf[BUFSIZE];

  /* get first line */

  if (fgets(buf, BUFSIZE, st) == NULL)
    {
      btrace("no first data line");
      return 1;
    }

  /* check it is a gimp palette */

  if (strncmp(buf, "GIMP Palette", 12) != 0)
    {
      btrace("file does not seem to be a GIMP palette");
      return 1;
    }

  /* setup cpt struct */

  cpt->model = model_rgb;
  cpt->fg.type = cpt->bg.type = cpt->nan.type = cpt_fill_colour_rgb;
  cpt->bg.colour.rgb = opt->bg;
  cpt->fg.colour.rgb = opt->fg;
  cpt->nan.colour.rgb = opt->nan;

  /* get next non-comment line */

  do
    if (fgets(buf, BUFSIZE, st) == NULL) return 1;
  while (skipline(buf));

  /* see if it is a name line */

  char name[BUFSIZE];

  if (sscanf(buf, "Name: %1023s", name) == 1)
    {
      if (opt->verbose)
	{
	  printf("GIMP palette %s\n", name);
	}

      cpt->name = strdup(name);

      /* skip to next non-comment */

      do
	if (fgets(buf, BUFSIZE, st) == NULL) return 1;
      while (skipline(buf));
    }

  /* see if it is columns line */

  int columns;

  if (sscanf(buf, "Columns: %i", &columns) == 1)
    {
      if (opt->verbose)
	{
	  printf("%i columns\n", columns);
	}

      /* skip to next non-comment */

      do
	if (fgets(buf, BUFSIZE, st) == NULL) return 1;
      while (skipline(buf));
    }

  /* now at the rgb data we hope */

  cpt_fill_t fill;
  fill.type = cpt_fill_colour_rgb;

  size_t n = 0;

  while (1)
    {
      int r, g, b;

      if (sscanf(buf, "%i %i %i", &r, &g, &b) != 3)
	{
	  btrace("bad line %s", buf);
	  return 1;
	}

      fill.colour.rgb.red = r;
      fill.colour.rgb.green = g;
      fill.colour.rgb.blue = b;

      cpt_seg_t *seg;

      if ((seg = cpt_seg_new()) == NULL)
	{
	  btrace("failed to create new segment");
	  return 1;
	}

      seg->sample.left.val = n;
      seg->sample.left.fill = fill;

      seg->sample.right.val = n + 1;
      seg->sample.right.fill = fill;

      if (cpt_append(seg, cpt) != 0)
	{
	  btrace("failed append of segment %i", (int)n);
	  return 1;
	}

      n++;

      do
	if (fgets(buf, BUFSIZE, st) == NULL)
	  {
	    if (opt->verbose)
              printf("read %zu RGB triples\n", n);

	    return gplcpt_write(opt, cpt);
	  }
      while (skipline(buf));
    }

  return 1;
}

static int cpt_step_optimise(cpt_t*);

static int gplcpt_write(const gplcpt_opt_t *opt, cpt_t *cpt)
{
  if (cpt_step_optimise(cpt) != 0)
    {
      btrace("failed optimise");
      return 1;
    }

  if (opt->verbose)
    printf("converted to %i segment cpt\n", cpt_nseg(cpt));

  cptwrite_opt_t write_opt;

  if (cpt_write_options(opt->gmt_version, &write_opt) != 0)
    {
      btrace("bad GMT version %i", opt->gmt_version);
      return 1;
    }

  if (opt->normalise)
    {
      if (cpt_normalise(cpt, false, 0) != 0)
        {
          btrace("normalising");
          return 1;
        }
    }

  return cpt_write(cpt, &write_opt, opt->output.path);
}

/*
  aggregates segments which have the same fill, assuming that
  the input consists of piecewise constant segments (as here)
*/

static int cpt_step_optimise(cpt_t *cpt)
{
  cpt_seg_t *left, *right;

  left = cpt->segment;
  right = left->right;

  while (right)
    {
      if (cpt_fill_eq(left->sample.right.fill,
                      right->sample.left.fill))
	{
	  cpt_seg_t *dead = right;

	  if ((right = dead->right) != NULL)
            right->left = left;

	  left->sample.right.val = dead->sample.right.val;
	  left->right = right;

	  cpt_seg_destroy(dead);
	}
      else
	{
	  left = left->right;
	  right = right->right;
	}
    }

  return 0;
}

/*
   returns whether the line is a comment line or not,
   it must not be null
*/

static int skipline(const char *line)
{
  const char *s;

  if (line == NULL) return 1;

  s = line;

  do {
    switch (*s)
      {
      case '#':
      case '\n':
      case '\0':
        return 1;
      case ' ':
      case '\t':
        break;
      default:
        return 0;
      }
    s++;
  }
  while (true);
}
