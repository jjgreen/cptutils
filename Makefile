# user targets

default: all

all:
	$(MAKE) -C src all

install:
	$(MAKE) -C src install

.PHONY: default all install

RUBBISH = *~
CONFIG = config.cache config.log config.status

clean:
	$(RM) $(RUBBISH)
	$(MAKE) -C src clean

veryclean:
	$(RM) $(CONFIG) $(RUBBISH)
	$(RM) -r autom4te.cache .mypy_cache
	$(MAKE) -C src veryclean

spotless: veryclean

.PHONY: clean veryclean spotless

test-accept:
	$(MAKE) -C src test-accept

test-unit:
	$(MAKE) -C src test-unit

test-python-type:
	$(MAKE) -C src test-python-type

test: test-accept test-unit test-python-type

.PHONY: test test-unit test-accept

# maintainer targets

# note that ./configure creates the version file VERSION, the variable
# $(VERSION) reads that file, so one should not use that variable
# until ./configure has been invoked, it will contain the old version.

VERSION = $(shell cat VERSION)

# coverage

coverage-prepare:
	CFLAGS='-O0 -g' ./configure --enable-coverage --enable-tests
	$(MAKE) all test

coverage-run:
	$(MAKE) -C src coverage

coverage-show:
	$(MAKE) -C src coverage-show

coverage-clean: veryclean

.PHONY: coverage-prepare coverage-run coverage-show coverage-clean

update-coverage: coverage-prepare coverage-run coverage-clean

.PHONY: update-coverage

# release targets proper

update-autoconf:
	aclocal --output config/aclocal.m4
	autoconf
	autoheader

update-assets:
	./configure
	bin/version-assets
	$(MAKE)
	$(MAKE) veryclean

update: update-autoconf update-assets

# this needs to be done once on first cloning the repo, only if you
# plan to develop the library: it symlinks files in .hooks (which
# is version controlled) to .git/hooks (which is not).

git-setup-hooks:
	bin/git-setup-hooks

.PHONY: git-setup-hooks

git-release-commit:
	git add -u
	git commit -m $(VERSION)
	git push origin master

git-create-tag:
	bin/git-release-tag $(VERSION)

git-push-tag:
	git push origin v$(VERSION)

.PHONY: git-release-commit git-create-tag git-push-tag

DIST = cptutils-$(VERSION)

dist:
	tar -C .. \
	  --transform s/^cptutils/$(DIST)/ \
	  --exclude-from .distignore \
	  -zpcvf ../$(DIST).tar.gz cptutils

.PHONY: dist
