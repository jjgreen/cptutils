# generated automatically by aclocal 1.16.5 -*- Autoconf -*-

# Copyright (C) 1996-2021 Free Software Foundation, Inc.

# This file is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

m4_ifndef([AC_CONFIG_MACRO_DIRS], [m4_defun([_AM_CONFIG_MACRO_DIRS], [])m4_defun([AC_CONFIG_MACRO_DIRS], [_AM_CONFIG_MACRO_DIRS($@)])])
m4_include([config/m4/ax_append_flag.m4])
m4_include([config/m4/ax_check_compile_flag.m4])
m4_include([config/m4/ax_check_define.m4])
m4_include([config/m4/ax_check_docbook_xslt.m4])
m4_include([config/m4/ax_compare_version.m4])
m4_include([config/m4/ax_count_cpus.m4])
m4_include([config/m4/ax_prog_xsltproc.m4])
